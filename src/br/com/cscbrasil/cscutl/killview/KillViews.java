/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.killview;

import br.com.cscbrasil.bmc.ARServerUser;
import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Constants;
import static com.bmc.arsys.api.Constants.*;
import com.bmc.arsys.api.EntryListInfo;
import com.bmc.arsys.api.Field;
import com.bmc.arsys.api.QualifierInfo;
import com.bmc.arsys.api.SQLResult;
import com.bmc.arsys.api.SortInfo;
import com.bmc.arsys.api.StatusInfo;
import com.bmc.arsys.api.Value;
import com.bmc.arsys.api.View;
import com.bmc.arsys.api.ViewCriteria;
import static com.bmc.arsys.api.ViewCriteria.LOCALE;
import com.bmc.thirdparty.org.apache.commons.lang.StringEscapeUtils;
import com.marzapower.loggable.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author ivan
 */
public class KillViews {

    RemedyConnection rmd;
    ARServerUser ar;
    List<String> viewsToKill;
    List<String> ignoreData;
    String inQuery;
    String inQual;

    public KillViews() {
        viewsToKill = new ArrayList<>();

        viewsToKill.add("fr");
        viewsToKill.add("it");
        viewsToKill.add("zh_CN");
        viewsToKill.add("zh_CH");
        viewsToKill.add("ja");
        viewsToKill.add("de");
        viewsToKill.add("ko");
        viewsToKill.add("de_DE");
        viewsToKill.add("fr_FR");
        viewsToKill.add("it_IT");
        //viewsToKill.add("es");
        //viewsToKill.add("es_ES");

        inQuery = "(";
        inQual = "(";
        for (String locale : viewsToKill) {
            inQuery += "'" + locale + "',";
            inQual += "'160'=\"" + locale + "\" OR ";
        }
        inQuery = inQuery.substring(0, inQuery.length() - 1);
        inQual = inQual.substring(0, inQual.length() - 4) + ")";

        inQuery += ")";
        rmd = RemedyConnections.get();
        ar = rmd.desti;

        ignoreData = new ArrayList<>();
        ignoreData.add("RKM:SearchUserSource");
        ignoreData.add("SRM:ProcessAOTSummaryData");
        ignoreData.add("SRM:Survey");
        ignoreData.add("SRM:WorkInfo");
        ignoreData.add("WOI:WorkInfo");
        ignoreData.add("WOI:WorkOrder");

    }

    public void execute(String pattern) throws ARException {
        Pattern formPattern = Pattern.compile(pattern);
        ar.cscSetModeBest();
        Log.get().info("Obtendo lista de formularios em ordem alfabetica " + pattern);
        List<String> formNames = ar.getListForm(0, Constants.AR_LIST_SCHEMA_ALL | Constants.AR_HIDDEN_INCREMENT);
        java.util.Collections.sort(formNames);
        Log.get().info("****** INICIO ELIMINACAO DE VIEWS ******");
        for (String formName : formNames) {
            Matcher formMatcher = formPattern.matcher(formName);
            if (formMatcher.find()) {
                if (formMatcher.group().equals(formName)) {
                    killV(formName);
                }
            }
        }
        Log.get().info("******  FINAL ELIMINACAO DE VIEWS ******");

        ar.cscSetModeBest();
        Log.get().info("Obtendo lista de formularios com dados traduzidos em ordem alfabetica " + pattern);
        int[] fieldIds = {AR_RESERV_LOCALE_LOCALIZED_SCHEMA};
        formNames = ar.getListForm(0, Constants.AR_LIST_SCHEMA_REGULAR | Constants.AR_HIDDEN_INCREMENT, null, fieldIds);
        java.util.Collections.sort(formNames);
        Log.get().info("****** INICIO ELIMINACAO DE DADOS ******");
        for (String formName : formNames) {
            if (ignoreData.contains(formName)) {
                continue;
            }
            Matcher formMatcher = formPattern.matcher(formName);
            if (formMatcher.find()) {
                if (formMatcher.group().equals(formName)) {
                    killD(formName);
                }
            }
        }
        Log.get().info("******  FINAL ELIMINACAO DE DADOS ******");
    }

    public void killV(String formName) throws ARException {
        ar.cscSetModeBest();
        ViewCriteria criteria = new ViewCriteria();
        criteria.setRetrieveAll(false);
        criteria.setPropertiesToRetrieve(LOCALE);
        List<View> views = ar.getListViewObjects(formName, 0, criteria);
        int sobram = 0;
        int apagariamos = 0;
        for (View view : views) {
            if (!viewsToKill.contains(view.getLocale())) {
                sobram++;
            } else {
                apagariamos++;
            }
        }
        if (apagariamos == 0) {
            return;
        }
        Log.get().info(formName);
        if (sobram > 0) {
            for (View view : views) {
                if (viewsToKill.contains(view.getLocale())) {
                    Log.get().info("   " + view.getName() + " (" + view.getVUIId() + ")" + " " + view.getLocale());
                    ar.cscSetModeBest();
                    while (true) {
                        try {
                            ar.deleteView(formName, view.getVUIId());
                        } catch (ARException ex) {
                            List<StatusInfo> lastStatus = ex.getLastStatus();
                            if (lastStatus.size() == 1) {
                                if (lastStatus.get(0).getMessageNum() == 8865) {
                                    ar.cscSetModeBase();
                                    continue;
                                }
                            }
                            Log.get().error(ex.getMessage());
                            break;
                        }
                        break;
                    }
                }
            }
        } else {
            Log.get().info("   Form ignorado, seriam apagadas todas as views");
        }
    }

    private void killD(String formName) throws ARException {
        ar.cscSetModeBest();
        int formNumber = ar.cscGetFormNumber(formName);
        String sql1 = String.format("SELECT COUNT(C1) FROM T%d", formNumber);
        String sql2 = String.format("SELECT COUNT(C1) FROM T%d where C%d in %s", formNumber, AR_RESERV_LOCALE_LOCALIZED_SCHEMA, inQuery);
        String sql3 = String.format("SELECT DISTINCT(C%d) from T%d", AR_RESERV_LOCALE_LOCALIZED_SCHEMA, formNumber);

        int count_total = execCount(sql1, formName);
        if (count_total > 0) {
            int count_apaga = execCount(sql2, formName);
            int count_sobra = count_total - count_apaga;
            if (count_apaga > 0) {
                List<String> locales = execStringList(sql3, formName);
                String localeString = "";
                for (String locale : locales) {
                    if (!viewsToKill.contains(locale)) {
                        localeString += "'" + locale + "',";
                    }
                }
                if (localeString.length() > 0) {
                    localeString = localeString.substring(0, localeString.length() - 1);
                }
                Log.get().info(String.format("%-80s total=%-9d apagar=%-9d sobram=%-9d locales que ficam=%s", formName, count_total, count_apaga, count_sobra, localeString));
                if (count_sobra <= 0) {
                    Log.get().info("   Formulario foi ignorado, seriam apagados todos os registros");
                    return;
                } else {
                    deleteViaDBByQuery(formName, inQual);
                }
            } else {
                Log.get().info(String.format("%-80s total=%-9d apagar=%-9d sobram=%-9d sem registros a apagar", formName, count_total, count_apaga, count_sobra));
            }

        }
    }

    private int execCount(String query, String formName) throws ARException {
        SQLResult listSQL;
        try {
            listSQL = ar.getListSQL(query, 999999999, false);
        } catch (ARException ex) {
            Log.get().error(formName + " - " + ex.getMessage() + " " + query);
            return 0;
        }
        List<List<Value>> contents = listSQL.getContents();
        for (List<Value> content : contents) {
            for (Value value : content) {
                return value.getIntValue();
            }
        }
        return 0;
    }

    private List<String> execStringList(String query, String formName) throws ARException {
        List<String> retorno = new ArrayList();
        SQLResult listSQL;
        try {
            listSQL = ar.getListSQL(query, 999999999, false);
        } catch (ARException ex) {
            Log.get().error(formName + " - " + ex.getMessage() + " " + query);
            return retorno;
        }
        List<List<Value>> contents = listSQL.getContents();
        for (List<Value> content : contents) {
            for (Value value : content) {
                if (value != null) {
                    if (value.toString() != null) {
                        if (value.toString().trim().length() > 0) {
                            retorno.add(value.toString().trim());
                        }
                    }
                }
            }
        }
        java.util.Collections.sort(retorno);
        return retorno;
    }

    private String escapeForDatabase(String queryparam) {
        return StringEscapeUtils.escapeSql(queryparam);
    }

    private List<String> getDBTables(String formName) {
        List<String> getDBTables = new ArrayList<>();
        int formId = ar.cscGetFormNumber(formName);
        if (formId == 0) {
            return getDBTables;
        }
        getDBTables.add(String.format("B%d", formId));
        getDBTables.add(String.format("H%d", formId));
        SQLResult result = null;
        try {
            String cmd = String.format("Select fieldid from field inner join arschema on arschema.schemaid=field.schemaid where (arschema.name='%s' OR arschema.name='%s__o' OR arschema.name='%s__c') AND datatype=%d AND foption<%d", escapeForDatabase(formName), escapeForDatabase(formName), escapeForDatabase(formName), Constants.AR_DATA_TYPE_ATTACH, Constants.AR_FIELD_OPTION_DISPLAY);
            result = ar.getListSQL(cmd, 999999999, false);
        } catch (ARException ex) {
            Log.get().fatal(ex.getMessage(), ex);
            return getDBTables;
        }
        List<List<Value>> records = result.getContents();
        for (List<Value> record : records) {
            Value v = record.get(0);
            getDBTables.add(String.format("B%dC%d", formId, v.getIntValue()));
        }
        getDBTables.add(String.format("T%d", formId));
        return getDBTables;
    }

    private void deleteViaDBByQuery(String formName, String qualString) throws ARException {
        List<String> dbTables = getDBTables(formName);
        QualifierInfo qualification = ar.parseQualification(formName, qualString);
        ArrayList<SortInfo> sortList = new ArrayList<>();
        sortList.add(new SortInfo(1, Constants.AR_SORT_ASCENDING));
        ArrayList<Field> entryListFields = new ArrayList<>();
        while (true) {
            List<EntryListInfo> listEntry = ar.getListEntry(formName, qualification, Constants.AR_START_WITH_FIRST_ENTRY, ar.cscGetMaxRecords(), sortList, entryListFields, false, null);
            if (listEntry.isEmpty()) {
                break;
            }
            for (EntryListInfo e : listEntry) {
                for (String table : dbTables) {
                    try {
                        String chave = "C1";
                        if (table.startsWith("H")) {
                            chave = "entryId";
                        }
                        if (table.startsWith("B") && table.contains("C")) {
                            chave = "entryId";
                        }
                        String cmd = String.format("delete from %s where %s='%s'", table, chave, e.getEntryID());
                        ar.getListSQL(cmd, 10, false);
                    } catch (ARException ex) {
                        // ignora
                    }
                }
            }
        }
    }
}
