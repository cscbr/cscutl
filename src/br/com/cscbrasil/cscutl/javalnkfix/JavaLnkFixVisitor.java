/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.javalnkfix;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.logging.Level;
import java.util.logging.Logger;
import mslinks.ShellLink;
import mslinks.ShellLinkException;

/**
 *
 * @author ivan
 */
public class JavaLnkFixVisitor implements FileVisitor {

    @Override
    public FileVisitResult preVisitDirectory(Object t, BasicFileAttributes bfa) throws IOException {
        if (!Files.isReadable((Path) t)) {
            return FileVisitResult.SKIP_SUBTREE;
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Object t, BasicFileAttributes bfa) throws IOException {
        Path p = (Path) t;

        if (p.toFile().isFile()) {
            if (p.toFile().getName().toLowerCase().endsWith(".lnk")) {
                System.out.println((Path) t);
                try {
                    fix(p.toString());
                } catch (ShellLinkException ex) {
                    System.out.println(p.toFile());
                    Logger.getLogger(JavaLnkFixVisitor.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Object t, IOException ioe) throws IOException {
        // you can log the exception 'exc'
        return FileVisitResult.SKIP_SUBTREE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Object t, IOException ioe) throws IOException {
        return FileVisitResult.CONTINUE;
    }

    public void fix(String path) throws IOException, ShellLinkException {
        ShellLink sl = new ShellLink(path);
        if (sl.getRelativePath() != null) {
            File f = new File(sl.getRelativePath());
            String name = f.getName();
            if (name.equalsIgnoreCase("javaw.exe") || name.equalsIgnoreCase("javaw")) {
                sl.setTarget(JavaLnkFix.javaPath + "\\bin\\javaw.exe");
                sl.saveTo(path);
            }
            if (name.equalsIgnoreCase("java.exe") || name.equalsIgnoreCase("java")) {
                sl.setTarget(JavaLnkFix.javaPath + "\\bin\\java.exe");
                sl.saveTo(path);
            }
        }
    }

}
