/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.javalnkfix;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import mslinks.ShellLinkException;

/**
 *
 * @author ivan
 */
public class JavaLnkFix {

    static String javaPath;

    public JavaLnkFix(String javaPath1) {
        javaPath = javaPath1;
    }

    public void start() throws IOException, ShellLinkException {
        //Files.walkFileTree(Paths.get("C:\\Users"), new JavaLnkFixVisitor());
        Files.walkFileTree(Paths.get("C:\\ProgramData\\Microsoft\\Windows\\Start Menu"), new JavaLnkFixVisitor());
        
    }
}
