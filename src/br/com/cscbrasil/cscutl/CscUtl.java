//     _____             _    _  _    _ 
//    / ____|           | |  | || |  | |
//   | |      ___   ___ | |  | || |_ | |
//   | |     / __| / __|| |  | || __|| |
//   | |____ \__ \| (__ | |__| || |_ | |
//    \_____||___/ \___| \____/  \__||_|
//                                      
//                                      
package br.com.cscbrasil.cscutl;

import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.cscutl.utils.MonitorUser;
import br.com.cscbrasil.cscutl.especificos.Caixa;
import br.com.cscbrasil.bmc.RemedyConnections;
import br.com.cscbrasil.common.Parameters;
import br.com.cscbrasil.cscutl.aif.AifConfig;
import br.com.cscbrasil.cscutl.bulkinstance.BulkInstance;
import br.com.cscbrasil.cscutl.bulkload.BulkLoad;
import br.com.cscbrasil.cscutl.especificos.Nec;
import br.com.cscbrasil.cscutl.especificos.Tecban;
import br.com.cscbrasil.cscutl.especificos.Tim;
import br.com.cscbrasil.cscutl.especificos.caixa.LongQueryKill;
import br.com.cscbrasil.cscutl.especificos.nec.NecC2;
import br.com.cscbrasil.cscutl.expurgo.Expurgo;
import br.com.cscbrasil.cscutl.killview.KillViews;
import br.com.cscbrasil.cscutl.lixo.Lixo;
import br.com.cscbrasil.cscutl.utils.AplicLic;
import br.com.cscbrasil.cscutl.utils.CleanUnderlineC;
import br.com.cscbrasil.cscutl.utils.CreateSuperUser;
import br.com.cscbrasil.cscutl.utils.DeleteDisplayOnlyJoin;
import br.com.cscbrasil.cscutl.utils.DumpRecord;
import br.com.cscbrasil.cscutl.utils.ExportData;
import br.com.cscbrasil.cscutl.utils.Inventario;
import br.com.cscbrasil.cscutl.utils.fortification.Fortification;
import br.com.cscbrasil.cscutl.utils.indices.Aplicador;
import br.com.cscbrasil.cscutl.utils.indices.Consolidador;
import br.com.cscbrasil.cscutl.utils.indices.GeraPlanilhaIndices;
import br.com.cscbrasil.cscutl.utils.monitoradb.MonitoraDb;
import br.com.cscbrasil.cscutl.utils.searchdbstring.SearchStringDb;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Entry;
import com.bmc.arsys.api.Field;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Timer;
import java.util.TimerTask;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.joda.time.DateTime;

/**
 *
 * @author ivan
 */
public class CscUtl {

    static {
        File log4j = new File("conf/log4j2.xml");
        System.setProperty("log4j.configurationFile", log4j.toURI().toString());
        LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
        org.apache.logging.log4j.core.config.Configuration config = ctx.getConfiguration();
        LoggerConfig loggerConfig = config.getLoggerConfig(LogManager.ROOT_LOGGER_NAME);
        ctx.updateLoggers();
    }

    public static void main(String[] args) throws URISyntaxException, IOException, FileNotFoundException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, WriteException, BiffException, ARException, InterruptedException {

        File log4j = new File(new URI(System.getProperty("log4j.configurationFile")).getPath());
        Log.get().info(String.format("CSC BRASIL - CscUtl %s", CscUtl.class.getPackage().getImplementationVersion()));
        Log.get().info(String.format("Processo iniciado - %d processadores", Runtime.getRuntime().availableProcessors()));
        Log.get().info(String.format("Arquivo de configuracao log = %s", log4j.getAbsolutePath()));
        Parameters.setConfigFileName("cscutl");
        RemedyConnections.init();

        /*
        if (1==1){
            RemedyConnection rmd=RemedyConnections.get();
            rmd.sourc.cscSetModeBest();
            rmd.desti.cscSetModeBest();
            Field field = rmd.sourc.getField("WOI:WorkOrder", 1000000150);
            rmd.desti.setField(field);            
            
        }
         */
        ///////////////////////////////////
        // 1 parametros
        ///////////////////////////////////
        if (args.length < 1) {
            return;
        }

        // Nec               
        if (args[0].equals("nec_user") && args.length < 3) {
            Nec c;
            try {
                c = new Nec(args[1]);
                c.user(null);
            } catch (ClassNotFoundException | SQLException ex) {
                Log.get().error(ex);
                return;
            }
        }
        if (args[0].equals("nec_delta") && args.length < 3) {
            Nec c;
            try {
                c = new Nec(args[1]);
                c.delta(null);
            } catch (ClassNotFoundException | SQLException ex) {
                Log.get().error(ex);
                return;
            }
        }
        if (args[0].equals("nec_conta") && args.length < 2) {
            Nec c;
            try {
                c = new Nec(null);
                c.conta(null);
            } catch (ClassNotFoundException | SQLException ex) {
                Log.get().error(ex);
                return;
            }
        }
        if (args[0].equals("nec_c2") && args.length < 2) {
            Nec c;
            try {
                c = new Nec(null);
                c.c2(null);
            } catch (ClassNotFoundException | SQLException ex) {
                Log.get().error(ex);
                return;
            }
        }
        if (args[0].equals("nec_retry") && args.length < 2) {
            Nec c;
            try {
                c = new Nec(null);
                c.retry(null);
            } catch (ClassNotFoundException | SQLException ex) {
                Log.get().error(ex);
                return;
            }
        }
        if (args[0].equals("nec_delete") && args.length < 2) {
            Nec c;
            try {
                c = new Nec(null);
                c.delete(null);
            } catch (ClassNotFoundException | SQLException ex) {
                Log.get().error(ex);
                return;
            }
        }
        // Tim
        if (args[0].equals("new_sirios")) {
            Tim.new_sirios();
            return;
        }
        if (args[0].equals("new_sirios2")) {
            Tim.new_sirios2();
            return;
        }
        if (args[0].equals("new_sirios3")) {
            Tim.new_sirios3();
            return;
        }
        ///

        // TecBan
        if (args[0].equals("carga_slm_assignment")) {
            Tecban.carga_slm_assignment();
            return;
        }
        ///

        if (args[0].equals("recriarViews")) {
            RecriarViews c;
            c = new RecriarViews();
            c.execute();
        }

        if (args[0].equals("delta_delete")) {
            Caixa c;
            c = new Caixa();
            c.delta_delete();
        }

        //nao finalizado
        /*
        if (args[0].equals("index_check")) {
            Caixa c;            
            c = new Caixa();
            c.index_check();
        }
         */
        if (args[0].equals("delta") && args.length < 2) {
            Caixa c;
            c = new Caixa();
            c.delta();
        }

        if (args[0].equals("monitor_user")) {
            MonitorUser c;
            c = new MonitorUser();
            c.monitor();
        }

        if (args[0].equals("long_query_kill")) {
            LongQueryKill c;
            c = new LongQueryKill();
            c.run();
        }

        if (args[0].equals("geraPlanilhaIndices")) {
            GeraPlanilhaIndices c;
            Consolidador consolidador;
            try {
                c = new GeraPlanilhaIndices();
                c.execute();
                consolidador = new Consolidador();
                consolidador.consolida();
            } catch (ARException | BiffException | WriteException ex) {
                Log.get().error(ex);
                return;
            }
        }
        if (args[0].equals("consolidaPlanilhaIndices")) {
            Consolidador consolidador;
            try {
                consolidador = new Consolidador();
                consolidador.consolida();
            } catch (BiffException | WriteException ex) {
                Log.get().error(ex);
                return;
            }
        }
        if (args[0].equals("aplicaPlanilhaIndices")) {
            Aplicador aplicador;
            try {
                aplicador = new Aplicador();
                aplicador.aplica();
            } catch (BiffException | WriteException ex) {
                Log.get().error(ex);
                return;
            }
        }
        if (args[0].equals("inventario")) {
            Inventario inventario;
            inventario = new Inventario();
            inventario.run();
        }
        if (args[0].equals("monitoraDB")) {
            TimerTask monitoraDB1 = new MonitoraDb(1);
            Timer timerMonitoraDb1 = new Timer(true);
            timerMonitoraDb1.schedule(monitoraDB1, 0, 1);
            TimerTask monitoraDB2 = new MonitoraDb(2);
            Timer timerMonitoraDb2 = new Timer(true);
            timerMonitoraDb2.schedule(monitoraDB2, 0, 1);
            Log.get().info("Monitores Iniciados");
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {

                }
            }
        }

        if (args[0].equals("lixo")) {
            Lixo c = new Lixo();
            c.p1();
        }

        ///////////////////////////////////
        // 2 parametros
        ///////////////////////////////////
        if (args.length < 2) {
            return;
        }

        if (args[0].equals("superUser")) {
            CreateSuperUser c = new CreateSuperUser();
            c = new CreateSuperUser();
            c.run(args[1]);
        }

        if (args[0].equals("nec_zauC2")) {
            NecC2 c = new NecC2();
            c = new NecC2();
            c.zauC2(args[1]);
        }

        if (args[0].equals("delta")) {
            Caixa c;
            c = new Caixa();
            c.delta(args[1]);
        }

        if (args[0].equals("deleteDisplayOnlyJoin") && args.length == 2) {
            DeleteDisplayOnlyJoin c;
            c = new DeleteDisplayOnlyJoin(args[1]);
            c.execute();
        }

        if (args[0].equals("aif") && args.length == 2) {
            AifConfig c;
            c = new AifConfig(args[1]);
            c.execute();
        }

        if (args[0].equals("killViews") && args.length == 2) {
            KillViews c;
            c = new KillViews();
            c.execute(args[1]);
        }

        if (args[0].equals("bulkInstance")) {
            BulkInstance c;
            c = new BulkInstance(args[1]);
            c.execute();
        }
        if (args[0].equals("bulkLoad")) {
            BulkLoad c;
            c = new BulkLoad(args[1]);
            c.execute();
        }
        if (args[0].equals("searchStringDb")) {
            SearchStringDb c;
            if (args.length == 3) {
                c = new SearchStringDb(args[1], args[2]);
            } else {
                c = new SearchStringDb(args[1], null);
            }
            try {
                c.execute();
            } catch (ARException ex) {
                Log.get().error(ex);
                return;
            }
        }
        if (args[0].equals("cleanUnderlineC")) {
            CleanUnderlineC c = new CleanUnderlineC(args[1]);
            try {
                c.execute();
            } catch (ARException ex) {
                Log.get().error(ex);
                return;
            }
        }
        if (args[0].equals("fortification")) {
            Fortification c;
            try {
                c = new Fortification();
                c.execute(args[1]);
            } catch (ARException ex) {
                Log.get().error(ex);
                return;
            }
        }
        if (args[0].equals("nec_conta")) {
            Nec c;
            try {
                c = new Nec(null);
                c.conta(args[1]);
            } catch (ClassNotFoundException | SQLException ex) {
                Log.get().error(ex);
                return;
            }
        }
        if (args[0].equals("nec_retry")) {
            Nec c;
            try {
                c = new Nec(null);
                c.retry(args[1]);
            } catch (ClassNotFoundException | SQLException ex) {
                Log.get().error(ex);
                return;
            }
        }

        ///////////////////////////////////
        // 3 parametros
        ///////////////////////////////////
        if (args.length < 3) {
            return;
        }
        if (args[0].equals("nec_delta")) {
            Nec c;
            try {
                c = new Nec(args[1]);
                c.delta(args[2]);
            } catch (ClassNotFoundException | SQLException ex) {
                Log.get().error(ex);
                return;
            }
        }
        if (args[0].equals("nec_user")) {
            Nec c;
            try {
                c = new Nec(args[1]);
                c.user(args[2]);
            } catch (ClassNotFoundException | SQLException ex) {
                Log.get().error(ex);
                return;
            }
        }
        if (args[0].equals("dumpRecord")) {
            DumpRecord c = new DumpRecord();
            c.execute(args[1], args[2]);
        }
        if (args[0].equals("expurgo")) {
            Expurgo c;
            String q = args[2].replaceAll("~", "\"");
            c = new Expurgo(args[1], q, "");
            c.execute();
        }
        if (args[0].equals("aplicaLic")) {
            AplicLic c;
            try {
                c = new AplicLic(args[1], args[2]);
                c.execute();
            } catch (ARException ex) {
                Log.get().error(ex);
                return;
            }
        }
        if (args[0].equals("exportData")) {
            ExportData c;
            c = new ExportData(args[1], args[2]);
            c.execute();
        }
    }

}
