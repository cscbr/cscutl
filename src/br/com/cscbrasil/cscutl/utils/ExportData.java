/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.utils;

import br.com.cscbrasil.bmc.ARServerUser;
import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import com.bmc.arsys.artools.arexport.ARExport;
import java.io.File;
import org.joda.time.DateTime;

/**
 *
 * @author ivan
 */
public class ExportData {

    RemedyConnection rmd;
    String formName;
    String qualString;

    public ExportData(String formName, String qualString) {
        this.rmd = RemedyConnections.get();
        this.formName = formName;
        this.qualString = qualString;
    }

    public synchronized void execute() {
        DateTime d = new DateTime();
        String arxPath = "exports";
        ARServerUser ar = rmd.desti;
        String arxNameNoExtension = formName.replaceAll("[^a-zA-Z0-9.-]", "_").toLowerCase();
        arxNameNoExtension = arxNameNoExtension;
        arxNameNoExtension = arxNameNoExtension + "_" + String.format("%04d_%02d_%02d_%02d_%02d_%02d", d.getYear(), d.getMonthOfYear(), d.getDayOfMonth(), d.getHourOfDay(), d.getMinuteOfHour(), d.getSecondOfMinute());
        File formDir;
        formDir = new File(arxPath);
        formDir.mkdirs();
        File outputFile = new File(formDir.getPath() + "/" + arxNameNoExtension + ".arx");
        //if (formName.equals("User")) { //-e "3, 7"
        //    String[] parms = {"-e", "1,2,3,4,5,6,7,8,15,103,104,108,109,110,119,122,124,126,127,128,129,130,138,139,179,60988,301600300,301628400,301628500,301628510,301628520,301628530,301628560,301628561,301628562,301628563,301628564,301628565,301628600,301628700,301628800,301628810,301628820,301628830,304290290,490000000,490000100", "-u", ar.getUser(), "-x", ar.getServer(), "-p", ar.getPassword(), "-t", String.format("%d", ar.getPort()), "-form", formName, "-datafile", outputFile.getAbsolutePath(), "-q", qualString, "-timestamp", "1"};
        //    ARExport.main(parms);
        //} else {
        String[] parms = {"-u", ar.getUser(), "-x", ar.getServer(), "-p", ar.getPassword(), "-t", String.format("%d", ar.getPort()), "-form", formName, "-datafile", outputFile.getAbsolutePath(), "-q", qualString, "-timestamp", "1"};
        ARExport.main(parms);
        //}

    }

}
