/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.utils;

import br.com.cscbrasil.bmc.ARServerUser;
import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Field;
import com.bmc.arsys.api.Form;
import com.bmc.arsys.api.JoinFieldMapping;
import com.bmc.arsys.api.JoinForm;
import com.marzapower.loggable.Log;
import java.util.List;

/**
 *
 * @author ivan
 */
public class DeleteDisplayOnlyJoin {

    String formName;
    RemedyConnection rmd;
    ARServerUser ar;

    public DeleteDisplayOnlyJoin(String formName) {
        this.formName = formName;
        RemedyConnection rmd = RemedyConnections.get();
        ar = rmd.desti;
        ar.cscSetModeBest();
    }

    public void execute() throws ARException {
        Form form = ar.getForm(formName);
        if (form.getFormType() != Constants.AR_SCHEMA_JOIN) {
            Log.get().error("Esse form nao e um join form");
        }

        int pos = 3;
        JoinForm joinForm = (JoinForm) form;
        String memberA = joinForm.getMemberA().substring(0, pos);
        String memberB = joinForm.getMemberB().substring(0, pos);

        while (memberA.equals(memberB)) {
            pos++;
            memberA = joinForm.getMemberA().substring(0, pos);
            memberB = joinForm.getMemberB().substring(0, pos);
        }
        memberA += ":";
        memberB += ":";

        while (memberA.contains("::")) {
            memberA = memberA.replaceAll("::", ":");
        }
        while (memberB.contains("::")) {
            memberB = memberB.replaceAll("::", ":");
        }

        boolean apagou = true;
        while (apagou) {
            apagou = false;
            List<Field> fields = ar.getListFieldObjects(formName);
            for (Field field : fields) {
                if (field.getFieldID() == 1) {
                    int a = 0;
                }
                if (field.getFieldOption() == Constants.AR_FIELD_OPTION_DISPLAY) {
                    if (field.getFieldType() == Constants.AR_FIELD_TYPE_DATA) {
                        Log.get().info("Eliminando campo display " + field.getName());
                        ar.deleteField(formName, field.getFieldID(), 0);
                        apagou = true;
                        continue;
                    }
                }
                String fieldName = field.getName();
                JoinFieldMapping fieldMap = (JoinFieldMapping) field.getFieldMap();
                if (fieldMap != null) {

                    String prefixo = memberA;
                    if (fieldMap.getIndex() == 1) {
                        prefixo = memberB;
                    }

                    if (field.getName().endsWith("__c")) {
                        fieldName = field.getName().substring(0, field.getName().length() - 3);
                    }
                    if (!fieldName.startsWith(prefixo)) {
                        fieldName = prefixo + fieldName;
                    }
                    while (fieldName.contains("::")) {
                        fieldName = fieldName.replaceAll("::", ":");
                    }
                    String duplo = prefixo + prefixo;
                    while (fieldName.startsWith(duplo)) {
                        fieldName = fieldName.replaceFirst(duplo, prefixo);
                    }

                }
                if (field.getFieldID() == 1) {
                    fieldName = "Request ID";
                }

                if (!fieldName.equals(field.getName())) {
                    field.setNewName(fieldName);
                    Log.get().info("Renomeando campo de " + field.getName() + " para " + fieldName);
                    ar.setField(field);
                    apagou = true;
                }
            }
        }

    }
}
