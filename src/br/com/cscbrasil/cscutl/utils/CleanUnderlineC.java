/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.utils;

import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import com.bmc.arsys.api.ARException;

/**
 *
 * @author ivan
 */
public class CleanUnderlineC {

    private final String form;
    RemedyConnection rmd;

    public CleanUnderlineC(String form) {
        this.form = form;
        rmd = RemedyConnections.get();
    }

    public void execute() throws ARException {
        rmd.sourc.getForm(form);
    }

}
