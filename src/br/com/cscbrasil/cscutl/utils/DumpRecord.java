/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.utils;

import br.com.cscbrasil.bmc.ARServerUser;
import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Entry;
import com.bmc.arsys.api.Field;
import com.bmc.arsys.api.Value;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author ivan
 */
public class DumpRecord {

    RemedyConnection rmd;
    ARServerUser ar;

    public DumpRecord() {
        rmd = RemedyConnections.get();
        ar = rmd.sourc;
        ar.cscSetModeBest();
    }

    public void execute(String formName, String requestId) throws ARException {
        FieldList f = new FieldList(formName);

        int[] entryListFields = f.getFieldIdsAll();
        Entry entry = ar.getEntry(formName, requestId, entryListFields);

        System.out.println("Entry e = new Entry();");

        Set<Map.Entry<Integer, Value>> entrySet = entry.entrySet();
        List<String> limpos = new ArrayList<>();
        for (Map.Entry<Integer, Value> e : entrySet) {
            int fieldId = e.getKey();
            Field field = ar.getField(formName, fieldId);
            String option = "";
            if (field.getFieldOption() == Constants.AR_FIELD_OPTION_DISPLAY || field.getFieldOption() == Constants.AR_FIELD_OPTION_SYSTEM) {
                continue;
            }
            if (field.getFieldOption() == Constants.AR_FIELD_OPTION_OPTIONAL) {
                option = "optional";
            }
            if (field.getFieldOption() == Constants.AR_FIELD_OPTION_REQUIRED) {
                option = "required";
            }
            String valor = "";
            if (e.getValue().getValue() != null) {
                switch (field.getDataType()) {
                    case Constants.AR_DATA_TYPE_NULL:
                        break;
                    case Constants.AR_DATA_TYPE_KEYWORD:
                        break;
                    case Constants.AR_DATA_TYPE_INTEGER:
                    case Constants.AR_DATA_TYPE_ENUM:
                    case Constants.AR_DATA_TYPE_BITMASK:
                    case Constants.AR_DATA_TYPE_BYTES:
                    case Constants.AR_DATA_TYPE_DECIMAL:
                    case Constants.AR_DATA_TYPE_DATE:
                        valor = String.format("%d", e.getValue().getIntValue());
                        break;
                    case Constants.AR_DATA_TYPE_TIME_OF_DAY:
                    case Constants.AR_DATA_TYPE_TIME:
                        valor = String.format("%s", e.getValue().toString());
                        break;
                    case Constants.AR_DATA_TYPE_CURRENCY:
                        valor = String.format("%s", e.getValue().getCurrencyValue());
                        break;
                    case Constants.AR_DATA_TYPE_REAL:
                        valor = String.format("%f", e.getValue().getDoubleValue());
                        break;
                    case Constants.AR_DATA_TYPE_CHAR:
                        valor = "\"" + e.getValue().toString() + "\"";
                        break;
                    case Constants.AR_DATA_TYPE_DIARY:
                        continue;
                    case Constants.AR_DATA_TYPE_ATTACH:
                        continue;
                }
            }
            if (valor.equals("\"null\"")) {
                valor = "";
            }
            if (!valor.isEmpty()) {
                System.out.println(String.format("e.put(%d, new Value(%s)); // %s (%s)", fieldId, valor, field.getName(), option));
            } else {
                limpos.add(String.format("e.put(%d, new Value(%s)); // %s (%s)", fieldId, valor, field.getName(), option));
            }
        }
        for (String linha : limpos) {
            System.out.println(linha);
        }
        System.out.println(String.format("String entryId = ar.createEntry(\"%s\", e);", formName));

    }

}
