/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.utils.indices;

import com.marzapower.loggable.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 *
 * @author ivan
 */
public class Schema {

    String schema;
    String customization;
    String cmdbClass;
    List<Index> nativos;
    List<Index> custom;
    List<Index> todos;

    public Schema(String Schema, String Customization, String CmdbClass) {
        this.schema = Schema;
        this.customization = Customization;
        this.cmdbClass = CmdbClass;
        nativos = new ArrayList<>();
        custom = new ArrayList<>();
        todos = new ArrayList<>();
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String Schema) {
        this.schema = Schema;
    }

    public String getCustomization() {
        return customization;
    }

    public void setCustomization(String Customization) {
        this.customization = Customization;
    }

    public String getCmdbClass() {
        return cmdbClass;
    }

    public void setCmdbClass(String CmdbClass) {
        this.cmdbClass = CmdbClass;
    }

    public String getAPI() {
        if (cmdbClass.equals("YES")) {
            return "CMDB";
        }
        return "ARS";
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.schema);
        hash = 97 * hash + Objects.hashCode(this.customization);
        hash = 97 * hash + Objects.hashCode(this.cmdbClass);
        hash = 97 * hash + Objects.hashCode(this.nativos);
        hash = 97 * hash + Objects.hashCode(this.custom);
        hash = 97 * hash + Objects.hashCode(this.todos);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Schema other = (Schema) obj;
        if (!Objects.equals(this.schema, other.schema)) {
            return false;
        }
        return true;
    }

    public void retiraDuplicatas() {
        SortedSet<String> myOrderedUniqueList = new TreeSet<>(java.util.Collections.reverseOrder());
        for (Index list : todos) {
            String add = list.toString();
            add = add.replaceAll("\\[", "");
            add = add.replaceAll("\\]", "");
            add = add.replaceAll(" ", "");
            add = add.replaceAll(",", " ");
            add = add.trim() + " ";
            if (add.substring(1).equals("6 1 ")) {
                continue;
            }
            myOrderedUniqueList.add(add.trim());
        }
        List<String> ficam = new ArrayList<>();
        for (String listCampos : myOrderedUniqueList) {
            boolean tem = false;
            for (String s : ficam) {
                if (s.startsWith(listCampos.substring(1) + " ")) {
                    tem = true;
                    break;
                }
            }
            if (!tem) {
                ficam.add(listCampos.substring(1).trim());
            }
        }
        List<Index> copia = new ArrayList<>(todos);
        todos.clear();
        for (String s : ficam) {
            Scanner scanner = new Scanner(s);
            List<Integer> list = new ArrayList<>();
            while (scanner.hasNextInt()) {
                list.add(scanner.nextInt());
            }
            boolean encontrou = false;
            for (Index c : copia) {
                if (c.getFields().equals(list)) {
                    Index index = new Index(this, c.getUnique(), c.getIndexCustomization(), list);
                    todos.add(index);
                    encontrou = true;
                    break;
                }
            }
            if (!encontrou) {
                Log.get().error("Nao encontrado indice " + list.toString() + " em " + schema);
            }
        }

    }
}
