/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.utils.indices;

import br.com.cscbrasil.bmc.ARServerUser;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Form;
import com.bmc.arsys.api.IndexInfo;
import com.bmc.arsys.api.ObjectPropertyMap;
import com.bmc.arsys.api.SQLResult;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 *
 * @author ivan
 */
public class Indexer {

    String formName;
    Form form;
    ARServerUser ar;
    Schema schema;
    List<List<Integer>> indicesCriar;

    Indexer(ARServerUser ar, Schema schema) {
        this.ar = ar;
        this.form = null;
        try {
            this.form = ar.getForm(schema.getSchema());
        } catch (ARException ex) {
            Log.get().error(ex);
            return;            
        }
        this.formName = form.getName();
        this.schema = schema;        
    }

    void run() {        
        int tenta = 0;
        while (true) {
            tenta++;            
            ar.cscSetModeBest();
            
            retiraIndicesPiratas();
            retiraIndicesDuplicados();
            retiraIndicesExistentes();

            if (indicesCriar.isEmpty()) {
                break;
            }
            if (tenta > 3) {
                Log.get().error("Verifique a criacao do indice em " + formName + ". Apos 3 tentativas o indice nao foi criado");
                break;
            }
            for (List<Integer> indice : indicesCriar) {
                createIndex(indice);
            }
        }
    }

    private void retiraIndicesDuplicados() {
        SortedSet<String> myOrderedUniqueList = new TreeSet<String>(java.util.Collections.reverseOrder());
        for (List<Integer> list : indicesCriar) {
            String add = list.toString();
            add = add.replaceAll("\\[", "");
            add = add.replaceAll("\\]", "");
            add = add.replaceAll(" ", "");
            add = add.replaceAll(",", " ");
            add = add.trim();
            myOrderedUniqueList.add(add);
        }
        List<String> ficam = new ArrayList<>();

        for (String listCampos : myOrderedUniqueList) {
            boolean tem = false;
            for (String s : ficam) {
                if (s.startsWith(listCampos)) {
                    tem = true;
                    break;
                }
            }
            if (!tem) {
                ficam.add(listCampos);
            }
        }

        indicesCriar.clear();
        for (String s : ficam) {
            Scanner scanner = new Scanner(s);
            List<Integer> list = new ArrayList<Integer>();
            while (scanner.hasNextInt()) {
                list.add(scanner.nextInt());
            }
            indicesCriar.add(list);
        }
    }

    private void retiraIndicesExistentes() {
        List<List<Integer>> temp = new ArrayList(indicesCriar);
        indicesCriar.clear();
        for (List<Integer> list : temp) {
            if (!hasIndex(list)) {
                indicesCriar.add(list);
            }
        }
    }

    private boolean hasIndex(List<Integer> ids) {
        if (ids.isEmpty()) {
            return true;
        }
        if (ar.cscGetDatabase().toLowerCase().contains("oracle")) {
            int t = ar.cscGetFormNumber(formName);
            if (t == 0) {
                return true;
            }

            String q;
            int position = 1;
            String w = "";
            for (Integer id : ids) {
                if (position == 1) {
                    w = String.format("select index_name from user_ind_columns where table_name = 'T%d' and column_name='C%d' and column_position=%d", t, id, position);
                } else {
                    w = String.format("select index_name from user_ind_columns where table_name = 'T%d' and column_name='C%d' and column_position=%d and index_name in (%s)", t, id, position, w);
                }
                position++;
            }

            SQLResult result;
            try {
                result = ar.getListSQL(w, 999999999, false);
            } catch (ARException ex) {
                Log.get().error(ex.getMessage(), ex);
                return true;
            }
            List<List<Value>> indexesDB = result.getContents();
            if (indexesDB.size() > 0) {
                return true;
            }
            return false;
        } else {
            ar.cscSetModeBest();
            Form formBest;
            try {
                formBest = ar.getForm(formName);
            } catch (ARException ex) {
                Log.get().error(ex.getMessage(), ex);
                return true;
            }
            List<IndexInfo> indices = formBest.getIndexInfo();
            for (IndexInfo indice : indices) {
                List<Integer> campos = indice.getIndexFields();
                if (campos.equals(ids)) {
                    return true;
                }
            }
        }
        return false;
    }

    private void createIndex(List<Integer> ids) {

        Log.get().info(" Indexando " + formName + " " + ids.toString());

        String cust;
        try {
            cust = ar.cscGetCustomization(form);
        } catch (ARException ex) {
            Log.get().error(ex.getMessage(), ex);
            return;
        }

        Form formBase = null;
        List<IndexInfo> indexBase = null;
        if (cust.equalsIgnoreCase("unmodified") || cust.equalsIgnoreCase("overlaid")) {
            ar.cscSetModeBase();
            ar.setGranularMode(Constants.AR_GRANULAR_INDEX);
            try {
                formBase = ar.getForm(formName);
            } catch (ARException ex) {
                Log.get().error(ex.getMessage(), ex);
                ar.cscSetModeBest();
                ar.setGranularMode(0);
                return;
            }

            indexBase = formBase.getIndexInfo();
            ar.cscSetModeBest();
            ar.setGranularMode(0);
        }

        Form formBest = null;
        List<IndexInfo> indexBest = null;
        ObjectPropertyMap propertiesBest = null;

        if (!cust.equalsIgnoreCase("unmodified")) {
            ar.cscSetModeBest();
            ar.setGranularMode(Constants.AR_GRANULAR_INDEX);
            try {
                formBest = ar.getForm(formName);
            } catch (ARException ex) {
                Log.get().error(ex.getMessage(), ex);
                ar.cscSetModeBest();
                ar.setGranularMode(0);
                return;
            }

            indexBest = formBest.getIndexInfo();
            propertiesBest = formBest.getProperties();
            ar.cscSetModeBest();
            ar.setGranularMode(0);
        }

        IndexInfo newIndex = new IndexInfo(ids, false, "");
        if (seExistirIndiceRemove(indexBase, newIndex)) {
            salvaIndexBase(formBase, indexBase);
        }
        if (seExistirIndiceRemove(indexBest, newIndex)) {
            if (cust.equalsIgnoreCase("overlaid")) {
                salvaIndexBest(formBest, propertiesBest, indexBest);
            }
            if (cust.equalsIgnoreCase("custom")) {
                salvaIndexBest(formBest, indexBest);
            }
        }

        if (cust.equalsIgnoreCase("unmodified")) {
            indexBase.add(newIndex);
            salvaIndexBase(formBase, indexBase);
        }
        if (cust.equalsIgnoreCase("overlaid")) {
            indexBest.add(newIndex);
            salvaIndexBest(formBest, propertiesBest, indexBest);
        }
        if (cust.equalsIgnoreCase("custom")) {
            indexBest.add(newIndex);
            salvaIndexBest(formBest, indexBest);
        }
        Log.get().info("     " + "OK");
    }

    private boolean seExistirIndiceRemove(List<IndexInfo> indexes, IndexInfo index) {
        List<IndexInfo> temp = new ArrayList<>(indexes);
        indexes.clear();
        boolean retorno = false;
        for (IndexInfo i : temp) {
            if (i.getIndexFields().equals(index.getIndexFields())) {
                retorno = true;
                continue;
            }
            indexes.add(i);
        }
        return retorno;
    }

    private void salvaIndexBase(Form formBase, List<IndexInfo> indexBase) {
        formBase.setIndexInfo(indexBase);
        ar.cscSetModeBase();
        ar.setGranularMode(Constants.AR_GRANULAR_INDEX);
        try {
            // Cria o indice em base
            ar.setForm(formBase);
        } catch (ARException ex) {
            Log.get().error(ex.getMessage(), ex);
            ar.cscSetModeBest();
            ar.setGranularMode(0);
            return;
        }
        ar.cscSetModeBest();
        ar.setGranularMode(0);
    }

    private void salvaIndexBest(Form formBest, ObjectPropertyMap propertiesBest, List<IndexInfo> indexBest) {
        int arOpropOverlayExtendMask = propertiesBest.getOrDefault(Constants.AR_OPROP_OVERLAY_EXTEND_MASK, new Value(0)).getIntValue();
        int arOpropOverlayInheritMask = propertiesBest.getOrDefault(Constants.AR_OPROP_OVERLAY_INHERIT_MASK, new Value(Constants.AR_GRANULAR_PERMISSIONS | Constants.AR_GRANULAR_INDEX | Constants.AR_GRANULAR_OTHER)).getIntValue();
        int arOpropOverlayExtendMaskNew = (arOpropOverlayExtendMask | Constants.AR_GRANULAR_INDEX);
        int arOpropOverlayInheritMaskNew = (arOpropOverlayInheritMask & (~Constants.AR_GRANULAR_INDEX));
        boolean additive = false;
        if (arOpropOverlayExtendMask != arOpropOverlayExtendMaskNew || arOpropOverlayInheritMask != arOpropOverlayInheritMaskNew) {
            additive = true;
        }
        formBest.setProperties(propertiesBest);
        try {
            // Coloca o additive
            ar.cscSetModeBest();
            if (additive) {
                ar.setForm(formBest);
            }
        } catch (ARException ex) {
            Log.get().error(ex.getMessage(), ex);
            ar.cscSetModeBest();
            ar.setGranularMode(0);
            return;
        }

        formBest.setIndexInfo(indexBest);

        ar.cscSetModeBest();
        ar.setGranularMode(0);
        try {
            // Cria o indice em best
            ar.setForm(formBest);
        } catch (ARException ex) {
            Log.get().error(ex.getMessage(), ex);
            ar.cscSetModeBest();
            ar.setGranularMode(0);
            return;
        }
        ar.cscSetModeBest();
        ar.setGranularMode(0);
    }

    private void salvaIndexBest(Form formBest, List<IndexInfo> indexBest) {
        formBest.setIndexInfo(indexBest);
        ar.cscSetModeBest();
        ar.setGranularMode(0);
        try {
            // Cria o indice em best
            ar.setForm(formBest);
        } catch (ARException ex) {
            Log.get().error(ex.getMessage(), ex);
            ar.cscSetModeBest();
            ar.setGranularMode(0);
            return;
        }
        ar.cscSetModeBest();
        ar.setGranularMode(0);
    }

    private void retiraIndicesPiratas() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
