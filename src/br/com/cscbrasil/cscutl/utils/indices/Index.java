/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.utils.indices;

import java.util.List;
import java.util.Objects;

/**
 *
 * @author ivan
 */
public class Index {

    Schema schema;
    String unique;
    String indexCustomization;
    List<Integer> fields;

    public Index(Schema schema, String unique, String indexCustomization, List<Integer> fields) {        
        this.unique = unique;
        this.indexCustomization = indexCustomization;
        this.fields = fields;
        this.schema = schema;
    }

    public String getUnique() {
        return unique;
    }

    public void setUnique(String unique) {
        this.unique = unique;
    }

    public String getIndexCustomization() {
        return indexCustomization;
    }

    public String getIndexCustomization2() {

        String customization = schema.getCustomization();
        String indexCustomization2 = getIndexCustomization();

        if (unique.equals("YES")) {
            if (customization.equals("Custom")) {
                return "Custom";
            } else {
                return "Base";
            }
        }

        if (indexCustomization2.equals("N/A")) {
            if (fields.size() == 1 && fields.get(0) == 1) {
                if (customization.equals("Custom")) {
                    return "Custom";
                } else {
                    return "Base";
                }
            }
            if (customization.equals("Custom")) {
                return "Custom";
            } else {
                return "Overlaid";
            }
        }

        if (indexCustomization2.equals("Overlaid")) {
            if (customization.equals("Custom")) {
                return "Custom";
            } else {
                return "Overlaid";
            }
        }
        if (indexCustomization2.equals("Unmodified")) {
            if (customization.equals("Custom")) {
                return "Custom";
            } else {
                return "Base";
            }
        }
        return indexCustomization2;
    }

    public void setIndexCustomization(String indexCustomization) {
        this.indexCustomization = indexCustomization;
    }

    public List<Integer> getFields() {
        return fields;
    }

    public void setFields(List<Integer> fields) {
        this.fields = fields;
    }

    @Override
    public int hashCode() {
        int hash = 7;        
        hash = 41 * hash + Objects.hashCode(this.unique);
        hash = 41 * hash + Objects.hashCode(this.indexCustomization);
        hash = 41 * hash + Objects.hashCode(this.fields);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Index other = (Index) obj;
        if (!Objects.equals(this.unique, other.unique)) {
            return false;
        }
        if (!Objects.equals(this.indexCustomization, other.indexCustomization)) {
            return false;
        }
        if (!Objects.equals(this.fields, other.fields)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        if (getIndexCustomization2().equals("Base")) {
            return "A" + fields.toString();
        }
        if (getIndexCustomization2().equals("Overlaid")) {
            return "B" + fields.toString();
        }
        return "C" + fields.toString();
    }

}
