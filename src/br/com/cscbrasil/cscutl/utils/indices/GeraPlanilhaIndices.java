package br.com.cscbrasil.cscutl.utils.indices;

import br.com.cscbrasil.bmc.CmdbInfo;
import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import br.com.cscbrasil.common.SortStringIgnoreCase;
import static br.com.cscbrasil.cscutl.utils.indices.Formatos.*;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Field;
import com.bmc.arsys.api.FieldCriteria;
import com.bmc.arsys.api.Form;
import com.bmc.arsys.api.IndexInfo;
import com.bmc.arsys.api.SQLResult;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jxl.Workbook;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class GeraPlanilhaIndices {

    public static int SYNCREPORT_EXPURGADO_CONTROLE_ID_FLAG = 555555000;
    public static int SYNCREPORT_EXPURGADO_CONTROLE_ID_TAG = 555555001;
    public static int SYNCREPORT_EXPURGADO_CONTROLE_ID_RAIZ = 555555002;
    public static int SYNCREPORT_EXPURGADO_CONTROLE_ID_MAX = 555555002;

    RemedyConnection rmd;
    WritableWorkbook workbook;
    WritableSheet sheet;
    int linha;

    public GeraPlanilhaIndices() {
        rmd = RemedyConnections.get();
        rmd.sourc.cscSetModeBest();
    }

    public void execute() throws IOException, WriteException, ARException {
        workbook = Workbook.createWorkbook(new File("indices_consolidados_01_fonte.xls"));
        sheet = workbook.createSheet(rmd.sourc.getServer(), 0);
        sheet.addCell(new jxl.write.Label(0, 0, "Schema", fmtTitulo));
        sheet.setColumnView(0, 61);
        sheet.addCell(new jxl.write.Label(1, 0, "Schema Customization", fmtTitulo));
        sheet.setColumnView(1, 19);
        sheet.addCell(new jxl.write.Label(2, 0, "Class ?", fmtTitulo));
        sheet.setColumnView(2, 6);
        sheet.addCell(new jxl.write.Label(3, 0, "Index", fmtTitulo));
        sheet.setColumnView(3, 20);
        sheet.addCell(new jxl.write.Label(4, 0, "Unique ?", fmtTitulo));
        sheet.setColumnView(4, 8);
        sheet.addCell(new jxl.write.Label(5, 0, "Exists DB ?", fmtTitulo));
        sheet.setColumnView(5, 11);
        sheet.addCell(new jxl.write.Label(6, 0, "Exists ARS ?", fmtTitulo));
        sheet.setColumnView(6, 11);
        sheet.addCell(new jxl.write.Label(7, 0, "Index Customization", fmtTitulo));
        sheet.setColumnView(7, 17);
        for (int f = 1; f <= 16; f++) {
            int c = ((f - 1) * 3) + 1;
            sheet.addCell(new jxl.write.Label(c + 7, 0, String.format("F%d", f), fmtTitulo));
            sheet.setColumnView(c + 7, 11);
            sheet.addCell(new jxl.write.Label(c + 8, 0, String.format("FName%d", f), fmtTitulo));
            sheet.setColumnView(c + 8, 30);
            sheet.addCell(new jxl.write.Label(c + 9, 0, String.format("O%d", f), fmtTitulo));
            sheet.setColumnView(c + 9, 5);
        }
        linha = 0;
        List<String> listForm = rmd.sourc.getListForm(0, Constants.AR_LIST_SCHEMA_REGULAR | Constants.AR_HIDDEN_INCREMENT);
        java.util.Collections.sort(listForm, new SortStringIgnoreCase());
        for (String formName : listForm) {
            //if (formName.equals("WOI:WorkOrder") || formName.equals("GSC_SRM_Request") || formName.equals("BMC.CORE:BMC_BaseElement")) {
            consolida(formName);
            //}
        }
        workbook.write();
        workbook.close();
    }

    private void consolida(String formName) throws ARException, WriteException {
        Log.get().info(formName);
        int formNumber = rmd.sourc.cscGetFormNumber(formName);
        String customization = rmd.sourc.cscGetCustomization(formName);
        CmdbInfo cmdbInfo = rmd.sourc.cscGetCmdbInfo(formName);

        // Indices em Base
        rmd.sourc.cscSetModeBase();
        List<IndexInfo> indexInfosBase;
        try {
            Form formBase = rmd.sourc.getForm(formName);
            indexInfosBase = formBase.getIndexInfo();
        } catch (ARException ex) {
            indexInfosBase = new ArrayList<>();
        }

        // Indices registrados no Remedy (Todos)
        rmd.sourc.cscSetModeBest();
        Form form = rmd.sourc.getForm(formName);
        List<IndexInfo> indexInfos = form.getIndexInfo();

        // Processar Indices no banco de dados        
        String q = String.format("select a.index_name,a.column_name,a.column_position,a.descend,b.uniqueness from user_ind_columns a inner join user_indexes b on b.table_name=a.table_name and b.index_name=a.index_name where a.table_name='T%d' order by a.index_name,a.column_position", formNumber);
        SQLResult listSQL = rmd.sourc.getListSQL(q, 999999999, false);
        List<List<Value>> contents = listSQL.getContents();
        Map<String, List<Integer>> dbIndexInfos = new HashMap<>();
        for (List<Value> content : contents) {
            String index_name = content.get(0).toString();
            String column_name = content.get(1).toString();
            String order;
            String descend = content.get(3).toString();
            if (descend.equals("ASC")) {
                order = "ASC";
            } else {
                order = "DESC";
            }
            int fieldID;
            try {
                fieldID = Integer.parseInt(column_name.substring(1));
            } catch (NumberFormatException ex) {
                fieldID = 0;
            }
            if (order.equals("DESC")) {
                fieldID = fieldID * -1;
            }
            if (!dbIndexInfos.containsKey(index_name)) {
                dbIndexInfos.put(index_name, new ArrayList<>());
            }
            dbIndexInfos.get(index_name).add(fieldID);
        }

        for (List<Value> content : contents) {
            String dbIndexName = content.get(0).toString();
            String column_name = content.get(1).toString();
            int fieldID;
            try {
                fieldID = Integer.parseInt(column_name.substring(1));
            } catch (NumberFormatException ex) {
                fieldID = 0;
            }
            Field field;
            try {
                FieldCriteria fl = new FieldCriteria();
                fl.setPropertiesToRetrieve(FieldCriteria.NAME);
                field = rmd.sourc.getField(formName, fieldID, fl);
            } catch (ARException ex) {
                field = null;
            }
            String fieldName;
            if (field == null) {
                fieldName = "???INVALID???";
            } else {
                fieldName = field.getName();
            }
            int column_position = content.get(2).getIntValue();
            String descend = content.get(3).toString();
            String unique = content.get(4).toString();
            String order;
            if (descend.equals("ASC")) {
                order = "ASC";
            } else {
                order = "DESC";
            }
            if (unique.equals("UNIQUE")) {
                unique = "YES";
            } else {
                unique = "NO";
            }

            String cmdb;
            if (cmdbInfo.isCMDB()) {
                cmdb = "YES";
            } else {
                cmdb = "NO";
            }
            int c = ((column_position - 1) * 3) + 1;
            if (column_position == 1) {
                String existsARS = existsARS(formName, formNumber, indexInfos, dbIndexName, unique);
                String nativo;
                if (existsARS.equals("YES")) {
                    String existsARS1 = existsARS(formName, formNumber, indexInfosBase, dbIndexName, unique);
                    if (existsARS1.equals("YES")) {
                        nativo = "Unmodified";
                    } else if (!customization.equals("custom")) {
                        nativo = "Overlaid";
                    } else {
                        nativo = "Custom";
                    }
                } else {
                    nativo = "N/A";
                }
                linha++;
                sheet.addCell(new jxl.write.Label(0, linha, formName, fmtPadrao));
                sheet.addCell(new jxl.write.Label(1, linha, customization, fmtPadraoCentro));
                sheet.addCell(new jxl.write.Label(2, linha, cmdb, fmtPadraoCentro));
                sheet.addCell(new jxl.write.Label(3, linha, dbIndexName, fmtPadrao));
                sheet.addCell(new jxl.write.Label(4, linha, unique, fmtPadraoCentro));
                sheet.addCell(new jxl.write.Label(5, linha, "YES", fmtPadraoCentro));
                sheet.addCell(new jxl.write.Label(6, linha, existsARS, fmtPadraoCentro));
                sheet.addCell(new jxl.write.Label(7, linha, nativo, fmtPadraoCentro));
                if (fieldID != 0) {
                    sheet.addCell(new jxl.write.Number(c + 7, linha, fieldID, fmtInteiro));
                } else {
                    sheet.addCell(new jxl.write.Label(c + 7, linha, column_name, fmtPadrao));
                }
                sheet.addCell(new jxl.write.Label(c + 8, linha, fieldName, fmtPadrao));
                sheet.addCell(new jxl.write.Label(c + 9, linha, order, fmtPadraoCentro));
            } else {
                if (fieldID != 0) {
                    sheet.addCell(new jxl.write.Number(c + 7, linha, fieldID, fmtInteiro));
                } else {
                    sheet.addCell(new jxl.write.Label(c + 7, linha, column_name, fmtPadrao));
                }
                sheet.addCell(new jxl.write.Label(c + 8, linha, fieldName, fmtPadrao));
                sheet.addCell(new jxl.write.Label(c + 9, linha, order, fmtPadraoCentro));
            }
        }

        // Processar Indices Remedy (os que nao existirem no banco)
        for (IndexInfo indexInfo : indexInfos) {
            List<Integer> indexFields = indexInfo.getIndexFields();
            String existsDB = existsDB(indexInfo, dbIndexInfos);
            if (!existsDB.equals("YES")) {
                String unique;
                if (indexInfo.isUnique()) {
                    unique = "YES";
                } else {
                    unique = "NO";
                }
                String nativo;
                String existsARS1 = existsARS(formName, formNumber, indexInfosBase, indexInfo.getIndexName(), unique);
                if (existsARS1.equals("YES")) {
                    nativo = "Unmodified";
                } else if (!customization.equals("custom")) {
                    nativo = "Overlaid";
                } else {
                    nativo = "Custom";
                }

                String cmdb;
                if (cmdbInfo.isCMDB()) {
                    cmdb = "YES";
                } else {
                    cmdb = "NO";
                }
                linha++;
                sheet.addCell(new jxl.write.Label(0, linha, formName, fmtPadrao));
                sheet.addCell(new jxl.write.Label(1, linha, customization, fmtPadraoCentro));
                sheet.addCell(new jxl.write.Label(2, linha, cmdb, fmtPadraoCentro));
                sheet.addCell(new jxl.write.Label(3, linha, indexInfo.getIndexName(), fmtPadrao));
                sheet.addCell(new jxl.write.Label(4, linha, unique, fmtPadraoCentro));
                sheet.addCell(new jxl.write.Label(5, linha, existsDB, fmtPadraoCentro));
                sheet.addCell(new jxl.write.Label(6, linha, "YES", fmtPadraoCentro));
                sheet.addCell(new jxl.write.Label(7, linha, nativo, fmtPadraoCentro));
                int column_position = 1;
                for (Integer fieldID : indexFields) {
                    Field field;
                    try {
                        FieldCriteria fl = new FieldCriteria();
                        fl.setPropertiesToRetrieve(FieldCriteria.NAME);
                        field = rmd.sourc.getField(formName, fieldID, fl);
                    } catch (ARException ex) {
                        field = null;
                    }
                    String fieldName;
                    if (field == null) {
                        fieldName = "???INVALID???";
                    } else {
                        fieldName = field.getName();
                    }

                    int c = ((column_position - 1) * 3) + 1;
                    sheet.addCell(new jxl.write.Number(c + 7, linha, fieldID, fmtInteiro));
                    sheet.addCell(new jxl.write.Label(c + 8, linha, fieldName, fmtPadrao));
                    sheet.addCell(new jxl.write.Label(c + 9, linha, "ASC", fmtPadraoCentro));
                    column_position++;
                }
            }
        }

        // Indices recomendados
        List<List<Integer>> recomendados;
        recomendados = new ArrayList<>();

        List<Integer> indice;
        indice = new ArrayList<>();
        indice.add(1);
        recomendados.add(indice);

        indice = new ArrayList<>();
        indice.add(6);
        recomendados.add(indice);

        indice = new ArrayList<>();
        indice.add(179);
        recomendados.add(indice);

        indice = new ArrayList<>();
        indice.add(3);
        recomendados.add(indice);

        indice = new ArrayList<>();
        indice.add(7);
        recomendados.add(indice);

        indice = new ArrayList<>();
        indice.add(SYNCREPORT_EXPURGADO_CONTROLE_ID_FLAG);
        recomendados.add(indice);

        indice = new ArrayList<>();
        indice.add(SYNCREPORT_EXPURGADO_CONTROLE_ID_FLAG);
        indice.add(SYNCREPORT_EXPURGADO_CONTROLE_ID_TAG);
        indice.add(SYNCREPORT_EXPURGADO_CONTROLE_ID_RAIZ);
        recomendados.add(indice);

        indice = new ArrayList<>();
        indice.add(SYNCREPORT_EXPURGADO_CONTROLE_ID_FLAG);
        indice.add(SYNCREPORT_EXPURGADO_CONTROLE_ID_RAIZ);
        recomendados.add(indice);

        for (List<Integer> recomendado : recomendados) {
            if (inexistente(formName, recomendado, indexInfos, dbIndexInfos)) {
                String cmdb;
                if (cmdbInfo.isCMDB()) {
                    cmdb = "YES";
                } else {
                    cmdb = "NO";
                }
                linha++;
                sheet.addCell(new jxl.write.Label(0, linha, formName, fmtPadrao));
                sheet.addCell(new jxl.write.Label(1, linha, customization, fmtPadraoCentro));
                sheet.addCell(new jxl.write.Label(2, linha, cmdb, fmtPadraoCentro));
                sheet.addCell(new jxl.write.Label(3, linha, String.format("I%d_%d_%d", formNumber, recomendado.get(0), linha), fmtPadrao));
                sheet.addCell(new jxl.write.Label(4, linha, "NO", fmtPadraoCentro));
                sheet.addCell(new jxl.write.Label(5, linha, "NO", fmtPadraoCentro));
                sheet.addCell(new jxl.write.Label(6, linha, "NO", fmtPadraoCentro));
                if (recomendado.size() == 1 && recomendado.get(0) == 1) {
                    sheet.addCell(new jxl.write.Label(7, linha, "N/A", fmtPadraoCentro));
                } else {
                    sheet.addCell(new jxl.write.Label(7, linha, "Overlaid", fmtPadraoCentro));
                }
                int column_position = 1;
                for (Integer fieldID : recomendado) {
                    Field field;
                    try {
                        FieldCriteria fl = new FieldCriteria();
                        fl.setPropertiesToRetrieve(FieldCriteria.NAME);
                        field = rmd.sourc.getField(formName, fieldID, fl);
                    } catch (ARException ex) {
                        field = null;
                    }
                    String fieldName;
                    if (field == null) {
                        fieldName = "???INVALID???";
                    } else {
                        fieldName = field.getName();
                    }

                    int c = ((column_position - 1) * 3) + 1;
                    sheet.addCell(new jxl.write.Number(c + 7, linha, fieldID, fmtInteiro));
                    sheet.addCell(new jxl.write.Label(c + 8, linha, fieldName, fmtPadrao));
                    sheet.addCell(new jxl.write.Label(c + 9, linha, "ASC", fmtPadraoCentro));
                    column_position++;
                }
            }
        }

    }

    private String existsARS(String formName, int formNumber, List<IndexInfo> indexInfos, String dbIndexName, String unique) throws ARException {
        if (formNumber == 0) {
            formNumber = rmd.sourc.cscGetFormNumber(formName);
        }
        if (indexInfos == null) {
            Form form = rmd.sourc.getForm(formName);
            indexInfos = form.getIndexInfo();
        }

        // Lista dos campos
        String q = String.format("select column_position,column_name,descend from user_ind_columns where table_name='T%d' and index_name='%s' order by column_Position", formNumber, dbIndexName);
        SQLResult listSQL;
        listSQL = rmd.sourc.getListSQL(q, 999999999, false);
        List<List<Value>> contents = listSQL.getContents();
        List<Integer> dbIndex = new ArrayList<>();
        for (List<Value> content : contents) {
            int fieldId;
            try {
                fieldId = Integer.parseInt(content.get(1).toString().substring(1));
            } catch (NumberFormatException ex) {
                fieldId = 0;
            }
            dbIndex.add(fieldId);
        }

        if (dbIndex.size() == 1) {
            if (dbIndex.get(0) == 1) {
                if (unique.equals("YES")) {
                    // indice pelo request id nao fica registrado no ars
                    return "PK";
                }
                return "UNIQUENESS ERROR";
            }
        }

        for (IndexInfo indexInfo : indexInfos) {
            List<Integer> indexFields = indexInfo.getIndexFields();
            if (indexFields.equals(dbIndex)) {
                if ((indexInfo.isUnique() && unique.equals("YES")) || (!indexInfo.isUnique() && unique.equals("NO"))) {
                    return "YES";
                }
                return "UNIQUENESS ERROR";
            }
        }
        return "NO";

    }

    private String existsDB(IndexInfo indexInfo, Map<String, List<Integer>> dbIndexInfos) {

        List<Integer> indexFields = indexInfo.getIndexFields();

        for (Map.Entry<String, List<Integer>> entry : dbIndexInfos.entrySet()) {
            String key = entry.getKey();
            List<Integer> value = entry.getValue();
            if (value.equals(indexFields)) {
                if (key.equals(indexInfo.getIndexName())) {
                    return "YES";
                }
                return "NAME ERROR";
            }
        }
        return "NO";
    }

    private boolean inexistente(String formName, List<Integer> recomendado, List<IndexInfo> indexInfos, Map<String, List<Integer>> dbIndexInfos) {
        // existem todos os campos ??
        for (Integer fieldID : recomendado) {
            Field field;
            try {
                FieldCriteria fl = new FieldCriteria();
                fl.setPropertiesToRetrieve(FieldCriteria.NAME);
                field = rmd.sourc.getField(formName, fieldID, fl);
            } catch (ARException ex) {
                return false; // faz de conta que o indice existe pois o campo nao existe                
            }
        }

        // existe no banco ?
        for (Map.Entry<String, List<Integer>> entry : dbIndexInfos.entrySet()) {
            String key = entry.getKey();
            List<Integer> value = entry.getValue();
            if (value.equals(recomendado)) {
                return false;
            }
        }

        // Existe no remedy ?
        for (IndexInfo indexInfo : indexInfos) {
            List<Integer> indexFields = indexInfo.getIndexFields();
            if (indexFields.equals(recomendado)) {
                return false;
            }
        }

        return true;
    }
}
