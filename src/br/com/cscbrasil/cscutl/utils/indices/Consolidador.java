/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.utils.indices;

import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import static br.com.cscbrasil.cscutl.utils.indices.Formatos.fmtInteiro;
import static br.com.cscbrasil.cscutl.utils.indices.Formatos.fmtPadrao;
import static br.com.cscbrasil.cscutl.utils.indices.Formatos.fmtPadraoCentro;
import static br.com.cscbrasil.cscutl.utils.indices.Formatos.fmtTitulo;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Field;
import com.bmc.arsys.api.FieldCriteria;
import com.marzapower.loggable.Log;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.biff.EmptyCell;
import jxl.read.biff.BiffException;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

/**
 *
 * @author ivan
 */
public class Consolidador {

    List<Schema> schemas;
    Workbook workbookRead;
    WritableWorkbook workbookWrite;
    RemedyConnection rmd;

    private void openWorkBookRead() throws IOException, BiffException {
        File old = new File("indices_consolidados_01_fonte.xls");
        File novo = new File("temp.xls");
        novo.delete();
        old.renameTo(novo);
        workbookRead = Workbook.getWorkbook(novo);
    }

    private void closeWorkBookRead() throws IOException, BiffException {
        workbookRead.close();
        File novo = new File("temp.xls");
        novo.delete();
    }

    private void openWorkBookWrite() throws IOException, BiffException {
        workbookWrite = Workbook.createWorkbook(new File("indices_consolidados_01_fonte.xls"), workbookRead);
        closeWorkBookRead();
    }

    private void closeWorkBookWrite() throws IOException, WriteException {
        workbookWrite.write();
        workbookWrite.close();
    }

    public Consolidador() throws IOException, BiffException {
        rmd = RemedyConnections.get();
        schemas = new ArrayList<>();
        openWorkBookRead();
        Sheet fonte = workbookRead.getSheet(0);
        for (int l = 1; l < fonte.getRows(); l++) {
            String xlsFormName = fonte.getCell(0, l).getContents();
            String xlsSchemaCustomization = fonte.getCell(1, l).getContents();
            String xlsIsClass = fonte.getCell(2, l).getContents();
            String xlsIndexName = fonte.getCell(3, l).getContents();
            String xlsIsUnique = fonte.getCell(4, l).getContents();
            String xlsIndexCustomization = fonte.getCell(7, l).getContents();
            //if (xlsFormName.equals("GSC:RF001-INT_PESSOAS")){
            //    int a=0;
            //}
            Schema schema = new Schema(xlsFormName, xlsSchemaCustomization, xlsIsClass);
            if (!schemas.contains(schema)) {
                schemas.add(schema);
            } else {
                schema = schemas.get(schemas.indexOf(schema));
            }
            List<Integer> indice = new ArrayList<>();
            for (int c = 8; c < fonte.getColumns(); c += 3) {
                Cell cell = fonte.getCell(c, l);
                if (cell instanceof EmptyCell) {
                    break;
                }
                int fieldId;
                try {
                    fieldId = Integer.parseInt(cell.getContents());
                } catch (NumberFormatException ex) {
                    Log.get().error(ex);
                    indice.clear();
                    break;
                }
                indice.add(fieldId);
            }
            if (!indice.isEmpty()) {
                Index index = new Index(schema, xlsIsUnique, xlsIndexCustomization, indice);
                if (!schema.todos.contains(index)) {
                    schema.todos.add(index);
                }
                if (xlsIndexCustomization.equalsIgnoreCase("unmodified") || xlsIndexCustomization.equalsIgnoreCase("N/A")) {
                    if (!schema.nativos.contains(index)) {
                        schema.nativos.add(index);
                    }
                } else if (!schema.custom.contains(index)) {
                    schema.custom.add(index);
                }
            }
        }
    }

    public void aplica() throws IOException, WriteException, BiffException {

    }

    public void consolida() throws IOException, WriteException, BiffException {
        retiraDuplicatas();
        openWorkBookWrite();
        WritableSheet sheet = workbookWrite.createSheet("Consolidado", 1);
        sheet.addCell(new jxl.write.Label(0, 0, "Schema", fmtTitulo));
        sheet.setColumnView(0, 61);
        sheet.addCell(new jxl.write.Label(1, 0, "API", fmtTitulo));
        sheet.setColumnView(1, 6);
        sheet.addCell(new jxl.write.Label(2, 0, "Unique ?", fmtTitulo));
        sheet.setColumnView(2, 8);
        sheet.addCell(new jxl.write.Label(3, 0, "Index Customization", fmtTitulo));
        sheet.setColumnView(3, 17);
        for (int f = 1; f <= 16; f++) {
            int c = ((f - 1) * 3) + 1;
            sheet.addCell(new jxl.write.Label(c + 3, 0, String.format("F%d", f), fmtTitulo));
            sheet.setColumnView(c + 3, 11);
            sheet.addCell(new jxl.write.Label(c + 4, 0, String.format("FName%d", f), fmtTitulo));
            sheet.setColumnView(c + 4, 30);
            sheet.addCell(new jxl.write.Label(c + 5, 0, String.format("O%d", f), fmtTitulo));
            sheet.setColumnView(c + 5, 5);
        }
        int linha = 0;
        for (Schema schema : schemas) {
            //if (schema.getSchema().equals("GSC:RF001-INT_PESSOAS")) {
                            Collections.sort(schema.todos, new Comparator<Index>() {
                    @Override
                    public int compare(Index t, Index t1) {
                        String c = t.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(",", " ") + " ";
                        String c1 = t1.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(",", " ") + " ";
                        return c.toString().compareTo(c1.toString());
                    }

                });
            //} else {
            //    continue;
            //}
            for (Index index : schema.todos) {
                String customization = index.getIndexCustomization2();
                linha++;
                sheet.addCell(new jxl.write.Label(0, linha, schema.getSchema(), fmtPadrao));
                sheet.addCell(new jxl.write.Label(1, linha, schema.getAPI(), fmtPadraoCentro));
                sheet.addCell(new jxl.write.Label(2, linha, index.getUnique(), fmtPadrao));
                sheet.addCell(new jxl.write.Label(3, linha, customization, fmtPadraoCentro));
                List<Integer> fields = index.getFields();
                int f = 0;
                for (Integer fld : fields) {
                    f++;
                    int c = ((f - 1) * 3) + 1;
                    Field field;
                    try {
                        FieldCriteria fl = new FieldCriteria();
                        fl.setPropertiesToRetrieve(FieldCriteria.OPTION);
                        field = rmd.sourc.getField(schema.getSchema(), fld, fl);
                    } catch (ARException ex) {
                        field = null;
                    }
                    String fieldName;
                    if (field == null) {
                        fieldName = "???INVALID???";
                    } else {
                        fieldName = field.getName();
                    }
                    sheet.addCell(new jxl.write.Number(c + 3, linha, fld, fmtInteiro));
                    sheet.setColumnView(c + 3, 11);
                    sheet.addCell(new jxl.write.Label(c + 4, linha, fieldName, fmtPadrao));
                    sheet.setColumnView(c + 4, 30);
                    sheet.addCell(new jxl.write.Label(c + 5, linha, "ASC"));
                    sheet.setColumnView(c + 5, 5);
                }
            }

        }

        closeWorkBookWrite();
    }

    public void retiraDuplicatas() {
        for (Schema schema : schemas) {
            //if (schema.getSchema().equals("WOI:WorkOrder")) {
            schema.retiraDuplicatas();
            //}
        }
    }

}
