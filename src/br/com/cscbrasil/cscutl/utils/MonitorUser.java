/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.utils;

import br.com.cscbrasil.bmc.ARServerUser;
import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.SQLResult;
import com.bmc.arsys.api.SortInfo;
import com.bmc.arsys.api.Timestamp;
import com.bmc.arsys.api.UserInfo;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ivan
 */
public class MonitorUser {

    public void monitor() throws ARException {
        RemedyConnection rmd = RemedyConnections.get();

        String sqlCommand = "select distinct(server) from AR_System_Server_Group_Operati order by server";
        SQLResult listSQL = rmd.desti.getListSQL(sqlCommand, 999999999, false);
        List<List<Value>> contents = listSQL.getContents();

        Map<String, ARServerUser> servers = new HashMap<>();

        for (List<Value> content : contents) {
            Log.get().info("Criando conexao com " + content.get(0).toString());
            ARServerUser ar = new ARServerUser(rmd.desti.getUser(), rmd.desti.getPassword(), rmd.desti.getAuthentication(), rmd.desti.getLocale(), content.get(0).toString(), rmd.desti.getPort());
            ar.logout();
            ar.login();
            servers.put(content.get(0).toString(), ar);
        }

        Iterator it = servers.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            ARServerUser ar = (ARServerUser) pair.getValue();
            it.remove();
            
            
            
            
        }

        /*
        Map<String, Long> u = new HashMap<>();
        List<UserInfo> listUser = rmd.desti.getListUser(Constants.AR_USER_LIST_REGISTERED, 0);
        for (UserInfo user : listUser) {
            Timestamp connectionTime = user.getConnectionTime();
            if (connectionTime.getValue() != 0) {
                u.put(user.getUserName(), user.getConnectionTime().getValue());
            }
        }
        System.out.println(u.size());
        */
    }

}
