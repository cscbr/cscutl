/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.utils.searchdbstring;

import br.com.cscbrasil.bmc.ARServerUser;
import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import br.com.cscbrasil.cscutl.common.SortFormByNameIgnoreCase;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Form;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

/**
 *
 * @author ivan
 */
public class SearchStringDb {

    String pesquisa;
    String troca;
    RemedyConnection rmd;
    ARServerUser ar;

    public SearchStringDb(String pesquisa, String troca) {
        this.pesquisa = pesquisa;
        this.troca = troca;
        rmd = RemedyConnections.get();
        ar = rmd.sourc;
    }

    public void execute() throws ARException {
        List<Form> listFormObjects = ar.getListFormObjects(0, Constants.AR_LIST_SCHEMA_REGULAR | Constants.AR_HIDDEN_INCREMENT, null, null, null);
        Collections.sort(listFormObjects, new SortFormByNameIgnoreCase());
        ForkJoinPool forkJoinPool = new ForkJoinPool(128, ForkJoinPool.defaultForkJoinWorkerThreadFactory, null, true);
        List<RecursiveAction> actions = new ArrayList<>();
        for (Form form : listFormObjects) {
            //RecursiveAction action = new SearchDb(form, this.pesquisa, this.troca);
            //actions.add(action);
            //forkJoinPool.execute(action);
            SearchDb searchDb = new SearchDb(form, this.pesquisa, this.troca);
            searchDb.compute();
        }
        for (RecursiveAction action : actions) {
            //action.join();
        }
    }
}
