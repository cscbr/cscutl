/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.utils.searchdbstring;

import br.com.cscbrasil.bmc.ARServerUser;
import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import com.bmc.arsys.api.ARErrors;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.CharacterFieldLimit;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Field;
import com.bmc.arsys.api.Form;
import com.bmc.arsys.api.SQLResult;
import com.bmc.arsys.api.StatusInfo;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveAction;

/**
 *
 * @author ivan
 */
public class SearchDb extends RecursiveAction {

    private final Form form;
    private final String pesquisa;
    private final String troca;
    private final RemedyConnection rmd;
    private ARServerUser ar;
    private final int formNumber;

    SearchDb(Form form, String pesquisa, String troca) {
        this.rmd = RemedyConnections.get();
        this.ar = rmd.sourc;
        this.form = form;
        this.formNumber = ar.cscGetFormNumber(form.getName());
        this.pesquisa = pesquisa;
        this.troca = troca;
    }

    @Override
    protected void compute() {
        if (form.getName().contains(":Load")) {
            return;
        }
        //Log.get().info("+ "+ form.getName());
        List<Field> listFieldObjects;
        try {
            listFieldObjects = this.ar.getListFieldObjects(form.getName());
        } catch (ARException ex) {
            Log.get().info("! " + form.getName());
            Log.get().error(ex);
            return;
        }

        List<Field> fields = new ArrayList<>();
        for (Field field : listFieldObjects) {
            if (field.getFieldID() == Constants.AR_CORE_ENTRY_ID || field.getFieldID() == Constants.AR_CORE_STATUS_HISTORY) {
                continue;
            }
            if (field.getDataType() != Constants.AR_DATA_TYPE_CHAR) {
                continue;
            }
            if (field.getFieldOption() == Constants.AR_FIELD_OPTION_DISPLAY) {
                continue;
            }
            CharacterFieldLimit fieldLimit = (CharacterFieldLimit) field.getFieldLimit();
            if (fieldLimit == null) {
                continue;
            }
            if (fieldLimit.getMaxLength() < pesquisa.length()) {
                continue;
            }
            // caso especifico nec
            if (fieldLimit.getMaxLength()!=15){
                continue;
            }
            
            fields.add(field);
        }
        if (fields.isEmpty()) {
            //Log.get().info("- "+ form.getName());
            return;
        }
        String sql;
        if (ar.cscIsOracle()) {
            sql = String.format("select c1 from t%d where ", formNumber);
            for (Field field : fields) {
                sql += String.format("UPPER(c%d) like '%%%s%%' or ", field.getFieldID(), pesquisa.toUpperCase());
            }
            sql = "select * from (" + sql.substring(0, sql.length() - 4) + ") where rownum<=1";
        } else {
            sql = String.format("select top 1 c1 from t%d where ", formNumber);
            for (Field field : fields) {
                sql += String.format("c%d like '%s%%' or ", field.getFieldID(), pesquisa.toUpperCase());
            }
            sql = sql.substring(0, sql.length() - 4);
        }

        SQLResult listSQL;
        try {
            listSQL = ar.getListSQL(sql, 999999999, false);
        } catch (ARException ex) {
            Log.get().info("! " + form.getName());
            Log.get().error(sql, ex);
            return;
        }

        if (listSQL.getContents().isEmpty()) {
            return;
        }

        for (Field field : fields) {
            String sql2;
            if (ar.cscIsOracle()) {
                sql2 = String.format("select * from (select c1 from t%d where UPPER(c%d) like '%%%s%%') where rownum<=1", formNumber, field.getFieldID(), pesquisa.toUpperCase());
            } else {
                sql2 = String.format("select top 1 c1 from t%d where c%d like '%s%%'", formNumber, field.getFieldID(), pesquisa.toUpperCase());
            }
            SQLResult listSQL2;
            try {
                listSQL2 = ar.getListSQL(sql2, 999999999, false);
            } catch (ARException ex) {
                Log.get().info("! " + form.getName());
                Log.get().error(sql2, ex);
                return;
            }

            if (listSQL2.getContents().isEmpty()) {
                continue;
            }

            if (troca == null) {
                //System.out.println (String.format("-- select c%d from t%d where c%d like '%s%%' ", field.getFieldID(), formNumber, field.getFieldID(), pesquisa.toUpperCase()));
                System.out.println("-- " + form.getName() + "   " + field.getFieldID() + " " + field.getName());
                //System.out.println("update t"+formNumber+" set c"+field.getFieldID()+"='PPR'+substring(c"+field.getFieldID()+",4,15) where c"+field.getFieldID()+" like '"+pesquisa.toUpperCase()+"%'\");");
                //Log.get().info(String.format("* %-80s %10d select c%d from t%d where UPPER(c%d) like '%%%s%%' ", form.getName(), field.getFieldID(), field.getFieldID(), formNumber, field.getFieldID(), pesquisa.toUpperCase()));
            } else {
                String sql3 = String.format("update t%d set c%d=replace(replace(c%d,'%s','%s'),'%s','%s') where UPPER(c%d) like '%%%s%%'", formNumber, field.getFieldID(), field.getFieldID(), pesquisa, troca, pesquisa.toUpperCase(), troca.toUpperCase(), field.getFieldID(), pesquisa.toUpperCase());
                Log.get().info(String.format("* %-80s %10d %s", form.getName(), field.getFieldID(), sql3));
                SQLResult listSQL3;
                try {
                    listSQL3 = ar.getListSQL(sql3, 999999999, false);
                } catch (ARException ex) {
                    List<StatusInfo> lastStatus = ex.getLastStatus();
                    if (lastStatus.size() == 1) {
                        if (lastStatus.get(0).getMessageNum() == ARErrors.AR_ERROR_DUP_ON_UNIQUE_INDEX) {
                            String sql4;
                            sql4 = String.format("select c1 from t%d where UPPER(c%d) like '%%%s%%'", formNumber, field.getFieldID(), pesquisa.toUpperCase());
                            SQLResult listSQL4;
                            try {
                                listSQL4 = ar.getListSQL(sql4, 999999999, false);
                            } catch (ARException ex1) {
                                Log.get().info("! " + form.getName());
                                Log.get().error(sql4, ex1);
                                continue;
                            }
                            List<List<Value>> contents = listSQL4.getContents();
                            for (List<Value> content : contents) {
                                String sql5 = String.format("update t%d set c%d=replace(replace(c%d,'%s','%s'),'%s','%s') where c1='%s'", formNumber, field.getFieldID(), field.getFieldID(), pesquisa, troca, pesquisa.toUpperCase(), troca.toUpperCase(), content.get(0).toString());
                                SQLResult listSQL5;
                                try {
                                    listSQL5 = ar.getListSQL(sql5, 999999999, false);
                                } catch (ARException ex1) {
                                    Log.get().info("! " + form.getName());
                                    Log.get().error(sql5,ex1);
                                    continue;
                                }
                            }
                            
                        }
                    }
                    Log.get().info("! " + form.getName());
                    Log.get().error(sql3, ex);
                    return;
                }
            }
        }
    }
}
