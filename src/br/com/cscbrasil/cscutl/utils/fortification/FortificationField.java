/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.utils.fortification;

import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.ARServerUser;
import com.bmc.arsys.api.AttachmentFieldLimit;
import com.bmc.arsys.api.CharacterFieldLimit;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.DiaryFieldLimit;
import com.bmc.arsys.api.Field;
import com.bmc.arsys.api.FieldLimit;
import com.bmc.arsys.api.Form;
import com.bmc.arsys.api.ObjectPropertyMap;
import com.bmc.arsys.api.TableFieldLimit;
import com.bmc.arsys.api.Value;
import java.util.Map;

/**
 *
 * @author ivan
 */
public class FortificationField {

    ARServerUser rmd;
    Field field;
    String fullTextCategoryName;
    int fullTextOptions;
    String fullTextOptionsString;
    boolean stripTagsForFTS;
    boolean literalFTSIndex;
    String customization;

    FortificationField(ARServerUser rmd, Field campo, String modo) throws ARException {
        if (campo.getFieldID() == 1000000829) {
            int a = 0;
        }

        if (modo.equals("base")) {
            rmd.setBaseOverlayFlag(true);
            rmd.setOverlayFlag(false);
            rmd.setOverlayGroup(Constants.AR_OVERLAY_CLIENT_MODE_BASE);
            rmd.setDesignOverlayGroup(Constants.AR_OVERLAY_CLIENT_MODE_BASE);
        } else {
            rmd.setBaseOverlayFlag(false);
            rmd.setOverlayFlag(true);
            rmd.setOverlayGroup("1");
            rmd.setDesignOverlayGroup("1");
        }
        this.rmd = rmd;
        this.field = campo;

        fullTextCategoryName = "";
        fullTextOptions = 0;
        fullTextOptionsString = "";
        stripTagsForFTS = false;
        literalFTSIndex = false;

        ObjectPropertyMap properties = this.field.getObjectProperty();
        FieldLimit limits = this.field.getFieldLimit();
        if (modo.equals("best") && Fortification.getCustomization(properties).startsWith("Overl")) {
            int granularOther = (properties.getOrDefault(Constants.AR_OPROP_OVERLAY_INHERIT_MASK, new Value(Constants.AR_GRANULAR_OTHER)).getIntValue() & Constants.AR_GRANULAR_OTHER);
            if (granularOther > 0) {
                // Overlay = No Overlay, retirar as duplicatas                
                rmd.setBaseOverlayFlag(true);
                rmd.setOverlayFlag(false);
                rmd.setOverlayGroup(Constants.AR_OVERLAY_CLIENT_MODE_BASE);
                rmd.setDesignOverlayGroup(Constants.AR_OVERLAY_CLIENT_MODE_BASE);
                Field fieldBase = rmd.getField(campo.getForm(), campo.getFieldID());
                if (this.field.getDataType() == Constants.AR_DATA_TYPE_CHAR) {
                    limits = new CharacterFieldLimit();
                }
                if (this.field.getDataType() == Constants.AR_DATA_TYPE_DIARY) {
                    limits = new DiaryFieldLimit();
                }
                if (this.field.getDataType() == Constants.AR_DATA_TYPE_ATTACH) {
                    limits = new AttachmentFieldLimit();
                }

                ObjectPropertyMap propertiesBase = fieldBase.getObjectProperty();
                for (Map.Entry<Integer, Value> entry : propertiesBase.entrySet()) {
                    Integer key = entry.getKey();
                    Value value = entry.getValue();
                    properties.remove(key, value);
                }
                rmd.setBaseOverlayFlag(false);
                rmd.setOverlayFlag(true);
                rmd.setOverlayGroup("1");
                rmd.setDesignOverlayGroup("1");
            } else {
                // Overlay = Overwrite nao precisa retirar as duplicatas
            }
        }

        customization = Fortification.getCustomization(properties);
        if (modo.equals("base")) {
            customization = "Base";
        }
        fullTextCategoryName = properties.getOrDefault(Constants.AR_OPROP_FT_MFS_CATEGORY_NAME, new Value("")).toString();
        if (fullTextCategoryName == null) {
            fullTextCategoryName = "";
        }
        int stripTagsForFTStmp = properties.getOrDefault(Constants.AR_OPROP_FT_STRIP_TAGS, new Value(0)).getIntValue();
        if (stripTagsForFTStmp == 1) {
            stripTagsForFTS = true;
        }

        fullTextOptions = 0;
        fullTextOptionsString = "";
        if (this.field.getDataType() == Constants.AR_DATA_TYPE_CHAR) {
            CharacterFieldLimit c = (CharacterFieldLimit) limits;
            if (c != null) {
                fullTextOptions = ((CharacterFieldLimit) limits).getFullTextOption();
            }
        }
        if (this.field.getDataType() == Constants.AR_DATA_TYPE_DIARY) {
            DiaryFieldLimit d = (DiaryFieldLimit) limits;
            if (d != null) {
                fullTextOptions = d.getFullTextOption();
            }
        }
        if (this.field.getDataType() == Constants.AR_DATA_TYPE_ATTACH) {
            AttachmentFieldLimit a = (AttachmentFieldLimit) limits;
            if (a != null) {
                fullTextOptions = a.getFullTextOption();
            }
        }
        if (this.field.getDataType() == Constants.AR_DATA_TYPE_TABLE) {
            fullTextOptions = properties.getOrDefault(Constants.AR_OPROP_FT_MFS_INDEX_TABLE_FIELD, new Value(0)).getIntValue();
            if (fullTextOptions == 0) {
                fullTextOptionsString = "";
            } else {
                fullTextOptionsString = "True";
            }
        }

        literalFTSIndex = false;
        if (this.field.getDataType() != Constants.AR_DATA_TYPE_TABLE) {
            if (fullTextOptions != 0) {
                if ((fullTextOptions & Constants.AR_FULLTEXT_OPTIONS_INDEXED) != 0) {
                    fullTextOptionsString = "FTS and MFS";
                }
                if ((fullTextOptions & Constants.AR_FULLTEXT_OPTIONS_LITERAL) != 0) {
                    literalFTSIndex = true;
                }
                if ((fullTextOptions & Constants.AR_FULLTEXT_OPTIONS_EXCLUDE_FIELD_BASED) != 0) {
                    fullTextOptionsString = "MFS Only";
                }
            }
        }

        rmd.setOverlayGroup("1");
    }

    public boolean isIndexed() {
        return !(fullTextCategoryName.isEmpty() && fullTextOptions == 0 && fullTextOptionsString.isEmpty() && !stripTagsForFTS && !literalFTSIndex);
    }
}
