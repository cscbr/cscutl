/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.utils.fortification;

import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.ARServerUser;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Field;
import com.bmc.arsys.api.Form;
import com.bmc.arsys.api.ObjectPropertyMap;
import com.bmc.arsys.api.Value;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ivan
 */
public class FortificationForm {

    Integer excludeFromMultiFormSearch;
    Integer weightedRelevancyTitleField;
    Integer weightedRelevancyEnvironmentField;
    Integer weightedRelevancyKeywordsField;
    Integer scanTimeInterval;
    Integer scanTimeDays;
    Integer scanTimeWeekdays;
    Integer scanTimeHours;
    Integer scanTimeMinute;

    List<FortificationField> indexedFields;
    Form form;
    ARServerUser rmd;
    String modo;

    public FortificationForm(ARServerUser rmd, String nomeForm, String modo) throws ARException {
        this.modo = modo;
        this.rmd = rmd;
        if (modo.equals("base")) {
            rmd.setBaseOverlayFlag(true);
            rmd.setOverlayFlag(false);
            rmd.setOverlayGroup(Constants.AR_OVERLAY_CLIENT_MODE_BASE);
            rmd.setDesignOverlayGroup(Constants.AR_OVERLAY_CLIENT_MODE_BASE);
        } else {
            rmd.setBaseOverlayFlag(false);
            rmd.setOverlayFlag(true);
            rmd.setOverlayGroup("1");
            rmd.setDesignOverlayGroup("1");
        }
        while (true) {
            try {
                form = rmd.getForm(nomeForm);
                break;
            } catch (ARException ex) {
                // Formularios custom nao existirao em base
                if (modo.equals("base")) {
                    rmd.setOverlayGroup("1");
                    form = rmd.getForm(nomeForm);
                    break;
                }
            }
        }

        indexedFields = new ArrayList<>();

        ObjectPropertyMap properties = form.getProperties();
        if (this.modo.equals("best") && Fortification.getCustomization(properties).startsWith("Overl")) {
            int granularOther = (properties.getOrDefault(Constants.AR_OPROP_OVERLAY_INHERIT_MASK, new Value(Constants.AR_GRANULAR_OTHER)).getIntValue() & Constants.AR_GRANULAR_OTHER);
            if (granularOther > 0) {
                // Overlay = No Overlay, retirar as duplicatas
                rmd.setBaseOverlayFlag(true);
                rmd.setOverlayFlag(false);
                rmd.setOverlayGroup(Constants.AR_OVERLAY_CLIENT_MODE_BASE);
                rmd.setDesignOverlayGroup(Constants.AR_OVERLAY_CLIENT_MODE_BASE);
                Form formBase = rmd.getForm(nomeForm);
                ObjectPropertyMap propertiesBase = formBase.getProperties();
                for (Map.Entry<Integer, Value> entry : propertiesBase.entrySet()) {
                    Integer key = entry.getKey();
                    Value value = entry.getValue();
                    properties.remove(key, value);
                }
                rmd.setBaseOverlayFlag(false);
                rmd.setOverlayFlag(true);
                rmd.setOverlayGroup("1");
                rmd.setDesignOverlayGroup("1");
            } else {
                // Overlay = Overwrite nao precisa retirar as duplicatas
            }
        }

        this.modo = Fortification.getCustomization(properties);

        weightedRelevancyTitleField = 0;
        weightedRelevancyEnvironmentField = 0;
        weightedRelevancyKeywordsField = 0;
        scanTimeInterval = 0;
        scanTimeDays = 0;
        scanTimeWeekdays = 0;
        scanTimeHours = 0;
        scanTimeMinute = 0;

        excludeFromMultiFormSearch = properties.getOrDefault(Constants.AR_OPROP_MFS_OPTION_MASK, new Value(0)).getIntValue();
        scanTimeInterval = properties.getOrDefault(Constants.AR_OPROP_FT_SCAN_TIME_INTERVAL, new Value(0)).getIntValue();
        scanTimeDays = properties.getOrDefault(Constants.AR_OPROP_FT_SCAN_TIME_MONTH_MASK, new Value(0)).getIntValue();
        scanTimeWeekdays = properties.getOrDefault(Constants.AR_OPROP_FT_SCAN_TIME_WEEKDAY_MASK, new Value(0)).getIntValue();
        scanTimeHours = properties.getOrDefault(Constants.AR_OPROP_FT_SCAN_TIME_HOUR_MASK, new Value(0)).getIntValue();
        scanTimeMinute = properties.getOrDefault(Constants.AR_OPROP_FT_SCAN_TIME_MINUTE, new Value(0)).getIntValue();

        String b = properties.getOrDefault(Constants.AR_OPROP_MFS_WEIGHTED_RELEVANCY_FIELDS, new Value("")).toString();
        if (b != null) {
            String[] teste = b.split(";");
            for (int i = 1; i < teste.length; i = i + 2) {
                if (teste[i].equals(Integer.toString(Constants.AR_MFS_WEIGHTED_RELEVANCY_TITLE_FIELD_TAG))) {
                    weightedRelevancyTitleField = Integer.parseInt(teste[i + 1]);
                }
                if (teste[i].equals(Integer.toString(Constants.AR_MFS_WEIGHTED_RELEVANCY_ENVIRONMENT_FIELD_TAG))) {
                    weightedRelevancyEnvironmentField = Integer.parseInt(teste[i + 1]);
                }
                if (teste[i].equals(Integer.toString(Constants.AR_MFS_WEIGHTED_RELEVANCY_KEYWORDS_FIELD_TAG))) {
                    weightedRelevancyKeywordsField = Integer.parseInt(teste[i + 1]);
                }
            }
        }

        if (!modo.equals("best")) {
            rmd.setOverlayGroup("1");
            return;
        }

        List<Field> fields = rmd.getListFieldObjects(nomeForm);

        for (Field field : fields) {
            FortificationField f1 = new FortificationField(rmd, field, "best");
            if (f1.isIndexed()) {
                indexedFields.add(f1);
            }
            
            if (f1.customization.startsWith("Overl")) {
                FortificationField f2 = new FortificationField(rmd, field, "base");
                if (f2.isIndexed()) {
                    indexedFields.add(f2);
                }
            }
             
        }

        rmd.setOverlayGroup("1");
    }

    public String getFormType() {
        int tipo = form.getFormType();
        if (tipo == Constants.AR_SCHEMA_NONE) {
            return "None";
        }
        if (tipo == Constants.AR_SCHEMA_REGULAR) {
            return "Regular";
        }
        if (tipo == Constants.AR_SCHEMA_JOIN) {
            return "Join";
        }
        if (tipo == Constants.AR_SCHEMA_VIEW) {
            return "View";
        }
        if (tipo == Constants.AR_SCHEMA_DIALOG) {
            return "Display";
        }
        if (tipo == Constants.AR_SCHEMA_VENDOR) {
            return "Vendor";
        }
        if (tipo == Constants.AR_SCHEMA_PLACEHOLDER) {
            return "Placeholder";
        }
        return "Placeholder";
    }

    public boolean hasMFS() {
        return excludeFromMultiFormSearch != 0 || weightedRelevancyTitleField != 0 || weightedRelevancyEnvironmentField != 0 || weightedRelevancyKeywordsField != 0 || scanTimeInterval != 0 || scanTimeDays != 0 || scanTimeWeekdays != 0 || scanTimeHours != 0 || scanTimeMinute != 0;
    }

    public String getName() {
        return form.getName();
    }

    public String getWeightedRelevancyTitleFieldName() {
        String retorno = "";
        if (weightedRelevancyTitleField > 0) {
            try {
                retorno = rmd.getField(getName(), weightedRelevancyTitleField).getName();
            } catch (ARException ex) {
            }
        }
        return retorno;
    }

    public String getWeightedRelevancyEnvironmentFieldName() {
        String retorno = "";
        if (weightedRelevancyEnvironmentField > 0) {
            try {
                retorno = rmd.getField(getName(), weightedRelevancyEnvironmentField).getName();
            } catch (ARException ex) {
            }
        }
        return retorno;
    }

    public String getWeightedRelevancyKeywordsFieldName() {
        String retorno = "";
        if (weightedRelevancyKeywordsField > 0) {
            try {
                retorno = rmd.getField(getName(), weightedRelevancyKeywordsField).getName();
            } catch (ARException ex) {
            }
        }
        return retorno;
    }

}
