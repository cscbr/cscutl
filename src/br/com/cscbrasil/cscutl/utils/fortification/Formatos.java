/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.utils.fortification;

import com.marzapower.loggable.Log;
import jxl.format.Alignment;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WriteException;

/**
 *
 * @author ivan
 */
public class Formatos {

    public static WritableCellFormat fmtNumber = fmt(false, true, false, false);
    public static WritableCellFormat fmtNumberTotal = fmt(false, true, false, true);
    public static WritableCellFormat fmtLabelTotal = fmt(false, false, false, true);
    public static WritableCellFormat fmtPadrao = fmt(false, false, false, false);
    public static WritableCellFormat fmtTitulo = fmt(false, false, true, true);
    public static WritableCellFormat fmtInteiro = fmt(true, true, true, false);

    private Formatos() {
    }

    private static WritableCellFormat fmt(boolean inteiro, boolean numerico, boolean centralizado, boolean negrito) {
        WritableFont cellFont = new WritableFont(WritableFont.createFont("Calibri"), 10);
        if (negrito) {
            try {
                cellFont.setBoldStyle(WritableFont.BOLD);
            } catch (WriteException ex) {
                Log.get().error(ex);
            }
        }
        WritableCellFormat f;
        if (numerico) {
            if (inteiro) {
                f = new WritableCellFormat(cellFont, NumberFormats.INTEGER);
            } else {
                f = new WritableCellFormat(cellFont, NumberFormats.THOUSANDS_FLOAT);
            }
        } else {
            f = new WritableCellFormat(cellFont);
        }

        if (centralizado) {
            try {
                f.setAlignment(Alignment.CENTRE);
            } catch (WriteException ex) {
                Log.get().error(ex);
            }
        }
        return f;
    }

}
