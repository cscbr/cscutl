/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.utils.fortification;

import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.ARServerUser;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.ObjectPropertyMap;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author ivan
 */
public class Fortification {

    private final ARServerUser ar;

    public Fortification() throws ARException, IOException {
        RemedyConnection rmd = RemedyConnections.get();
        ar = rmd.sourc;
        ar.login();
        ar.setOverlayGroup("1");
        ar.setDesignOverlayGroup("1");
        ar.setLocale("pt_BR");
        Log.get().info("Fortification");
    }

    public class SortIgnoreCase implements Comparator<Object> {

        @Override
        public int compare(Object o1, Object o2) {
            String s1 = (String) o1;
            String s2 = (String) o2;
            return s1.toLowerCase().compareTo(s2.toLowerCase());
        }
    }

    public void execute(String operacao) throws ARException, IOException, WriteException {
        if (operacao == null) {
            Log.get().error("Operacao invalida - " + operacao + " deve ser read ou write");
        }

        if (operacao.equals("read")) {
            execute_read();
            return;
        }
        if (operacao.equals("write")) {
            execute_write();
            return;
        }
        Log.get().error("Operacao invalida - " + operacao + " deve ser read ou write");
    }

    public void execute_write() throws ARException, IOException, WriteException {

    }

    public void execute_read() throws ARException, IOException, WriteException {
        File outputDir = new File("outputs");
        FileUtils.forceMkdir(outputDir);
        File fFullTextInfoBefore = new File("outputs/01-FullTextInfoBefore.xls");
        WritableWorkbook workbook = Workbook.createWorkbook(fFullTextInfoBefore);
        WritableSheet sheetForms = workbook.createSheet("Forms", 0);
        WritableSheet sheetFields = workbook.createSheet("Fields", 1);
        sheetForms.addCell(new Label(0, 0, "Status", Formatos.fmtTitulo));
        sheetForms.addCell(new Label(1, 0, "Form", Formatos.fmtTitulo));
        sheetForms.addCell(new Label(2, 0, "Type", Formatos.fmtTitulo));
        sheetForms.addCell(new Label(3, 0, "Customization", Formatos.fmtTitulo));
        sheetForms.addCell(new Label(4, 0, "Exclude from MFS", Formatos.fmtTitulo));
        sheetForms.addCell(new Label(5, 0, "Title (Field ID)", Formatos.fmtTitulo));
        sheetForms.addCell(new Label(6, 0, "Title (Database Name)", Formatos.fmtTitulo));
        sheetForms.addCell(new Label(7, 0, "Environment (Field ID)", Formatos.fmtTitulo));
        sheetForms.addCell(new Label(8, 0, "Environment (Database Name)", Formatos.fmtTitulo));
        sheetForms.addCell(new Label(9, 0, "Keywords (Field ID)", Formatos.fmtTitulo));
        sheetForms.addCell(new Label(10, 0, "Keywords (Database Name)", Formatos.fmtTitulo));
        sheetForms.addCell(new Label(11, 0, "Scan Time Interval", Formatos.fmtTitulo));
        sheetForms.addCell(new Label(12, 0, "Scan Time Mask Days", Formatos.fmtTitulo));
        sheetForms.addCell(new Label(13, 0, "Scan Time Mask Weekdays ", Formatos.fmtTitulo));
        sheetForms.addCell(new Label(14, 0, "Scan Time Mask Hours", Formatos.fmtTitulo));
        sheetForms.addCell(new Label(15, 0, "Scan Time Mask Minute", Formatos.fmtTitulo));
        sheetFields.addCell(new Label(0, 0, "Status", Formatos.fmtTitulo));
        sheetFields.addCell(new Label(1, 0, "Form", Formatos.fmtTitulo));
        sheetFields.addCell(new Label(2, 0, "Type", Formatos.fmtTitulo));
        sheetFields.addCell(new Label(3, 0, "Field Id", Formatos.fmtTitulo));
        sheetFields.addCell(new Label(4, 0, "Field Name", Formatos.fmtTitulo));
        sheetFields.addCell(new Label(5, 0, "Customization", Formatos.fmtTitulo));
        sheetFields.addCell(new Label(6, 0, "Fulltext Category Name", Formatos.fmtTitulo));
        sheetFields.addCell(new Label(7, 0, "Index for FTS", Formatos.fmtTitulo));
        sheetFields.addCell(new Label(8, 0, "Literal FTS Index", Formatos.fmtTitulo));
        sheetFields.addCell(new Label(9, 0, "Strip Tags for FTS", Formatos.fmtTitulo));
        int linha0 = 0;
        int linha1 = 0;

        List<String> formNames = ar.getListForm(0, Constants.AR_LIST_SCHEMA_ALL | Constants.AR_HIDDEN_INCREMENT);

        java.util.Collections.sort(formNames, new SortIgnoreCase());

        int atual = 0;

        for (String formName : formNames) {
            atual++;
            Log.get().info(String.format("(%05d/%05d) %s", atual, formNames.size(), formName));
            List<FortificationForm> fs = new ArrayList<>();
            FortificationForm f1 = new FortificationForm(ar, formName, "best");
            fs.add(f1);
            String customization = Fortification.getCustomization(f1.form.getProperties());
            if (customization.startsWith("Overl")) {
                FortificationForm f2 = new FortificationForm(ar, formName, "base");
                fs.add(f2);
            }
            for (FortificationForm f : fs) {
                if (f.hasMFS()) {
                    linha0++;
                    sheetForms.addCell(new Label(0, linha0, "Enabled", Formatos.fmtPadrao));
                    sheetForms.addCell(new Label(1, linha0, f.getName(), Formatos.fmtPadrao));
                    sheetForms.addCell(new Label(2, linha0, f.getFormType(), Formatos.fmtPadrao));
                    sheetForms.addCell(new Label(3, linha0, f.modo, Formatos.fmtPadrao));
                    if (f.excludeFromMultiFormSearch == 0) {
                        sheetForms.addCell(new Label(4, linha0, "No", Formatos.fmtPadrao));
                    } else {
                        sheetForms.addCell(new Label(4, linha0, "Yes", Formatos.fmtPadrao));
                    }
                    if (f.weightedRelevancyTitleField > 0) {
                        sheetForms.addCell(new jxl.write.Number(5, linha0, f.weightedRelevancyTitleField, Formatos.fmtInteiro));
                        sheetForms.addCell(new Label(6, linha0, f.getWeightedRelevancyTitleFieldName(), Formatos.fmtPadrao));
                    }
                    if (f.weightedRelevancyEnvironmentField > 0) {
                        sheetForms.addCell(new jxl.write.Number(7, linha0, f.weightedRelevancyEnvironmentField, Formatos.fmtInteiro));
                        sheetForms.addCell(new Label(8, linha0, f.getWeightedRelevancyEnvironmentFieldName(), Formatos.fmtPadrao));
                    }
                    if (f.weightedRelevancyKeywordsField > 0) {
                        sheetForms.addCell(new jxl.write.Number(9, linha0, f.weightedRelevancyKeywordsField, Formatos.fmtInteiro));
                        sheetForms.addCell(new Label(10, linha0, f.getWeightedRelevancyKeywordsFieldName(), Formatos.fmtPadrao));
                    }
                    if (f.scanTimeInterval != 0) {
                        sheetForms.addCell(new jxl.write.Number(11, linha0, f.scanTimeInterval, Formatos.fmtInteiro));
                    }
                    if (f.scanTimeDays != 0) {
                        sheetForms.addCell(new jxl.write.Number(12, linha0, f.scanTimeDays, Formatos.fmtInteiro));
                    }
                    if (f.scanTimeWeekdays != 0) {
                        sheetForms.addCell(new jxl.write.Number(13, linha0, f.scanTimeWeekdays, Formatos.fmtInteiro));
                    }
                    if (f.scanTimeHours != 0) {
                        sheetForms.addCell(new jxl.write.Number(14, linha0, f.scanTimeHours, Formatos.fmtInteiro));
                    }
                    if (f.scanTimeMinute != 0) {
                        sheetForms.addCell(new jxl.write.Number(15, linha0, f.scanTimeMinute, Formatos.fmtInteiro));
                    }
                }
                if (f.indexedFields.size() > 0) {
                    for (FortificationField indexedField : f.indexedFields) {
                        linha1++;
                        sheetFields.addCell(new Label(0, linha1, "Enabled", Formatos.fmtPadrao));
                        sheetFields.addCell(new Label(1, linha1, f.getName(), Formatos.fmtPadrao));
                        sheetFields.addCell(new Label(2, linha1, f.getFormType(), Formatos.fmtPadrao));
                        sheetFields.addCell(new jxl.write.Number(3, linha1, indexedField.field.getFieldID(), Formatos.fmtInteiro));
                        sheetFields.addCell(new Label(4, linha1, indexedField.field.getName(), Formatos.fmtPadrao));
                        sheetFields.addCell(new Label(5, linha1, indexedField.customization, Formatos.fmtPadrao));
                        sheetFields.addCell(new Label(6, linha1, indexedField.fullTextCategoryName, Formatos.fmtPadrao));
                        sheetFields.addCell(new Label(7, linha1, indexedField.fullTextOptionsString, Formatos.fmtPadrao));
                        if (indexedField.literalFTSIndex) {
                            sheetFields.addCell(new Label(8, linha1, "Yes", Formatos.fmtPadrao));
                        } else {
                            sheetFields.addCell(new Label(8, linha1, "No", Formatos.fmtPadrao));
                        }
                        if (indexedField.stripTagsForFTS) {
                            sheetFields.addCell(new Label(9, linha1, "Yes", Formatos.fmtPadrao));
                        } else {
                            sheetFields.addCell(new Label(9, linha1, "No", Formatos.fmtPadrao));
                        }
                    }
                }
            }
        }
        workbook.write();
        workbook.close();
    }

    public static String getCustomization(ObjectPropertyMap properties) {
        int overlayProp = properties.getOrDefault(Constants.AR_SMOPROP_OVERLAY_PROPERTY, new Value(0)).getIntValue();
        if (overlayProp == Constants.AR_ORIGINAL_OBJECT || overlayProp == Constants.AR_OVERLAID_OBJECT) {
            return "Base";
        }
        if (overlayProp == Constants.AR_OVERLAY_OBJECT) {
            return "Overlay";
        }
        if (overlayProp == Constants.AR_CUSTOM_OBJECT) {
            return "Custom";
        }
        return "Unknown";
    }
}
