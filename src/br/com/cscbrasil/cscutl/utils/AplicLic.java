/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.utils;

import br.com.cscbrasil.bmc.RemedyConnections;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.ARServerUser;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Entry;
import com.bmc.arsys.api.Menu;
import com.bmc.arsys.api.MenuCriteria;
import com.bmc.arsys.api.MenuItem;
import com.bmc.arsys.api.OutputInteger;
import com.bmc.arsys.api.QualifierInfo;
import com.bmc.arsys.api.SortInfo;
import com.bmc.arsys.api.Value;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ivan
 */
public class AplicLic {

    private final int numlic;
    private final ARServerUser rmd;
    String chave;
    String expire;

    public AplicLic(String chave, String expire) throws ARException {
        numlic = 100000;
        this.chave = chave;
        this.expire = expire;
        rmd = RemedyConnections.get().desti;
        rmd.login();
    }

    public void execute() throws ARException {
        // Obtem licencas ja aplicadas
        int[] fieldIds = {1};
        QualifierInfo qualification;
        qualification = rmd.parseQualification("AR System Licenses", "1=1");
        ArrayList<SortInfo> sortList = new ArrayList<>();
        sortList.add(new SortInfo(Constants.AR_CORE_CREATE_DATE, Constants.AR_SORT_ASCENDING));
        sortList.add(new SortInfo(Constants.AR_CORE_ENTRY_ID, Constants.AR_SORT_ASCENDING));
        OutputInteger nMatches = new OutputInteger();
        List<Entry> result = rmd.getListEntryObjects("AR System Licenses", qualification, Constants.AR_START_WITH_FIRST_ENTRY, 999999999, sortList, fieldIds, false, nMatches);

        // Apaga licencas ja aplicadas
        for (Entry e : result) {
            rmd.deleteEntry("AR System Licenses", e.getEntryId(), Constants.AR_JOIN_DELOPTION_FORCE);
        }

        // Obtem lista de licencas "aplicaveis"        
        MenuCriteria criteria = new MenuCriteria();
        criteria.setRetrieveAll(true);
        Menu m = rmd.getMenu("AR System Administration: AddRemLic - Product Features", criteria);

        // Aplica licencas
        aplica_lic_abre_menu(m.getContent());

    }

    private void aplica_lic_abre_menu(List<MenuItem> items) throws ARException {
        for (MenuItem item : items) {
            if (item.getType() == Constants.AR_MENU_TYPE_MENU) {
                aplica_lic_abre_menu(item.getSubMenu());
            }
            if (item.getType() == Constants.AR_MENU_TYPE_VALUE) {
                aplica_lic_cria(item.getValue());
            }
        }
    }

    private void aplica_lic_cria(String value) throws ARException {
        Entry registro = new Entry();

        registro.put(2700, new Value(value));
        if (value.equals("AR Server")) {
            registro.put(2703, new Value(chave));
            registro.put(2702, new Value(1));
            registro.put(2704, new Value(expire));
            registro.put(2, new Value("n/a"));
            registro.put(8, new Value("n/a"));
            registro.put(7, new Value(0));
        } else if (value.contains("Fixed") || value.contains("Floating")) {
            if (value.equals("AR User Fixed")) {
                registro.put(2702, new Value(numlic - 3));
            } else {
                registro.put(2702, new Value(numlic));
            }
        } else {
            registro.put(2702, new Value(1));
        }

        rmd.createEntry("AR System Licenses", registro);

    }
}
