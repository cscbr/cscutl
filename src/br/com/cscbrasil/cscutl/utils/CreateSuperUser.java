/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.utils;

import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import br.com.cscbrasil.cscutl.common.Util;
import com.bmc.arsys.api.ARErrors;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Entry;
import com.bmc.arsys.api.StatusInfo;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.util.List;

/**
 *
 * @author ivan
 */
public class CreateSuperUser {

    RemedyConnection rmd;

    public CreateSuperUser() {
        rmd = RemedyConnections.get();
    }

    public void run(String remedyLoginID) throws ARException {

        List<List<Value>> people = Util.simpleSQL(rmd.sourc, "select person_id from ctm_people where remedy_login_id='%s'", remedyLoginID);
        if (people.size() == 0) {
            Log.get().error("Impossivel encontrar " + remedyLoginID + " em CTM:People");
            return;
        }

        String personId = people.get(0).get(0).toString();

        List<List<Value>> permissions = Util.simpleSQL(rmd.desti, "select product_name,permission_group,permission_group_id,VIATIL_LICENSE_REQUIRED,PERMISSION_TAG_NAME from LIC_SYS_License_Permission_Map where status=1 and (permission_group like '%Analyst' or permission_group like '%Manager' or permission_group like '%Administrator' or permission_group like '%Admin' or permission_group like '%Master' or permission_group like '%Config' or permission_group like '%User')");
        for (List<Value> permission : permissions) {
            String productName = permission.get(0).toString();//"Configuration/Asset Management";
            String permissionGroup = permission.get(1).toString();//"Asset Admin";            
            if (!permissionGroup.equals("Incident Master")) {
                continue;
            }
            Integer permissionGroupID = permission.get(2).getIntValue();
            Integer VIATIL_LICENSE_REQUIRED = permission.get(3).getIntValue();

            String viaTILLicenseType = "Fixed";
            if (VIATIL_LICENSE_REQUIRED == 0) {
                viaTILLicenseType = "Not Applicable";
            }
            if (VIATIL_LICENSE_REQUIRED == 1) {
                viaTILLicenseType = "Read";
            }

            String PermissionTagName = permission.get(4).toString();
            Integer PermissionGroupType = 1; // Fixo Application Permission ?

            Entry entry = new Entry();
            entry.put(2, new Value(rmd.sourc.getUser())); //Submitter        
            entry.put(4, new Value(remedyLoginID)); //Remedy Login ID
            entry.put(7, new Value(1)); //Status
            entry.put(8, new Value(".")); //Short Description
            //entry.put(112, new Value(";1000000001;")); //Assignee Groups
            //entry.put(60989, new Value("")); //Assignee Groups_parent
            entry.put(240001002, new Value(productName)); //Product Name
            entry.put(1000000080, new Value(personId)); //Person ID
            entry.put(1000001578, new Value(permissionGroup)); //Permission Group
            entry.put(1000001579, new Value(permissionGroupID)); //Permission Group ID
            entry.put(1000002294, new Value(viaTILLicenseType)); //viaTIL License Type
            entry.put(1000002340, new Value(PermissionTagName)); //Permission Tag Name
            entry.put(1000003972, new Value(PermissionGroupType)); //Permission Group Type
            Log.get().info(permissionGroup + " - " + viaTILLicenseType);
            try {
                String createEntry = rmd.desti.createEntry("CTM:People Permission Groups", entry);
            } catch (ARException ex) {
                List<StatusInfo> lastStatus = ex.getLastStatus();
                if (lastStatus.size() == 1) {
                    if (lastStatus.get(0).getMessageNum() == ARErrors.AR_ERROR_DUP_ON_UNIQUE_INDEX) {
                        continue;
                    }
                }
                Log.get().error(ex);
            }
            int a = 0;
        }
    }
}
