/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.utils;

import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Field;
import com.marzapower.loggable.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author ivan
 */
public class FieldList {

    RemedyConnection rmd;
    String formName;
    List<Integer> fieldListAnexos;
    int[] fieldIdsAll;

    public FieldList(String formName) {
        rmd = RemedyConnections.get();
        this.formName = formName;
        List<Field> fieldsFonte;
        try {
            fieldsFonte = rmd.sourc.getListFieldObjects(formName);
        } catch (ARException ex) {
            Log.get().error(ex.getMessage(), ex);
            return;
        }

        for (Iterator<Field> i = fieldsFonte.iterator(); i.hasNext();) {
            Field f = i.next();
            if (f.getFieldOption() == Constants.AR_FIELD_OPTION_DISPLAY) {
                i.remove();
                continue;
            }
            if (f.getFieldID() == Constants.AR_CORE_STATUS_HISTORY) {
                i.remove();
            }
        }
        fieldListAnexos = new ArrayList<>();

        for (Iterator<Field> i = fieldsFonte.iterator(); i.hasNext();) {
            Field f = i.next();
            if (f.getFieldOption() == Constants.AR_FIELD_OPTION_DISPLAY) {
                i.remove();
                continue;
            }
            if (f.getFieldID() == Constants.AR_CORE_STATUS_HISTORY) {
                i.remove();
                continue;
            }
            if (f.getDataType() == Constants.AR_DATA_TYPE_ATTACH) {
                fieldListAnexos.add(f.getFieldID());
            }
        }
        fieldIdsAll = fieldsFonte.stream().mapToInt((Field p) -> p.getFieldID()).toArray();
    }

    List<Integer> getFieldListAnexos() {
        return fieldListAnexos;
    }

    public int[] getFieldIdsAll() {
        return fieldIdsAll;
    }

}
