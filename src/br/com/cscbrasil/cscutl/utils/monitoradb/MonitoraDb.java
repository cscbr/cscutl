/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.utils.monitoradb;

import br.com.cscbrasil.bmc.ARServerUser;
import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Constants;
import com.marzapower.loggable.Log;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 *
 * @author ivan
 */
public class MonitoraDb extends TimerTask {

    private final Map<String, Future<Long>> FUTURES = new HashMap<>();
    private final Map<String, Callable> CALLABLES = new HashMap<>();
    private final ForkJoinPool POOL;

    RemedyConnection rmd;
    ARServerUser ar;
    final int tipo;

    public MonitoraDb(int tipo) {
        this.tipo = tipo;
        this.POOL = new ForkJoinPool(128, ForkJoinPool.defaultForkJoinWorkerThreadFactory, null, true);
        rmd = RemedyConnections.get();
        if (tipo == 1) {
            ar = rmd.sourc;
        } else {
            ar = rmd.desti;
        }
    }

    @Override
    public void run() {
        List<String> formNames;
        try {
            formNames = ar.getListForm(0, Constants.AR_LIST_SCHEMA_REGULAR | Constants.AR_HIDDEN_INCREMENT);
        } catch (ARException ex) {
            Log.get().error(ex);
            return;
        }
        for (String formName : formNames) {
            Future<Long> future = FUTURES.get(formName);
            if (future == null) {
                Callable f;
                f = new MonitoraDdDetail(formName, this.tipo);
                CALLABLES.put(formName, f);
                FUTURES.put(formName, POOL.submit(f));
            } else if (future.isCancelled() || future.isDone()) {
                Long resp;
                try {
                    resp = future.get(0, TimeUnit.MILLISECONDS);
                } catch (InterruptedException | ExecutionException ex) {
                    Log.get().error(ex);
                    continue;
                } catch (TimeoutException ex) {
                    continue;
                }
                // durma ou reinicie o processo
                if (resp == null) {
                    Callable f = CALLABLES.get(formName);
                    FUTURES.put(formName, POOL.submit(f));
                } else if (resp >= 0) {
                    long proximo = resp;
                    long agora = System.currentTimeMillis();
                    if (agora > proximo) {
                        Callable f = CALLABLES.get(formName);
                        FUTURES.put(formName, POOL.submit(f));
                    }
                }
            }
        }
    }
}
