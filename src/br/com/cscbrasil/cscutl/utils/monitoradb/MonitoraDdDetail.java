/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.utils.monitoradb;

import br.com.cscbrasil.bmc.ARServerUser;
import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.SQLResult;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.util.List;
import java.util.concurrent.Callable;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author ivan
 */
public class MonitoraDdDetail implements Callable {

    private final RemedyConnection rmd;
    private ARServerUser ar;
    private final int formNumber;
    private long lastTs = -1;
    private String formName;

    MonitoraDdDetail(String formName, int tipo) {
        this.formName = formName;
        this.rmd = RemedyConnections.get();
        if (tipo == 1) {
            this.ar = rmd.sourc;
        } else {
            this.ar = rmd.desti;
        }
        this.formNumber = ar.cscGetFormNumber(formName);
    }

    @Override
    public Long call() throws ARException {
        String cmd = String.format("select max(c6) from t%d", this.formNumber);
        SQLResult listSQL;
        listSQL = ar.getListSQL(cmd, 999999999, false);

        List<List<Value>> contents = listSQL.getContents();
        if (contents.isEmpty()) {
            return 0L;
        }
        if (contents.get(0) == null) {
            return 0L;
        }
        if (contents.get(0).get(0) == null) {
            return 0L;
        }

        long ts;
        try {
            ts = contents.get(0).get(0).getIntValue();
        } catch (NullPointerException ex) {
            return 0L;
        }
        if (ts != lastTs) {
            if (lastTs >= 0) {
                Log.get().info(String.format("%-20s %-80s %s->%s", ar.getServer(), formName, epoch2String(lastTs), epoch2String(ts)));
            }
            lastTs = ts;
        }
        return 0L;
    }

    static String epoch2String(long value) {
        DateTime jodaDateTime = new DateTime(value * 1000);
        DateTimeFormatter dtfOut = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
        return dtfOut.print(jodaDateTime);
    }

}
