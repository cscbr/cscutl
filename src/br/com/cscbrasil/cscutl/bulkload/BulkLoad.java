/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.bulkload;

import br.com.cscbrasil.bmc.ARServerUser;
import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Entry;
import com.bmc.arsys.api.EntryListInfo;
import com.bmc.arsys.api.Field;
import com.bmc.arsys.api.Form;
import com.bmc.arsys.api.QualifierInfo;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.CellFormat;
import jxl.read.biff.BiffException;

/**
 *
 * @author ivan
 */
public class BulkLoad {

    String fileName;
    Workbook workbookRead;
    Sheet fonte;
    File xls;
    ARServerUser ar;
    WorkbookSettings ws;

    public BulkLoad(String fileName) {
        this.fileName = fileName;
        xls = new File(fileName);
    }

    public void execute() throws IOException, BiffException, ARException {
        RemedyConnection rmd = RemedyConnections.get();
        ar = rmd.desti;
        ar.cscSetModeBest();
        WorkbookSettings ws = new WorkbookSettings();
        ws.setEncoding("Cp1252");
        workbookRead = Workbook.getWorkbook(xls, ws);

        Sheet[] sheets = workbookRead.getSheets();
        for (Sheet fonte : sheets) {
            readSheet(fonte);
        }

        workbookRead.close();
    }

    private void readSheet(Sheet sheet) throws ARException {
        String formName = sheet.getCell(0, 0).getContents();
        Form form = ar.getForm(formName);
        List<Field> listFieldObjects = ar.getListFieldObjects(formName);
        Map<String, Integer> fields = new HashMap<>();
        for (int c = 0; c < sheet.getColumns(); c++) {
            String fieldName = sheet.getCell(c, 1).getContents();
            int fieldId = 0;
            for (Field f : listFieldObjects) {
                if (f.getName().equals(fieldName)) {
                    fieldId = f.getFieldID();
                    break;
                }
            }
            if (fieldId != 0) {
                fields.put(fieldName, fieldId);
            }
        }

        for (int l = 2; l < sheet.getRows(); l++) {
            Entry registro = new Entry();
            List<Integer> entryListFields = new ArrayList<>();
            String ql = "";
            for (int c = 0; c < sheet.getColumns(); c++) {
                CellFormat cellFormat = sheet.getCell(c, 1).getCellFormat();
                String attribute = sheet.getCell(c, 1).getContents();
                String value = sheet.getCell(c, l).getContents();
                int fieldId = fields.get(attribute);
                if (cellFormat.getBackgroundColour().getDescription().equals("red")) {
                    ql += String.format("'%s'=\"%s\" AND ", attribute, value);
                }
                if (value.startsWith("Andr")) {
                    int a = 0;
                }
                registro.put(fieldId, new Value(value));
                entryListFields.add(fieldId);
                if (value != null) {
                    if (value.length() == 0) {
                        registro.put(fieldId, new Value());
                    }
                }
            }
            ql = ql.substring(0, ql.length() - 5).trim();
            QualifierInfo q;
            q = ar.parseQualification(formName, ql);
            Log.get().info(registro.toString());

            List<EntryListInfo> listEntry = ar.getListEntry(formName, q, Constants.AR_START_WITH_FIRST_ENTRY, 999999999, null, null, false, null);

            if (listEntry.size() == 0) {
                ar.createEntry(formName, registro);
            } else {
                //Entry entry = ar.getEntry(formName, listEntry.get(0).getEntryID(), null);
                //Log.get().info(entry.toString());
                ar.setEntry(formName, listEntry.get(0).getEntryID(), registro, null, Constants.AR_JOIN_SETOPTION_NONE);
            }
            //ar.mergeEntry(formName, registro, Constants.AR_MERGE_ENTRY_DUP_MERGE, q, Constants.AR_MERGE_MULTIOPTIONS_CHANGE_FIRST);
            //int a = 0;
        }
    }
}
