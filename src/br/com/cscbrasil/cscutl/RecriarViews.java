/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl;

import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import com.bmc.arsys.api.ARErrors;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Field;
import com.bmc.arsys.api.Form;
import com.bmc.arsys.api.JoinForm;
import com.bmc.arsys.api.SQLResult;
import com.bmc.arsys.api.StatusInfo;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ivan
 */
class RecriarViews {

    List<String> processados;

    RemedyConnection rmd;

    void execute() throws ARException {
        processados = new ArrayList<>();
        rmd = RemedyConnections.get();
        rmd.desti.cscSetModeBest();

        //int[] requestList = {Constants.AR_SERVER_INFO_CACHE_MODE, Constants.AR_SERVER_INFO_DELAYED_CACHE, Constants.AR_SERVER_INFO_DELAY_RECACHE_TIME};
        int[] requestList = {Constants.AR_SERVER_INFO_CACHE_MODE, Constants.AR_SERVER_INFO_DELAY_RECACHE_TIME};

        //ServerInfoMap serverInfoOriginal = rmd.desti.getServerInfo(requestList);
        //ServerInfoMap serverInfoModifica = rmd.desti.getServerInfo(requestList);
        //serverInfoModifica.put(Constants.AR_SERVER_INFO_CACHE_MODE, new Value(1));
        //serverInfoModifica.put(Constants.AR_SERVER_INFO_DELAY_RECACHE_TIME, new Value(1800));
        //serverInfoModifica.put(Constants.AR_SERVER_INFO_DELAYED_CACHE, new Value(1));        
        //rmd.desti.setServerInfo(serverInfoModifica);
        List<String> listForm = rmd.desti.getListForm();
        java.util.Collections.sort(listForm);

        for (String formName : listForm) {
            rmd.desti.cscSetModeBest();
            Form form = rmd.desti.getForm(formName);
            if (form.getFormType() == Constants.AR_SCHEMA_VENDOR) {
                continue;
            }
            if (form.getFormType() == Constants.AR_SCHEMA_DIALOG) {
                continue;
            }
            if (form.getFormType() == Constants.AR_SCHEMA_VIEW) {
                continue;
            }
            if (form.getFormType() == Constants.AR_SCHEMA_PLACEHOLDER) {
                continue;
            }
            if (form.getFormType() == Constants.AR_SCHEMA_NONE) {
                continue;
            }
            processaForm("", formName);
        }
        //rmd.desti.setServerInfo(serverInfoOriginal);
    }

    private void processaForm(String espacos, String formName) {
        if (processados.contains(formName)) {
            return;
        }
        System.out.print(espacos + formName);
        if (viewValida(formName)) {
            System.out.println(" ok");
            return;
        }
        System.out.println(" recriando...");
        rmd.desti.cscSetModeBest();
        Form form;
        Field f;
        String cscGetCustomization;
        try {
            form = rmd.desti.getForm(formName);
            if (form.getFormType() == Constants.AR_SCHEMA_JOIN) {
                processaForm(espacos + "   ", ((JoinForm) form).getMemberA());
                processaForm(espacos + "   ", ((JoinForm) form).getMemberB());
            }
            f = rmd.desti.getField(formName, 1);
            f.setFieldID(1234567890);
            f.setName("___cscbrasil___");
            f.setFieldOption(Constants.AR_FIELD_OPTION_OPTIONAL);
            f.setObjectProperty(null);

            cscGetCustomization = rmd.desti.cscGetCustomization(form);
        } catch (ARException ex) {
            Log.get().fatal(String.format("%-80s - %s", formName, ex.getMessage()));
            return;
        }

        if (cscGetCustomization.equals("Custom")) {
            rmd.desti.cscSetModeBest();
        } else {
            rmd.desti.cscSetModeBase();
        }

        Field fExiste = null;
        try {
            fExiste = rmd.desti.getField(formName, 1234567890);
        } catch (ARException ex) {
        }

        try {
            if (fExiste == null) {
                rmd.desti.createField(f, true);
            }
            rmd.desti.deleteField(formName, 1234567890, 0);
            processados.add(formName);
        } catch (ARException ex) {
            List<StatusInfo> lastStatus = ex.getLastStatus();
            boolean ignora = false;
            if (lastStatus.size() == 1 && lastStatus.get(0).getMessageNum() == ARErrors.AR_ERROR_CANNOT_CREATE_SHADOW_FIELD) {
                ignora = true;
            }
            if (!ignora) {
                Log.get().error(String.format("%-80s - %s", formName, ex.getMessage()));
            }
        }
    }

    private List<String> getDBTables(String formName) {
        List<String> getDBTables = new ArrayList<>();
        int formId = rmd.desti.cscGetFormNumber(formName);
        if (formId == 0) {
            return getDBTables;
        }
        getDBTables.add(String.format("B%d", formId));
        getDBTables.add(String.format("H%d", formId));
        SQLResult result = null;
        try {
            String cmd = String.format("Select fieldid from field inner join arschema on arschema.schemaid=field.schemaid where (arschema.name='%s' OR arschema.name='%s__o' OR arschema.name='%s__c') AND datatype=%d AND foption<%d", formName, formName, formName, Constants.AR_DATA_TYPE_ATTACH, Constants.AR_FIELD_OPTION_DISPLAY);
            result = rmd.desti.getListSQL(cmd, 999999999, false);
        } catch (ARException ex) {
            Log.get().fatal(ex.getMessage(), ex);
            return getDBTables;
        }
        List<List<Value>> records = result.getContents();
        for (List<Value> record : records) {
            Value v = record.get(0);
            getDBTables.add(String.format("B%dC%d", formId, v.getIntValue()));
        }
        getDBTables.add(String.format("T%d", formId));
        return getDBTables;
    }

    private boolean viewValida(String formName) {
        List<String> views = viewNames(formName);

        Form form;
        try {
            form = rmd.desti.getForm(formName);
        } catch (ARException ex) {
            return false;
        }
        if (form.getFormType() == Constants.AR_SCHEMA_JOIN) {
            List<String> dbTables = getDBTables(formName);
            views.addAll(dbTables);
        }

        for (String view : views) {
            if (view == null) {
                continue;
            }
            String sql = String.format("select object_name, status from user_objects where object_type = 'VIEW' and object_name = '%s'", view.toUpperCase());
            SQLResult result;
            try {
                result = rmd.desti.getListSQL(sql, 999999999, false);
            } catch (ARException ex) {
                return false;
            }
            List<List<Value>> contents = result.getContents();
            if (contents.size() == 0) {
                return false;
            }
            List<Value> content = contents.get(0);
            if (!content.get(1).toString().equals("VALID")) {
                return false;
            }
        }
        processados.add(formName);
        return true;
    }

    private List<String> viewNames(String formName) {
        List<String> retorno = new ArrayList<>();
        String sql = String.format("select distinct(viewName) as vName from arschema where name='%s' union select distinct(shviewName) as vName from arschema where name='%s'", formName, formName);
        SQLResult result;
        try {
            result = rmd.desti.getListSQL(sql, 999999999, false);
        } catch (ARException ex) {
            return retorno;
        }

        List<List<Value>> contents = result.getContents();
        for (List<Value> content : contents) {
            retorno.add(content.get(0).toString());
        }
        return retorno;
    }

}
