package br.com.cscbrasil.cscutl.common;

import br.com.cscbrasil.bmc.ARServerUser;
import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import com.bmc.arsys.api.ARErrors;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.ArchiveInfo;
import com.bmc.arsys.api.AuditInfo;
import com.bmc.arsys.api.CharacterField;
import com.bmc.arsys.api.CharacterFieldLimit;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Entry;
import com.bmc.arsys.api.EntryListInfo;
import com.bmc.arsys.api.Field;
import com.bmc.arsys.api.Filter;
import com.bmc.arsys.api.Form;
import com.bmc.arsys.api.IndexInfo;
import com.bmc.arsys.api.OverlaidInfo;
import com.bmc.arsys.api.QualifierInfo;
import com.bmc.arsys.api.RegularFieldMapping;
import com.bmc.arsys.api.SQLResult;
import com.bmc.arsys.api.SelectionField;
import com.bmc.arsys.api.SelectionFieldLimit;
import com.bmc.arsys.api.ServerInfoMap;
import com.bmc.arsys.api.SortInfo;
import com.bmc.arsys.api.StatusInfo;
import com.bmc.arsys.api.Timestamp;
import com.bmc.arsys.api.Value;
import com.bmc.arsys.artools.arexport.ARExport;
import com.bmc.thirdparty.org.apache.commons.lang.StringEscapeUtils;
import com.marzapower.loggable.Log;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author ivan
 */
public class Util {

    public static int[] fieldIdsMinimal = {Constants.AR_CORE_ENTRY_ID, Constants.AR_CORE_MODIFIED_DATE};

    public static void base(ARServerUser ar) {
        ar.setBaseOverlayFlag(true);
        ar.setOverlayFlag(false);
        ar.setOverlayGroup(Constants.AR_OVERLAY_CLIENT_MODE_BASE);
        ar.setDesignOverlayGroup(Constants.AR_OVERLAY_CLIENT_MODE_BASE);
    }

    public static void best(ARServerUser ar) {
        ar.setBaseOverlayFlag(false);
        ar.setOverlayFlag(true);
        ar.setOverlayGroup("1");
        ar.setDesignOverlayGroup("1");
    }

    public static void developmentCacheMode(ARServerUser ar, boolean par) throws ARException {
        if (1 == 1) {
            return;
        }
        ServerInfoMap serverInfoMap = new ServerInfoMap();
        if (par) {
            serverInfoMap.put(Constants.AR_SERVER_INFO_CACHE_MODE, new Value(1));
            serverInfoMap.put(Constants.AR_SERVER_INFO_DELAY_RECACHE_TIME, new Value(3600));
        } else {
            serverInfoMap.put(Constants.AR_SERVER_INFO_CACHE_MODE, new Value(0));
            serverInfoMap.put(Constants.AR_SERVER_INFO_DELAY_RECACHE_TIME, new Value(300));
        }
        ar.setServerInfo(serverInfoMap);
    }

    public static List<String> getDBTables(ARServerUser ar, String formName) {
        List<String> getDBTables = new ArrayList<>();
        int formId = getFormNumber(ar, formName);
        if (formId == 0) {
            return getDBTables;
        }
        getDBTables.add(String.format("B%d", formId));
        getDBTables.add(String.format("H%d", formId));
        SQLResult result = null;
        try {
            String cmd = String.format("Select fieldid from field inner join arschema on arschema.schemaid=field.schemaid where (arschema.name='%s' OR arschema.name='%s__o' OR arschema.name='%s__c') AND datatype=%d AND foption<%d", Util.escapeForDatabase(formName), Util.escapeForDatabase(formName), Util.escapeForDatabase(formName), Constants.AR_DATA_TYPE_ATTACH, Constants.AR_FIELD_OPTION_DISPLAY);
            result = ar.getListSQL(cmd, 999999999, false);
        } catch (ARException ex) {
            Log.get().fatal(ex.getMessage(), ex);
            return getDBTables;
        }
        List<List<Value>> records = result.getContents();
        for (List<Value> record : records) {
            Value v = record.get(0);
            getDBTables.add(String.format("B%dC%d", formId, v.getIntValue()));
        }
        getDBTables.add(String.format("T%d", formId));
        return getDBTables;
    }

    public static int getFormNumber(ARServerUser ar, String formName) {
        SQLResult result = null;
        try {
            String cmd = String.format("select schemaid from arschema where name='%s' order by name,schemaid", Util.escapeForDatabase(formName));
            result = ar.getListSQL(cmd, 10, false);
        } catch (ARException ex) {
            Log.get().fatal(ex.getMessage(), ex);
            return 0;
        }

        List<List<Value>> records = result.getContents();
        if (records.isEmpty()) {
            return 0;
        }
        if (records.get(0).isEmpty()) {
            return 0;
        }
        Value v = records.get(0).get(0);
        if (v == null) {
            return 0;
        }
        if (v.getValue() instanceof Integer) {
            return v.getIntValue();
        }
        return 0;
    }

    public static boolean existeIndiceC6(Form form) {
        boolean existeIndice = false;
        List<IndexInfo> indices = form.getIndexInfo();
        for (IndexInfo indice : indices) {
            List<Integer> campos = indice.getIndexFields();
            if (!campos.isEmpty()) {
                if (campos.get(0) == Constants.AR_CORE_MODIFIED_DATE) {
                    existeIndice = true;
                    break;
                }
            }
            if (existeIndice) {
                break;
            }
        }
        return existeIndice;
    }

    public static void longNap() {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException ex) {

        }
    }

    public static void normalNap() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {

        }
    }

    public static void minuteNap() {
        try {
            Thread.sleep(1000 * 60);
        } catch (InterruptedException ex) {

        }
    }

    public static void tinyNap() {
        try {
            Thread.sleep(15);
        } catch (InterruptedException ex) {

        }
    }

    public static void noNap() {
    }

    public static String escapeForDatabase(String queryparam) {
        return StringEscapeUtils.escapeSql(queryparam);
    }

    public static String getCustomization(ARServerUser ar, Form form) throws ARException {
        String customization;
        int overlayProp = Constants.AR_ORIGINAL_OBJECT;
        SQLResult sqlResult = ar.getListSQL(String.format("select OVERLAYPROP,OVERLAYGROUP from ARSCHEMA where NAME='%s' order by OVERLAYPROP asc", Util.escapeForDatabase(form.getName())), 999999999, false);
        List<List<Value>> results = sqlResult.getContents();
        for (List<Value> record : results) {
            overlayProp = record.get(0).getIntValue();
            break;
        }
        switch (overlayProp) {
            case Constants.AR_OVERLAID_OBJECT:
                customization = "Overlaid";
                break;
            case Constants.AR_OVERLAY_OBJECT:
                customization = "Overlay";
                break;
            case Constants.AR_CUSTOM_OBJECT:
                customization = "Custom";
                break;
            default:
                customization = "Unmodified";
                break;
        }
        return customization;
    }

    public static String getformType(Form form) {
        String retorno;
        int formTypeAux = form.getFormType();
        switch (formTypeAux) {
            case Constants.AR_SCHEMA_REGULAR:
                retorno = "Regular";
                break;
            case Constants.AR_SCHEMA_JOIN:
                retorno = "Join";
                break;
            case Constants.AR_SCHEMA_VIEW:
                retorno = "View";
                break;
            case Constants.AR_SCHEMA_DIALOG:
                retorno = "Display";
                break;
            case Constants.AR_SCHEMA_VENDOR:
                retorno = "Vendor";
                break;
            case Constants.AR_SCHEMA_PLACEHOLDER:
                retorno = "Placeholder";
                break;
            default:
                retorno = "Unknown";
                break;
        }

        ArchiveInfo a = form.getArchiveInfo();
        AuditInfo b = form.getAuditInfo();
        if (a.getArchiveType() == Constants.AR_ARCHIVE_NONE && a.getArchiveFrom() != null) {
            if (!a.getArchiveFrom().isEmpty()) {
                retorno = "Archive";
            }
        }
        if (b.getAuditStyle() == Constants.AR_AUDIT_NONE && b.getAuditForm() != null) {
            if (!b.getAuditForm().isEmpty()) {
                retorno = "Audit";
            }
        }
        if (b.getAuditStyle() == Constants.AR_AUDIT_LOG_SHADOW) {
            retorno = "Audit";
        }
        return retorno;
    }

    public static void deleteViaDBByEntryID(ARServerUser ar, String formName, String entryID, List<String> dbTables) throws ARException {
        for (String table : dbTables) {
            String chave = "C1";
            if (table.startsWith("H")) {
                chave = "entryId";
            }
            if (table.startsWith("B") && table.contains("C")) {
                chave = "entryId";
            }
            String cmd = String.format("delete from %s where %s='%s'", table, chave, entryID);
            try {
                SQLResult result = ar.getListSQL(cmd, 999999999, false);
            } catch (ARException ex) {
                if (table.startsWith("T")) {
                    Log.get().error("Erro ao eliminar o registro " + entryID + " do form " + formName + " em " + ar.getServer() + " " + cmd, ex);
                    throw ex;
                }
            }
        }
    }

    public static void deleteViaDBByEntryID(ARServerUser ar, String formName, List<String> entryIDs, List<String> dbTables) throws ARException {
        if (entryIDs == null) {
            return;
        }
        if (entryIDs.size() == 0) {
            return;
        }
        String entryID = "";
        for (int i = 0; i < entryIDs.size(); i++) {
            entryID += ",'" + entryIDs.get(i) + "'";
        }
        entryID = entryID.substring(1);

        for (String table : dbTables) {
            String chave = "C1";
            if (table.startsWith("H")) {
                chave = "entryId";
            }
            if (table.startsWith("B") && table.contains("C")) {
                chave = "entryId";
            }
            String cmd = String.format("delete from %s where %s in (%s)", table, chave, entryID);
            int a = 0;

            try {
                SQLResult result = ar.getListSQL(cmd, 999999999, false);
            } catch (ARException ex) {
                if (table.startsWith("T")) {
                    Log.get().error("Erro ao eliminar o registro " + entryID + " do form " + formName + " em " + ar.getServer() + " " + cmd, ex);
                    throw ex;
                }
            }

        }
    }

    public static void deleteViaDBByEntryID(ARServerUser ar, String formName, String entryID) throws ARException {
        List<String> dbTables = getDBTables(ar, formName);
        if (dbTables.isEmpty()) {
            Log.get().error("Impossivel determinar as tabelas relacionadas ao form " + formName + " em " + ar.getServer());
            return;
        }
        deleteViaDBByEntryID(ar, formName, entryID, dbTables);
    }

    public static void deleteViaDBByQuerySomenteT(ARServerUser ar, String formName, String qualifica) {
        int table = getFormNumber(ar, formName);
        if (table == 0) {
            Log.get().error(String.format("Impossivel determinar numero do form %s em %s", formName, ar.getServer()));
            return;
        }
        String cmd = String.format("delete from T%d where %s", table, qualifica);
        try {
            SQLResult result = ar.getListSQL(cmd, 999999999, false);
        } catch (ARException ex) {
            // ignora
        }
    }

    public static List<Field> getFields(ARServerUser sourc, String formName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static void deleteViaAPIByQuery(ARServerUser ar, String formName, String qualString) throws ARException {
        QualifierInfo qualification = ar.parseQualification(formName, qualString);
        deleteViaAPIByQuery(ar, formName, qualification);
    }

    public static void logError(ARServerUser retorno, ARException ex) {
        Log.get().error(retorno.getServer() + " - " + ex.getMessage());
    }

    static DateTime getGMTDateTimeFromEpoch(Value v) {
        if (v.getValue() == null) {
            v = new Value(0);
        }
        long instant = v.getIntValue();
        return Util.getGMTDateTimeFromEpoch(instant);
    }

    static DateTime getGMTDateTimeFromEpoch(long instant) {
        DateTime d = new DateTime(instant * 1000, DateTimeZone.UTC);
        return d;
    }

    static DateTime getLocalDateTimeFromEpoch(Value v) {
        if (v.getValue() == null) {
            v = new Value(0);
        }
        long instant = v.getIntValue();
        return Util.getLocalDateTimeFromEpoch(instant);
    }

    static DateTime getLocalDateTimeFromEpoch(long instant) {
        DateTime d = new DateTime(instant * 1000, DateTimeZone.getDefault());
        return d;
    }

    static String getCustomization(ARServerUser ar, Filter filter) throws ARException {
        String customization;
        int overlayProp = Constants.AR_ORIGINAL_OBJECT;

        SQLResult sqlResult = ar.getListSQL(String.format("select OVERLAYPROP,OVERLAYGROUP from FILTER where NAME='%s' order by OVERLAYPROP asc", Util.escapeForDatabase(filter.getName())), 999999999, false);
        List<List<Value>> results = sqlResult.getContents();
        for (List<Value> record : results) {
            overlayProp = record.get(0).getIntValue();
            break;
        }
        switch (overlayProp) {
            case Constants.AR_OVERLAID_OBJECT:
                customization = "Overlaid";
                break;
            case Constants.AR_OVERLAY_OBJECT:
                customization = "Overlay";
                break;
            case Constants.AR_CUSTOM_OBJECT:
                customization = "Custom";
                break;
            default:
                customization = "Unmodified";
                break;
        }
        return customization;
    }

    static void deleteViaDBByQuery(ARServerUser ar, String formName, String qualString) throws ARException {
        List<String> dbTables = getDBTables(ar, formName);
        QualifierInfo qualification = ar.parseQualification(formName, qualString);
        ArrayList<SortInfo> sortList = new ArrayList<>();
        sortList.add(new SortInfo(1, Constants.AR_SORT_ASCENDING));
        ArrayList<Field> entryListFields = new ArrayList<>();
        while (true) {
            List<EntryListInfo> listEntry = ar.getListEntry(formName, qualification, Constants.AR_START_WITH_FIRST_ENTRY, ar.cscGetMaxRecords(), sortList, entryListFields, false, null);
            if (listEntry.isEmpty()) {
                break;
            }
            for (EntryListInfo e : listEntry) {
                for (String table : dbTables) {
                    try {
                        String chave = "C1";
                        if (table.startsWith("H")) {
                            chave = "entryId";
                        }
                        if (table.startsWith("B") && table.contains("C")) {
                            chave = "entryId";
                        }
                        String cmd = String.format("delete from %s where %s='%s'", table, chave, e.getEntryID());
                        ar.getListSQL(cmd, 10, false);
                    } catch (ARException ex) {
                        // ignora
                    }
                }
            }
        }

    }

    static void deleteViaAPIByQuery(ARServerUser ar, String formName, QualifierInfo qualification) throws ARException {
        List<EntryListInfo> entries = ar.getListEntry(formName, qualification, Constants.AR_START_WITH_FIRST_ENTRY, 999999999, null, null, false, null);
        for (EntryListInfo entryInfo : entries) {
            ar.deleteEntry(formName, entryInfo.getEntryID(), Constants.AR_JOIN_DELOPTION_FORCE);
        }
    }

    public synchronized static void exportarARX(ARServerUser ar, String arxPath, String arxNameNoExtension, String formName, String qualString) throws Exception {
        File formDir;
        formDir = new File(arxPath);
        formDir.mkdirs();
        File outputFile = new File(formDir.getPath() + "/" + arxNameNoExtension.replaceAll("[^a-zA-Z0-9.-]", "_").toLowerCase() + ".arx");
        String[] parms = {"-u", ar.getUser(), "-x", ar.getServer(), "-p", ar.getPassword(), "-t", String.format("%d", ar.getPort()), "-form", formName, "-datafile", outputFile.getAbsolutePath(), "-q", qualString, "-timestamp", "1"};
        ARExport.main(parms);
    }

    static String getMinString(String s1, String s2) {
        if (s1 == null) {
            s1 = "";
        }
        if (s2 == null) {
            s2 = "";
        }
        if (s1.compareTo(s2) < 0) {
            return s1;
        }
        return s2;
    }

    static String getMaxString(String s1, String s2) {
        if (s1 == null) {
            s1 = "";
        }
        if (s2 == null) {
            s2 = "";
        }
        if (s1.compareTo(s2) < 0) {
            return s2;
        }
        return s1;
    }

    static long getServerTime(ARServerUser ar) throws ARException {
        int[] requestList = {Constants.AR_SERVER_INFO_SERVER_TIME};
        ServerInfoMap map;
        map = ar.getServerInfo(requestList);
        return map.get(Constants.AR_SERVER_INFO_SERVER_TIME).getIntValue();
    }

    private static boolean apagado(Entry record, List<Entry> registrosFonte) {
        String d1 = record.getEntryId();
        boolean retorno = true;
        for (Entry fonte : registrosFonte) {
            if (fonte.getEntryId().equals(d1)) {
                retorno = false;
                break;
            }
        }
        return retorno;
    }

    static void createFields(ARServerUser ar, List<Field> fields) {
        RemedyConnection rmd = RemedyConnections.get();
        Util.best(ar);
        Form form = null;
        String formName = null;
        for (Field fieldToCreate : fields) {
            boolean getForm = false;
            if (form == null) {
                getForm = true;
            } else if (!fieldToCreate.getForm().equals(formName)) {
                getForm = true;
            }
            if (getForm) {
                formName = fieldToCreate.getForm();
                try {
                    form = ar.getForm(fieldToCreate.getForm());
                } catch (ARException ex) {
                    Log.get().error(ex.getMessage());
                    continue;
                }
            }
            boolean criar = false;
            Field f;
            try {
                f = ar.getField(formName, fieldToCreate.getFieldID());
            } catch (ARException ex) {
                List<StatusInfo> lastStatus = ex.getLastStatus();
                if (lastStatus.size() == 1) {
                    if (lastStatus.get(0).getMessageNum() == ARErrors.AR_ERROR_NO_SUCH_FIELD) {
                        criar = true;
                    }
                }
                if (!criar) {
                    Log.get().error(ex.getMessage());
                    continue;
                }
            }
            if (criar) {
                String formType = Util.getformType(form);
                if (rmd.sourc.getServer().equals(ar.getServer())) {
                    if (!formType.equals("Regular")) {
                        continue;
                    }
                }

                String customization;
                try {
                    customization = Util.getCustomization(ar, form);
                } catch (ARException ex) {
                    Log.get().error(ex.getMessage());
                    continue;
                }
                if (customization.equalsIgnoreCase("unmodified")) {
                    OverlaidInfo o = new OverlaidInfo(formName, Constants.AR_STRUCT_ITEM_SCHEMA, formName, 0, null);
                    o.setFormName(formName);
                    o.setName(formName);
                    o.setInheritMask(Constants.AR_GRANULAR_PERMISSIONS | Constants.AR_GRANULAR_INDEX | Constants.AR_GRANULAR_OTHER);
                    try {
                        ar.createOverlay(o);
                    } catch (ARException ex) {
                        Log.get().error(ex.getMessage());
                        continue;
                    }
                }
                try {
                    Log.get().info(String.format("Criar campo %s em %s", fieldToCreate.getName(), formName));
                    ar.createField(fieldToCreate, false);
                } catch (ARException ex) {
                    Log.get().error(ex.getMessage());
                }
            }
        }
    }

    public static Field novoCampoChar(String formName, int fieldId, String fieldName, int maxLenght) {
        Field f = new CharacterField();
        f.setForm(formName);
        f.setName(fieldName.replaceAll("[^a-zA-Z0-9.-]", "_"));
        f.setFieldID(fieldId);
        f.setFieldOption(Constants.AR_FIELD_OPTION_OPTIONAL);
        f.setCreateMode(Constants.AR_FIELD_OPEN_AT_CREATE);
        f.setFieldMap(new RegularFieldMapping());
        f.setFieldLimit(new CharacterFieldLimit(maxLenght, Constants.AR_MENU_OVERWRITE, Constants.AR_QBE_MATCH_EQUAL, null, null, Constants.AR_LENGTH_UNIT_BYTE));
        return f;
    }

    public static Field novoCampoNaoSim(String formName, int fieldId, String fieldName) {
        Field f = new SelectionField();
        f.setForm(formName);
        f.setName(fieldName.replaceAll("[^a-zA-Z0-9.-]", "_"));
        f.setFieldID(fieldId);
        f.setFieldOption(Constants.AR_FIELD_OPTION_OPTIONAL);
        f.setCreateMode(Constants.AR_FIELD_OPEN_AT_CREATE);
        f.setFieldMap(new RegularFieldMapping());
        String[] l = {"Não", "Sim"};
        f.setFieldLimit(new SelectionFieldLimit(l));
        return f;
    }

    public static String timestamp2String(Timestamp timestamp) {
        DateTime jodaDateTime = new DateTime(timestamp.getValue() * 1000);
        DateTimeFormatter dtfOut = DateTimeFormat.forPattern("MM/dd/yyyy HH:mm:ss");
        return dtfOut.print(jodaDateTime.toDateTime(DateTimeZone.UTC)) + " GMT";
    }

    public static String epoch2String(long value) {
        DateTime jodaDateTime = new DateTime(value * 1000);
        DateTimeFormatter dtfOut = DateTimeFormat.forPattern("MM/dd/yyyy HH:mm:ss");
        return dtfOut.print(jodaDateTime);
    }

    public static List<List<Value>> simpleSQL(ARServerUser ar, String cmd, String p0, String p1, String p2, String p3, String p4, String p5, String p6, String p7, String p8, String p9) throws ARException {
        if (p0 != null) {
            if (p1 == null) {
                cmd = String.format(cmd, p0);
            } else {
                if (p2 == null) {
                    cmd = String.format(cmd, p0, p1);
                } else {
                    if (p3 == null) {
                        cmd = String.format(cmd, p0, p1, p2);
                    } else {
                        if (p4 == null) {
                            cmd = String.format(cmd, p0, p1, p2, p3);
                        } else {
                            if (p5 == null) {
                                cmd = String.format(cmd, p0, p1, p2, p3, p4);
                            } else {
                                if (p6 == null) {
                                    cmd = String.format(cmd, p0, p1, p2, p3, p4, p5);
                                } else {
                                    if (p7 == null) {
                                        cmd = String.format(cmd, p0, p1, p2, p3, p4, p5, p6);
                                    } else {
                                        if (p8 == null) {
                                            cmd = String.format(cmd, p0, p1, p2, p3, p4, p5, p6, p7);
                                        } else {
                                            if (p9 == null) {
                                                cmd = String.format(cmd, p0, p1, p2, p3, p4, p5, p6, p7, p8);
                                            } else {
                                                cmd = String.format(cmd, p0, p1, p2, p3, p4, p5, p6, p7, p8, p9);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        SQLResult listSQL = ar.getListSQL(cmd, 999999999, false);
        return listSQL.getContents();
    }

    public static List<List<Value>> simpleSQL(ARServerUser ar, String cmd, String p0, String p1, String p2, String p3, String p4, String p5, String p6, String p7, String p8) throws ARException {
        return simpleSQL(ar, cmd, p0, p1, p2, p3, p4, p5, p6, p7, p8, null);
    }

    public static List<List<Value>> simpleSQL(ARServerUser ar, String cmd, String p0, String p1, String p2, String p3, String p4, String p5, String p6, String p7) throws ARException {
        return simpleSQL(ar, cmd, p0, p1, p2, p3, p4, p5, p6, p7, null, null);
    }

    public static List<List<Value>> simpleSQL(ARServerUser ar, String cmd, String p0, String p1, String p2, String p3, String p4, String p5, String p6) throws ARException {
        return simpleSQL(ar, cmd, p0, p1, p2, p3, p4, p5, p6, null, null, null);
    }

    public static List<List<Value>> simpleSQL(ARServerUser ar, String cmd, String p0, String p1, String p2, String p3, String p4, String p5) throws ARException {
        return simpleSQL(ar, cmd, p0, p1, p2, p3, p4, p5, null, null, null, null);
    }

    public static List<List<Value>> simpleSQL(ARServerUser ar, String cmd, String p0, String p1, String p2, String p3, String p4) throws ARException {
        return simpleSQL(ar, cmd, p0, p1, p2, p3, p4, null, null, null, null, null);
    }

    public static List<List<Value>> simpleSQL(ARServerUser ar, String cmd, String p0, String p1, String p2, String p3) throws ARException {
        return simpleSQL(ar, cmd, p0, p1, p2, p3, null, null, null, null, null, null);
    }

    public static List<List<Value>> simpleSQL(ARServerUser ar, String cmd, String p0, String p1, String p2) throws ARException {
        return simpleSQL(ar, cmd, p0, p1, p2, null, null, null, null, null, null, null);
    }

    public static List<List<Value>> simpleSQL(ARServerUser ar, String cmd, String p0, String p1) throws ARException {
        return simpleSQL(ar, cmd, p0, p1, null, null, null, null, null, null, null, null);
    }

    public static List<List<Value>> simpleSQL(ARServerUser ar, String cmd, String p0) throws ARException {
        return simpleSQL(ar, cmd, p0, null, null, null, null, null, null, null, null, null);
    }

    public static List<List<Value>> simpleSQL(ARServerUser ar, String cmd) throws ARException {
        return simpleSQL(ar, cmd, null, null, null, null, null, null, null, null, null, null);
    }

    public static void deleteViaDBByEntryID(ARServerUser ar, String formName, List<String> ids) throws ARException {
        List<String> dbTables = getDBTables(ar, formName);
        if (dbTables.isEmpty()) {
            Log.get().error("Impossivel determinar as tabelas relacionadas ao form " + formName + " em " + ar.getServer());
            return;
        }
        deleteViaDBByEntryID(ar, formName, ids, dbTables);
    }

}
