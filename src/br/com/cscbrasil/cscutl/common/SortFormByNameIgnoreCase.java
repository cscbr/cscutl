/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.common;

import com.bmc.arsys.api.Form;
import java.util.Comparator;

/**
 *
 * @author ivan
 */
public class SortFormByNameIgnoreCase implements Comparator<Object> {

    @Override
    public int compare(Object o1, Object o2) {
        Form s1 = (Form) o1;
        Form s2 = (Form) o2;
        return s1.getName().toLowerCase().compareTo(s2.getName().toLowerCase());
    }

}
