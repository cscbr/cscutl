/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.aif;

import br.com.cscbrasil.bmc.ARServerUser;
import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.ActiveLink;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Container;
import com.bmc.arsys.api.ContainerOwner;
import com.bmc.arsys.api.Entry;
import com.bmc.arsys.api.EntryListInfo;
import com.bmc.arsys.api.Filter;
import com.bmc.arsys.api.Form;
import com.bmc.arsys.api.QualifierInfo;
import com.bmc.arsys.api.StatusInfo;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author ivan
 */
public class AifConfig {

    RemedyConnection rmd;
    ARServerUser ar;
    Form form;
    String formName;
    String woiName;
    String aotName;
    String pdtName;
    String reqCFGAdvancedInterface;
    String guiCFGAdvancedInterface;
    String friendlyName;
    String workOrderTemplate;
    String pdtInstanceId;
    String aotInstanceId;
    String operationId;
    private String rootInstanceId;
    private String assocGuid;
    private String flowGuid;
    private String srdName;
    private String cfgFormGuid;

    public AifConfig(String formName) {
        ar = RemedyConnections.get().desti;
        ar.cscSetModeBest();
        this.formName = formName;
        String[] split = formName.split(":");
        this.friendlyName = split[split.length - 1];
        this.woiName = friendlyName;// + " (Ordem Trabalho)";
        this.aotName = "AOT - " + friendlyName;
        this.pdtName = "PDT - " + friendlyName;
        this.srdName = "Solicitação - " + friendlyName;
    }

    public void execute() throws ARException {
        obterForm();
        registrarForm();
        activeLinks();
        Filters();
        Conteiners();
        Template();
        ApplicationObjectTemplate();
        ProcessDefinitionTemplate();
        SRD();
    }

    private void obterForm() throws ARException {
        Log.get().info("Obter form " + formName);
        form = ar.getForm(formName);
    }

    private void registrarForm() throws ARException {
        String interfaceForm = "SRS:CFGAdvancedInterface";
        Log.get().info("Registrar formulario avancado " + formName);
        String q = String.format("'CustomFormName'=\"%s\"", formName);
        QualifierInfo qualification = ar.parseQualification(interfaceForm, q);
        List<EntryListInfo> listEntry = ar.getListEntry(interfaceForm, qualification, Constants.AR_START_WITH_FIRST_ENTRY, 999999999, null, null, true, null);
        if (listEntry.size() != 0) {
            Log.get().info("   Ja estava registrado");
            int[] entryListFields = {179};
            Entry entry = ar.getEntry(interfaceForm, listEntry.get(0).getEntryID(), entryListFields);
            cfgFormGuid = entry.get(179).toString();

            return;
        }
        Entry e = new Entry();
        e.put(2, new Value(ar.getUser())); //  Submitter        
        e.put(7, new Value(0)); //  Status
        e.put(8, new Value("X")); //  Short Description
        e.put(112, new Value(";0;")); //  AssigneeGroupPermissions
        e.put(302826000, new Value(friendlyName)); // 	CustomFormTemplate_Name
        e.put(302826200, new Value(formName)); //	CustomFormName
        e.put(1000000001, new Value("- Global -")); //	Location Company
        //e.put(4, new Value()); //  Assigned To
        //e.put(160, new Value()); //  DefaultSRD_Locale
        //e.put(179, new Value()); //  instanceId
        //e.put(60900, new Value()); //  Vendor Assignee Groups
        //e.put(60901, new Value()); //  Vendor Assignee Groups_parent
        //e.put(60989, new Value()); //  AssigneeGroupPermissions_parent
        //e.put(301286600, new Value()); //	CustomFormServer
        //e.put(301600300, new Value()); //	DataTags
        //e.put(304234600, new Value()); //	Consumer
        reqCFGAdvancedInterface = ar.createEntry(interfaceForm, e);
        int[] entryListFields = {179};
        Entry entry = ar.getEntry(interfaceForm, reqCFGAdvancedInterface, entryListFields);
        guiCFGAdvancedInterface = entry.get(179).toString();
    }

    private void activeLinks() throws ARException {
        Log.get().info("Alterando Active Links");
        List<String> activeLinks = ar.getListActiveLink("GSC:RFXXX:AIF_Exemplo");

        for (String activeLink : activeLinks) {
            ActiveLink activeLink1 = ar.getActiveLink(activeLink);
            List<String> formList = activeLink1.getFormList();
            if (!formList.contains(formName)) {
                formList.add(formName);
                activeLink1.setFormList(formList);
                ar.setActiveLink(activeLink1);
            }
        }
    }

    private void Filters() throws ARException {
        Log.get().info("Alterando Filters");
        List<String> filters = ar.getListFilter("GSC:RFXXX:AIF_Exemplo");
        for (String filter : filters) {
            /*
            if (filter.equals("GSC:RFXXX:AIF_200_Service_Guide_05_Submeter")) {
                Filter f = ar.getFilter(filter);
                String filterName = formName + "_200_Service_Guide_05_Submeter";
                f.setName(filterName);
                List<String> novoFormList = new ArrayList<>();
                novoFormList.add(formName);
                f.setFormList(novoFormList);
                List<FilterAction> actionList = f.getActionList();
                for (FilterAction action : actionList) {
                    if (action instanceof PushFieldsAction) {
                        PushFieldsAction push = (PushFieldsAction) action;
                        push.setPushToForm(formName);
                    }
                }
                f.setActionList(actionList);

                try {
                    ar.deleteFilter(filterName, 0);
                } catch (ARException ex) {

                }
                ar.createFilter(f);
                continue;
            }
            */
            Filter filter1 = ar.getFilter(filter);
            List<String> formList = filter1.getFormList();
            if (!formList.contains(formName)) {
                formList.add(formName);
                filter1.setFormList(formList);
                ar.setFilter(filter1);
            }
        }
    }

    private void Conteiners() throws ARException {
        Log.get().info("Alterando Guides");
        int[] containerTypes = {0};
        List<Container> containers = ar.getListContainerObjects(null);
        int a = 0;
        for (Container container : containers) {
            if (!container.getContainerOwner().contains(new ContainerOwner(2, "GSC:RFXXX:AIF_Exemplo"))) {
                continue;
            }
            List<ContainerOwner> containerOwner = container.getContainerOwner();
            ContainerOwner co = new ContainerOwner(2, formName);
            if (!containerOwner.contains(co)) {
                containerOwner.add(co);
                container.setContainerOwner(containerOwner);
                ar.setContainer(container);
            }
            /*
            if (container.getName().equals("GSC:RFXXX:AIF_Service_FilterGuide_Submeter")) {
                List<Reference> references = container.getReferences();
                references.add(new Reference("", "", ReferenceType.FILTER, formName + "_200_Service_Guide_05_Submeter"));
                container.setReferences(references);
                ar.setContainer(container);
            }
            */
        }
    }
    //Filter("GSC:RFXXX:AIF_Exemplo");        

    private void Template() throws ARException {
        String templateForm = "WOI:Template";
        Log.get().info("Criar Template de Ordem de Trabalho");
        String q = String.format("'8'=\"%s\"", woiName);
        QualifierInfo qualification = ar.parseQualification(templateForm, q);
        List<EntryListInfo> listEntry = ar.getListEntry(templateForm, qualification, Constants.AR_START_WITH_FIRST_ENTRY, 999999999, null, null, true, null);
        if (listEntry.size() != 0) {
            Log.get().info("   Ja estava registrado");
            int[] entryListFields = {179};
            Entry entry = ar.getEntry(templateForm, listEntry.get(0).getEntryID(), entryListFields);
            workOrderTemplate = entry.get(179).toString();
            return;
        }

        Entry e = new Entry();
        e.put(2, new Value(ar.getUser())); // Submitter
        //e.put(4, new Value()); // Assigned To
        e.put(7, new Value(1)); // Status
        e.put(8, new Value(woiName)); // Template Name
        e.put(112, new Value(";1000000025;")); // Assignee Groups
        //e.put(179, new Value()); // GUID
        //e.put(60900, new Value()); // Vendor Assignee Groups
        //e.put(60901, new Value()); // Vendor Assignee Groups_parent
        //e.put(60989, new Value()); // Assignee Groups_parent
        //e.put(10000101, new Value()); // Notes
        e.put(10007100, new Value(friendlyName)); // Name
        //e.put(10007101, new Value()); // Category
        //e.put(200000003, new Value()); // Product Categorization Tier 1
        //e.put(200000004, new Value()); // Product Categorization Tier 2
        //e.put(200000005, new Value()); // Product Categorization Tier 3
        //e.put(200000007, new Value()); // Site Group
        //e.put(200000012, new Value()); // Region
        //e.put(200000020, new Value()); // Business Service
        //e.put(240001002, new Value()); // Product Name
        //e.put(260000001, new Value()); // Site
        //e.put(301405500, new Value()); // MappedTemplateInstanceId
        e.put(301405600, new Value(woiName)); // MappedTemplate_Name
        e.put(301405700, new Value(friendlyName)); // MappedTemplatedDescription
        //e.put(301405800, new Value()); // Used_by_SRMS
        //e.put(301600300, new Value()); // DataTags
        //e.put(301600500, new Value()); // z1D_StatusQuerySelection
        e.put(303408700, new Value()); // WO Type Field 10
        e.put(303408800, new Value()); // WO Type Field 11
        e.put(303408900, new Value()); // WO Type Field 12
        e.put(303409000, new Value()); // WO Type Field 13
        e.put(303409100, new Value()); // WO Type Field 14
        e.put(303409200, new Value()); // WO Type Field 15
        e.put(303409300, new Value()); // WO Type Field 16
        e.put(303409400, new Value()); // WO Type Field 17
        e.put(303409500, new Value()); // WO Type Field 18
        e.put(303409600, new Value()); // WO Type Field 19
        e.put(303409700, new Value()); // WO Type Field 20
        e.put(303409800, new Value()); // WO Type Field 21
        e.put(303409900, new Value()); // WO Type Field 22
        e.put(303410000, new Value()); // WO Type Field 23
        e.put(303410100, new Value()); // WO Type Field 24
        e.put(303410200, new Value()); // WO Type Field 25
        e.put(303410300, new Value()); // WO Type Field 26
        e.put(303410400, new Value()); // WO Type Field 27
        e.put(303410500, new Value()); // WO Type Field 28
        e.put(303410600, new Value()); // WO Type Field 29
        e.put(303410700, new Value()); // WO Type Field 30
        e.put(303562800, new Value(".")); // WO Type Field 10 Label
        e.put(303563300, new Value(".")); // WO Type Field 11 Label
        e.put(303563400, new Value(".")); // WO Type Field 12 Label
        e.put(303563500, new Value(".")); // WO Type Field 13 Label
        e.put(303563600, new Value(".")); // WO Type Field 14 Label
        e.put(303563700, new Value(".")); // WO Type Field 15 Label
        e.put(303563800, new Value(".")); // WO Type Field 16 Label
        e.put(303563900, new Value(".")); // WO Type Field 17 Label
        e.put(303564000, new Value(".")); // WO Type Field 18 Label
        e.put(303564100, new Value(".")); // WO Type Field 19 Label
        e.put(303564200, new Value(".")); // WO Type Field 20 Label
        e.put(303564300, new Value(".")); // WO Type Field 21 Label
        e.put(303564400, new Value(".")); // WO Type Field 22 Label
        e.put(303564500, new Value(".")); // WO Type Field 23 Label
        e.put(303564600, new Value(".")); // WO Type Field 24 Label
        e.put(303564700, new Value(".")); // WO Type Field 25 Label
        e.put(303564800, new Value(".")); // WO Type Field 26 Label
        e.put(303564900, new Value(".")); // WO Type Field 27 Label
        e.put(303565000, new Value(".")); // WO Type Field 28 Label
        e.put(303565100, new Value(".")); // WO Type Field 29 Label
        e.put(303565200, new Value(".")); // WO Type Field 30 Label
        e.put(303820400, new Value(0)); // z1D_remove_colcount
        e.put(304057200, new Value("CSC BRASIL")); // z1D_company
        e.put(304211400, new Value()); // z1D_OldLocationCompanyGroupID
        e.put(304211500, new Value()); // z1D_NewLocationCompanyGroupID
        e.put(304271150, new Value()); // ASLOGID
        e.put(304324800, new Value()); // z1D_Company_Get
        e.put(304327340, new Value("Yes")); // z1G_WOTaskDisplayOptionSettings
        e.put(304339421, new Value()); // z1D_FLagOpenWindow
        e.put(304340611, new Value()); // z1D template
        e.put(304340621, new Value()); // z1D ExtQualification
        e.put(304382031, new Value()); // WO Type Field 48
        e.put(304382041, new Value()); // WO Type Field 49
        e.put(304382051, new Value()); // WO Type Field 50
        e.put(304382061, new Value()); // WO Type Field 51
        e.put(304382071, new Value(".")); // WO Type Field 48 Label
        e.put(304382081, new Value(".")); // WO Type Field 49 Label
        e.put(304382091, new Value(".")); // WO Type Field 50 Label
        e.put(304382101, new Value(".")); // WO Type Field 51 Label
        e.put(377770001, new Value(".")); // WO Type Field 1 Label
        e.put(377770002, new Value(".")); // WO Type Field 2 Label
        e.put(377770003, new Value(".")); // WO Type Field 3 Label
        e.put(377770004, new Value(".")); // WO Type Field 4 Label
        e.put(377770005, new Value(".")); // WO Type Field 5 Label
        e.put(377770006, new Value(".")); // WO Type Field 6 Label
        e.put(377770007, new Value(".")); // WO Type Field 7 Label
        e.put(377770008, new Value(".")); // WO Type Field 8 Label
        e.put(377770009, new Value(".")); // WO Type Field 9 Label
        e.put(377770011, new Value()); // WO Type Field 1 Value
        e.put(377770012, new Value()); // WO Type Field 2 Value
        e.put(377770013, new Value()); // WO Type Field 3 Value
        e.put(377770014, new Value()); // WO Type Field 4 Value
        e.put(377770015, new Value()); // WO Type Field 5 Value
        e.put(377770016, new Value()); // WO Type Field 6 Value
        e.put(377770017, new Value()); // WO Type Field 7 Value
        e.put(377770018, new Value()); // WO Type Field 8 Value
        e.put(377770019, new Value()); // WO Type Field 9 Value
        e.put(400129200, new Value()); // CI_ReconId
        e.put(1000000000, new Value("Ordem Trabalho - " + friendlyName)); // Summary
        e.put(1000000001, new Value("CSC BRASIL")); // Location Company
        e.put(1000000014, new Value()); // Support Organization
        e.put(1000000015, new Value()); // Support Group Name
        e.put(1000000038, new Value()); // Chg Location Address
        e.put(1000000063, new Value()); // Categorization Tier 1
        e.put(1000000064, new Value()); // Categorization Tier 2
        e.put(1000000065, new Value()); // Categorization Tier 3
        e.put(1000000079, new Value()); // Support Group ID
        e.put(1000000082, new Value("CSC BRASIL")); // Company
        e.put(1000000085, new Value(1)); // Add Request For:
        e.put(1000000181, new Value(2000)); // Work Order Type
        e.put(1000000251, new Value()); // Company3
        e.put(1000000403, new Value()); // CAB Manager ( Change Co-ord )
        e.put(1000000408, new Value()); // CAB Manager Login
        e.put(1000001270, new Value()); // Product Cat Tier 1(2)
        e.put(1000001271, new Value()); // Product Cat Tier 2 (2)
        e.put(1000001272, new Value()); // Product Cat Tier 3 (2)
        e.put(1000002268, new Value()); // Product Name (2)
        e.put(1000002269, new Value()); // Product Model/Version (2)
        e.put(1000002270, new Value()); // Manufacturer (2)
        e.put(1000003227, new Value()); // ASORG
        e.put(1000003228, new Value()); // ASCPY
        e.put(1000003229, new Value()); // ASGRP
        e.put(1000003230, new Value()); // ASCHG
        e.put(1000003234, new Value()); // ASGRPID
        String entryId = ar.createEntry(templateForm, e);

        int[] entryListFields = {179};
        Entry entry = ar.getEntry(templateForm, entryId, entryListFields);
        workOrderTemplate = entry.get(179).toString();
    }

    private void ApplicationObjectTemplate() throws ARException {
        Log.get().info("Criar Template Objeto Aplicacao");
        String q = String.format("'301287500'=\"%s\"", aotName);
        QualifierInfo qualification = ar.parseQualification("SRM:AppTemplateBridge", q);
        List<EntryListInfo> listEntry = ar.getListEntry("SRM:AppTemplateBridge", qualification, Constants.AR_START_WITH_FIRST_ENTRY, 999999999, null, null, true, null);
        if (listEntry.size() != 0) {
            Log.get().info("   Ja estava registrado");
            int[] entryListFields = {179};
            Entry entry = ar.getEntry("SRM:AppTemplateBridge", listEntry.get(0).getEntryID(), entryListFields);
            aotInstanceId = entry.get(179).toString();
            return;
        }

        Entry e = new Entry();
        e.put(2, new Value(ar.getUser())); // Submitter (required)
        e.put(7, new Value(1)); // Status (required)
        e.put(8, new Value(friendlyName)); // TemplateSummary (required)
        e.put(112, new Value("1000000025;")); // Assignee Groups (optional)        
        e.put(300062100, new Value("TEMPLATE")); // Type (required)
        e.put(301287400, new Value(workOrderTemplate)); // TemplateInstanceID (optional)
        e.put(301287500, new Value(aotName)); // Name (required)        
        e.put(301287600, new Value("CA001143D417CBG#eXRQbB3uCAPvoA")); // AppRegistryInstanceID (optional)
        e.put(301289000, new Value("Work Order Application")); // AppRegistryName (required)
        e.put(301453000, new Value("CSC BRASIL")); // Company (required)
        //e.put(303386600, new Value("AGGAAC47C58A0AOWNC07OVQSJ5072A")); // AOTInstanceID_DIspProp (optional)
        e.put(303439400, new Value(friendlyName)); // End User Displayed Name (optional)
        e.put(1000001437, new Value(woiName)); // Template Name (optional)
        /*
        e.put(301289100, new Value()); // TemplateRequestID (optional)
        e.put(179, new Value()); // AOTInstanceID (optional)
        e.put(4, new Value()); // Assigned To (optional)
        e.put(160, new Value()); // Locale (optional)
        e.put(60989, new Value()); // Assignee Groups_parent (optional)
        e.put(301600300, new Value()); // DataTags (optional)
        e.put(302911400, new Value()); // URL (optional)
         */
        String entryId = ar.createEntry("SRM:AppTemplateBridge", e);

        listEntry = ar.getListEntry("SRM:AppTemplateBridge", qualification, Constants.AR_START_WITH_FIRST_ENTRY, 999999999, null, null, true, null);
        if (listEntry.size() != 0) {
            int[] entryListFields = {179};
            Entry entry = ar.getEntry("SRM:AppTemplateBridge", listEntry.get(0).getEntryID(), entryListFields);
            aotInstanceId = entry.get(179).toString();
            return;
        }

        int[] entryListFields = {179};
        Entry entry = ar.getEntry("SRM:AppTemplateBridge", entryId, entryListFields);
        aotInstanceId = entry.get(179).toString();

    }

    private void ProcessDefinitionTemplate() throws ARException {
        pdtInstanceId = null;
        Log.get().info("Criar Template Processo");
        String q = String.format("'301244700'=\"%s\"", pdtName);
        QualifierInfo qualification = ar.parseQualification("SRM:ProcessDefinitionTemplate", q);
        List<EntryListInfo> listEntry = ar.getListEntry("SRM:ProcessDefinitionTemplate", qualification, Constants.AR_START_WITH_FIRST_ENTRY, 999999999, null, null, true, null);
        if (listEntry.size() != 0) {
            Log.get().info("   Ja estava registrado");
            int[] entryListFields = {179};
            Entry entry = ar.getEntry("SRM:ProcessDefinitionTemplate", listEntry.get(0).getEntryID(), entryListFields);
            pdtInstanceId = entry.get(179).toString();
        }

        if (pdtInstanceId == null) {
            UUID uuid1 = UUID.randomUUID();
            UUID uuid2 = UUID.randomUUID();
            pdtInstanceId = uuid1.toString().replaceAll("-", "");
            operationId = uuid2.toString().replaceAll("-", "");
            SRMAssociations();
            Entry e = new Entry();
            e.put(2, new Value(ar.getUser())); // z1S_Submitter (required)
            e.put(7, new Value(1)); // Status (required)
            e.put(112, new Value("1000000025;")); // Assignee Groups (optional)                
            e.put(300317500, new Value(0)); // SystemStatus (required)
            e.put(301244700, new Value(pdtName)); // PDT_Name (required)
            e.put(301438012, new Value(3000)); // Request Type (optional)
            e.put(301453000, new Value("CSC BRASIL")); // Company (required)        
            e.put(303439400, new Value(friendlyName)); // End User Displayed Name (optional)
            e.put(490000200, new Value(1)); // deleted (optional)
            e.put(490000700, new Value(operationId)); // OperationID (optional)
            e.put(179, new Value(pdtInstanceId)); // InstanceId (optional)
            String entryId = ar.createEntry("SRM:ProcessDefinitionTemplate", e);
        } else {
            SRMAssociations();
        }
        SRMAppTemplateFlow();
    }

    private void SRMAssociations() throws ARException {
        String q = String.format("'301377600'=\"%s\" AND '301377700'=\"%s\"", pdtInstanceId, aotInstanceId);
        QualifierInfo qualification = ar.parseQualification("SRM:Associations", q);
        List<EntryListInfo> listEntry = ar.getListEntry("SRM:Associations", qualification, Constants.AR_START_WITH_FIRST_ENTRY, 999999999, null, null, true, null);
        if (listEntry.size() != 0) {
            int[] entryListFields = {179};
            Entry entry = ar.getEntry("SRM:Associations", listEntry.get(0).getEntryID(), entryListFields);
            assocGuid = entry.get(179).toString();
            return;
        }

        Entry e = new Entry();
        e.put(2, new Value(ar.getUser())); // Submitter (required)
        e.put(4, new Value("not used")); // Assigned To (optional)
        e.put(7, new Value(0)); // Status (required)
        e.put(8, new Value("not used")); // Short Description (required)
        e.put(112, new Value(";1000000025;")); // Assignee Groups (optional)
        e.put(300059000, new Value(0)); // RecordStatus (required)
        e.put(301181100, new Value(formName)); // Assoc2Name (optional)
        e.put(301257300, new Value(0)); // SessionStatus (optional)
        e.put(301284900, new Value("SRM:ProcessDefinitionTemplate_Base")); // Assoc1FormID (optional)
        e.put(301285000, new Value("SRM:AppTemplateBridge_Base")); // Assoc2FormID (optional)
        e.put(301285100, new Value(0)); // AssocType (optional)
        e.put(301377600, new Value(pdtInstanceId)); // Assoc1InstanceID (optional)        
        e.put(301377700, new Value(aotInstanceId)); // Assoc2InstanceID (optional)
        e.put(301521000, new Value(1)); // InstanceNumber (optional)
        e.put(302775700, new Value(1)); // ChildType (optional)
        e.put(304345161, new Value(1)); // z1D_NodeOrder (optional)
        e.put(301257200, new Value()); // Deleted (optional)   


        /*
     
        e.put(179, new Value("SRGAAC47C58A0AOWO9T2OVSJS00L5G")); // InstanceID (optional)
        e.put(60900, new Value()); // Vendor Assignee Groups (optional)
        e.put(60901, new Value()); // Vendor Assignee Groups_parent (optional)
        e.put(60989, new Value()); // Assignee Groups_parent (optional)
        
        e.put(301377900, new Value()); // zTmpUsedInFlow (optional)
        e.put(301378000, new Value()); // zTmpFlowInstanceID (optional)
        e.put(301378100, new Value()); // zTmpFlowSequence (optional)
        e.put(301600300, new Value()); // DataTags (optional)
        e.put(302773900, new Value()); // Service Request Name (optional)
        e.put(302774000, new Value()); // Service Request InstanceID (optional)
        e.put(302774100, new Value()); // Parent Type (optional)
        e.put(302777500, new Value()); // AssociationTemplateInstanceID (optional)
        e.put(303859400, new Value()); // XXCondition TF (optional)
        e.put(303953200, new Value()); // RelatedConditionAssociationId (optional)
        e.put(304301770, new Value()); // MileStone (optional)
        e.put(304307360, new Value()); // Image Instance ID (optional)
        e.put(304307560, new Value()); // Process_AOI_Completed (optional)
        e.put(1000000077, new Value()); // XXConditionInstanceID (optional)
        e.put(301285200, new Value()); // Tag (optional)
        e.put(301285300, new Value()); // zParamCommandAction (optional)
        e.put(301295900, new Value()); // SessionID (optional)
         */
        String entryId = ar.createEntry("SRM:Associations", e);
        int[] entryListFields = {179};
        Entry entry = ar.getEntry("SRM:Associations", entryId, entryListFields);
        assocGuid = entry.get(179).toString();
    }

    private void SRMAppTemplateFlow() throws ARException {
        String q = String.format("'301373600'=\"%s\" AND '301373800'=\"%s\"", pdtInstanceId, assocGuid);
        QualifierInfo qualification = ar.parseQualification("SRM:AppTemplateFlow", q);
        List<EntryListInfo> listEntry = ar.getListEntry("SRM:AppTemplateFlow", qualification, Constants.AR_START_WITH_FIRST_ENTRY, 999999999, null, null, true, null);
        if (listEntry.size() != 0) {
            int[] entryListFields = {179};
            Entry entry = ar.getEntry("SRM:AppTemplateFlow", listEntry.get(0).getEntryID(), entryListFields);
            flowGuid = entry.get(179).toString();
            return;
        }

        UUID uuid1 = UUID.randomUUID();
        rootInstanceId = uuid1.toString().replaceAll("-", "");
        Entry e = new Entry();
        e.put(2, new Value(ar.getUser())); // Submitter (required)
        e.put(4, new Value("not used")); // Assigned To (optional)
        e.put(7, new Value(0)); // Status (required)
        e.put(8, new Value("x")); // Short Description (required)
        //e.put(179, new Value("AGGAAC47C58A0AOWOMTROVS2SP0LNZ")); // InstanceID (optional)
        e.put(10007405, new Value(0)); // Evaluate if Predecessor Failure? (optional)
        e.put(10007406, new Value(1)); // Evaluate if Predecessor Canceled? (optional)
        e.put(10007407, new Value(0)); // Flow to Successor when (optional)
        e.put(300059000, new Value(0)); // RecordStatus (required)
        //e.put(301257200, new Value()); // Deleted (optional)
        //e.put(301257300, new Value()); // SessionStatus (optional)
        e.put(301284900, new Value("START")); // PredFormID (optional)
        e.put(301285000, new Value("SRM_SCO_ASSOC")); // SuccFormID (optional)
        //e.put(301285300, new Value()); // zParamCommandAction (optional)
        //e.put(301285400, new Value()); // zParamType (optional)
        //e.put(301296300, new Value()); // zParamPredFlowInstanceID (optional)
        //e.put(301299500, new Value()); // zTmpSessionID (optional)
        e.put(301299600, new Value(0)); // zTmpChangeFlag (optional)
        //e.put(301373000, new Value()); // zTmpUsedInFlowInstanceID (optional)
        //e.put(301373100, new Value()); // zTmpInteger_01 (optional)
        //e.put(301373500, new Value()); // zParamSuccFlowInstanceID (optional)
        e.put(301373600, new Value(pdtInstanceId)); // RootInstanceID (optional)
        e.put(301373800, new Value(assocGuid)); // SuccInstanceID (optional)
        e.put(301373900, new Value("START_INSTANCE_ID")); // PredInstanceID (optional)
        e.put(301375900, new Value(1)); // InstanceNumber (optional)
        e.put(301376100, new Value(1)); // SequenceNumber (optional)
        e.put(301376200, new Value(0)); // zTmpLocked (optional)
        //e.put(301437800, new Value()); // DoneQual (optional)
        //e.put(301600300, new Value()); // DataTags (optional)
        //e.put(303859400, new Value()); // Condition TF (optional)
        //e.put(303859600, new Value()); // Condition_InstanceID (optional)
        String entryId = ar.createEntry("SRM:AppTemplateFlow", e);
    }

    private void SRD() throws ARException {
        Log.get().info("Criar Solicitacao");
        String srdNumber = SRD_Number();
        Entry e = new Entry();
        e.put(2, new Value(ar.getUser())); // Submitter (required)
        e.put(7, new Value(0)); // SRD_Status (required)
        e.put(8, new Value(srdName)); // ShortDescription (required)
        e.put(112, new Value("1000000025;")); // Assignee Groups (optional)
        //e.put(179, new Value("AGGAAC47C58A0AOWPDUIOVSTTG0NKI")); // InstanceId (optional)
        e.put(14506, new Value(srdName)); // Description (optional)
        e.put(230000009, new Value("SERVICE_CATALOG")); // FormKeyword (optional)
        e.put(260000001, new Value("CSC BRASIL SP")); // RequestedBySite (optional)
        e.put(300317500, new Value(0)); // SRD_ManualOfflineFlag (optional)
        e.put(300991100, new Value(4)); // ???QuestionsPerPage (optional)
        e.put(301236000, new Value("Service Request Definition - Ad Hoc")); // Approval Process Name (optional)
        e.put(301244700, new Value(friendlyName)); // SRD_Title (required)
        e.put(301244800, new Value(srdName)); // SRD_Description (required)
        e.put(301248900, new Value(1)); // ServiceRequestApproval? (required)
        e.put(301251901, new Value("Ivan Yort")); // RequestCatalogManager_FullName (required)
        e.put(301251902, new Value("CSC BRASIL")); // RequestCatalogManager_Company (required)
        e.put(301252600, new Value("Ivan Yort")); // RequestedByName (optional)
        e.put(301252700, new Value("55 11 9933-0369")); // RequestedByPhone (optional)
        e.put(301266800, new Value("fibra.cscbrasil@gmail.com")); // ServiceOwnerEmail (optional)
        e.put(301268300, new Value(2)); // ApprovalProcessType (optional)
        e.put(301323400, new Value(1)); // MaxApprovalLevels (optional)
        e.put(301436200, new Value("APD000000000309")); // ApprovalDefinitionID (optional)
        e.put(301438012, new Value(3000)); // SRD_Request Type (required)
        e.put(301453000, new Value("CSC BRASIL")); // SRD_LocationCompany (required)
        e.put(302771600, new Value(3)); // Request Frequency (optional)
        //e.put(302771601, new Value([Timestamp = 1506108253])      
        e.put(302771610, new Value(3)); // Recent Request Count (optional)
        e.put(302783800, new Value(0.00)); // SRD_Expected Cost (optional)
        e.put(302785000, new Value(1)); // SystemRequest (optional)
        e.put(302792000, new Value(1000)); // TakeSRDOfflineFlag (optional)
        e.put(302793000, new Value(0.00)); // SRD_Price (optional)
        e.put(302793500, new Value(1)); // SRD_TurnaroundTimeX (optional)
        e.put(302793600, new Value(2000)); // SRD_TurnaroundTimeUnits (optional)
        e.put(302796800, new Value(friendlyName)); // SRD_Title_Level (optional)
        e.put(302798500, new Value(0)); // OptionChooseService_TurnaroundTime (optional)
        e.put(302798600, new Value(0)); // OptionChooseService_Price (optional)
        e.put(302798700, new Value(0)); // OptionRequestWizard_ExpectedCompletion (optional)
        e.put(302798800, new Value(0)); // OptionRequestWizard_RequiredBy (optional)
        e.put(302798900, new Value(0)); // OptionRequestWizard_Phone (optional)
        e.put(302799000, new Value(0)); // OptionRequestWizard_Email (optional)
        e.put(302799100, new Value(0)); // OptionRequestWizard_Notes (optional)
        e.put(302799200, new Value(0)); // OptionRequestWizard_Attachment (optional)
        e.put(302799300, new Value(0)); // OptionRequestWizard_ReviewSummary (optional)
        e.put(302799400, new Value(0)); // OptionViewRequest_ServiceCoordinator (optional)
        e.put(302799500, new Value(0)); // OptionViewRequest_Price (optional)
        e.put(302799600, new Value(0)); // OptionRequestDetails_ExpectedCompletion (optional)
        e.put(302799700, new Value(0)); // OptionRequestDetails_RequiredBy (optional)
        e.put(302799800, new Value(0)); // OptionRequestDetails_Phone (optional)
        e.put(302799900, new Value(0)); // OptionRequestDetails_Email (optional)
        e.put(302800000, new Value(0)); // OptionRequestDetails_Notes (optional)
        e.put(302800100, new Value(0)); // OptionRequestWizard_Instructions (optional)
        e.put(302800600, new Value(1)); // SRD_LocaleStatus (required)
        //e.put(302804800, new Value("SRGAAC47C58A0AOWPA9VOVSQ8T0MX8")); // SRD_InstanceIdDispProp (optional)
        e.put(302813500, new Value(1000)); // DELETEManuallyTakenOffline (optional)
        //e.put(302822400, new Value("AG001143D417CBv8qvRQ#GBbEQjRkJ")); // ImageInstanceId (optional)
        //e.put(302825800, new Value(friendlyName)); // Custom_Template (optional)
        //e.put(302825900, new Value(cfgFormGuid)); // Custom_CFG_Form_InstanceId (optional)
        e.put(302830900, new Value("IDGAAC47FC9Q2AOOO0GSONS06X4T3T")); // Category Tag1 (required)
        e.put(302849400, new Value(srdNumber)); // SRD_Number (required)
        e.put(302851900, new Value(0)); // NT_SubmitNotification (optional)
        e.put(302852000, new Value(0)); // NT_AssignNotification (optional)
        e.put(302852100, new Value(0)); // NT_ApproverNotification (optional)
        e.put(302852300, new Value(0)); // NT_CompleteNotification (optional)
        e.put(302852400, new Value(0)); // NT_CancelledNotification (optional)
        e.put(302852500, new Value(0)); // NT_PendingNotification (optional)
        e.put(302858800, new Value(0)); // CreateBusinessProcess (optional)
        //e.put(302874000, new Value(pdtInstanceId)); // PDT_InstanceID (optional)
        //e.put(302876900, new Value(pdtName)); // PDT_Name (optional)
        e.put(302896600, new Value(0)); // UseApprovalEngFlag_SRD (optional)
        e.put(302901900, new Value(ar.getUser())); // CatManLoginID (optional)
        e.put(302903500, new Value(1)); // SurveyStatus (optional)
        e.put(302920500, new Value(0)); // SRDSettingsUsed (optional)
        e.put(303025700, new Value(0)); // OptionRequestDetails_ServiceCoordinator (optional)
        e.put(303025800, new Value(0)); // OptionRequestDetails_Price (optional)
        //e.put(303055600, new Value()); // Search Cache (optional)
        e.put(303484000, new Value("CSC BRASIL")); // RequestCoordinator_Company (optional)
        e.put(303490000, new Value(0)); // OptionRequestDetails_Quantity (optional)
        e.put(303490100, new Value(0)); // OptionRequestWizard_Quantity (optional)
        e.put(303616900, new Value(0)); // OptionRequestDetails_ProcessView (optional)
        e.put(303772800, new Value("SRM_NONE")); // z1D_Approval_Keyword (optional)
        e.put(303785300, new Value("IDGAAC47FC9Q2AOOO0GSONS06X4T3T")); // Category1_Instance ID (optional)
        e.put(304158000, new Value(0)); // OptionRequestDetails_Approvals (optional)
        e.put(304160300, new Value(0)); // OptionApprovals (optional)
        e.put(304163900, new Value(1)); // ReopenFulfillment (optional)
        e.put(304240550, new Value(0)); // RO_OfferingType (optional)
        //e.put(304242570, new Value("OI-53b207505a29430097af146014357f14")); // SRD_RO_InstanceID (optional)
        //e.put(304242580, new Value("REGAAC47C58A0AOWPDUIOVSTTG0NK6")); // SRD_RO_ReconID (optional)
        //e.put(304242590, new Value("BMC.ASSET")); // SRD_RO_DatasetID (optional)
        e.put(304258520, new Value(2000)); // SRD_AddOn (optional)
        e.put(490000200, new Value(1)); // deleted (optional)
        e.put(490000700, new Value(operationId)); // OperationID (optional)
        e.put(1000000001, new Value("CSC BRASIL")); // RequestedByCompany (optional)
        e.put(1000000018, new Value("Yort")); // RequestedByLastName (optional)
        e.put(1000000019, new Value("Ivan")); // RequestedByFirstName (optional)
        e.put(1000000056, new Value("55 11 9933-0369")); // RequestedByPhoneNumber (optional)
        e.put(1000000063, new Value("TI Infrastructure")); // Category1 (required)
        e.put(1000000074, new Value("STE000000003401")); // RequestedBySiteID (optional)
        e.put(1000000080, new Value("PPL000000006618")); // RequestedByPersonID (optional)
        e.put(1000000337, new Value(ar.getUser())); // RequestedByLoginID (optional)
        e.put(1000001021, new Value("PPL000000006618")); // RequestedByPersonID1 (optional)
        e.put(1000003143, new Value("SYSTEM PHASE")); // ApprovalPhaseName (optional)
        //e.put(1000005708, new Value("AG001143D417CBv8qvRQ#GBbEQjRkJ")); // ImageInstanceId2 (optional)
        e.put(1000005709, new Value("Nenhum")); // Approval Type (required)
        /*
        e.put(4, new Value()); // AssignedTo (optional)
        e.put(160, new Value()); // SRD_Locale (optional)
        e.put(14507, new Value()); // Generic Field (optional)
        e.put(60900, new Value()); // Vendor Assignee Groups (optional)
        e.put(60901, new Value()); // Vendor Assignee Groups_parent (optional)
        e.put(60989, new Value()); // Assignee Groups_parent (optional)
        e.put(10002506, new Value()); // ServiceRequestCoordinatorGroup (optional)
        e.put(200000006, new Value()); // RequestedByDepartment (optional)
        e.put(200000007, new Value()); // RequestedBySite Group (optional)
        e.put(200000012, new Value()); // RequestedByRegion (optional)
        e.put(240000014, new Value()); // EffectiveEndDate (optional)
        e.put(260000500, new Value()); // App Proper Name (optional)
        e.put(301071300, new Value()); // SRD_URL (optional)
        e.put(301236400, new Value()); // SRApproverName (optional)
        e.put(301249800, new Value()); // SCOApproverName (optional)
        e.put(301251900, new Value()); // ServiceOwnerFullName (optional)
        e.put(301252000, new Value()); // ServiceOwnerPhone (optional)
        e.put(301307300, new Value()); // SRD_VersionNumber (optional)
        e.put(301318900, new Value()); // RequestedByInstanceID (optional)
        e.put(301323600, new Value()); // DefaultProcessName (optional)
        e.put(301367200, new Value()); // ServiceOwnerIID (optional)
        e.put(301367400, new Value()); // ServiceOwnerOrganization (optional)
        e.put(301367500, new Value()); // ServiceOwnerDepartment (optional)
        e.put(301472100, new Value()); // ServiceOwner LoginName (optional)
        e.put(301600300, new Value()); // DataTags (optional)
        e.put(302760900, new Value()); // AssigneeGroupID (optional)
        e.put(302771500, new Value()); // SRD_Aliases (optional)
        e.put(302792800, new Value()); // SRDApproverName (optional)
        e.put(302793100, new Value()); // SRD_Level (optional)
        e.put(302793300, new Value()); // Questions Form (optional)
        e.put(302793400, new Value()); // SRD_BusinessService (optional)
        e.put(302793700, new Value()); // SRD_Account Number (optional)
        e.put(302822600, new Value()); // BusinessServiceInstanceId (optional)
        e.put(302823600, new Value()); // SRD_Instructions (optional)
        e.put(302831000, new Value()); // Category Tag2 (optional)
        e.put(302831100, new Value()); // Category Tag3 (optional)
        e.put(302831200, new Value()); // SRD_Level Tag (optional)
        e.put(302839500, new Value()); // SurveySelection (optional)
        e.put(302844800, new Value()); // SurveyEnabled (optional)
        e.put(302852200, new Value()); // NT_RejectNotification (optional)
        e.put(302852600, new Value()); // NT_ApprovedNotification (optional)
        e.put(302858900, new Value()); // CMD_SYNC_SRDwithCMDB (optional)
        e.put(302883500, new Value()); // SurveyAssocInstanceID (optional)
        e.put(302892200, new Value()); // ApprovalFlag_Customer (optional)
        e.put(302892400, new Value()); // ApprovalFlag_CatMan (optional)
        e.put(302892500, new Value()); // ApprovalFlag_Coordinator (optional)
        e.put(302892600, new Value()); // ApprovalFlag_Other (optional)
        e.put(302896700, new Value()); // UseApprovalEngFlag_Glb (optional)
        e.put(302897800, new Value()); // HotList (optional)
        e.put(302902000, new Value()); // CoordGroupID (optional)
        e.put(302902900, new Value()); // Active Approval_Cust (optional)
        e.put(302903000, new Value()); // Active Approval_CatMan (optional)
        e.put(302903100, new Value()); // Active Approval_Coordinator (optional)
        e.put(302903400, new Value()); // SurveyName (optional)
        e.put(302906200, new Value()); // SRDAttachmentField (optional)
        e.put(302974200, new Value()); // Category2_Instance ID (optional)
        e.put(302974400, new Value()); // Category3_Instance ID (optional)
        e.put(303062800, new Value()); // InternalFlag (optional)
        e.put(303503300, new Value()); // AdvInterface_ForceUserOpen (optional)
        e.put(303829500, new Value()); // Approver_Person (optional)
        e.put(303829600, new Value()); // Approver_Group (optional)
        e.put(304020900, new Value()); // z1D_APRLookup_InstanceId (optional)
        e.put(304021000, new Value()); // z1D_APR_Definition_ID (optional)
        e.put(304088900, new Value()); // ReqMgrApprover (optional)
        e.put(304234600, new Value()); // Consumer (optional)
        e.put(304234700, new Value()); // PackageIDList (optional)
        e.put(304240120, new Value()); // PackageQual (optional)
        e.put(304258530, new Value()); // SRD_ChangePolicy (optional)
        e.put(304261730, new Value()); // CSM_HideNavCat (optional)
        e.put(304261740, new Value()); // CSM_HidePackages (optional)
        e.put(304261750, new Value()); // CSM_DeliveryROID (optional)
        e.put(304278110, new Value()); // SRD_AllowCancel (optional)
        e.put(304349161, new Value()); // SRD_RTF_Attachment1 (optional)
        e.put(304349171, new Value()); // SRD_RTF_Attachment2 (optional)
        e.put(304349181, new Value()); // SRD_RTF_Attachment3 (optional)
        e.put(304411511, new Value()); // SRD_Opt_SO_InstanceID (optional)
        e.put(400127400, new Value()); // BS_DatasetId (optional)
        e.put(400129200, new Value()); // BS_ReconId (optional)
        e.put(420050029, new Value()); // Abydos Launch Wizard (optional)
        e.put(420050030, new Value()); // Abydos Use Process (optional)
        e.put(420050101, new Value()); // Abydos Console Process Name (optional)
        e.put(1000000010, new Value()); // RequestedByOrganization (optional)
        e.put(1000000020, new Value()); // RequestedByMiddleInitial (optional)
        e.put(1000000048, new Value()); // RequestedByInternet E-mail (optional)
        e.put(1000000054, new Value()); // RequestedByCorporateID (optional)
        e.put(1000000064, new Value()); // Category2 (optional)
        e.put(1000000065, new Value()); // Category3 (optional)
        e.put(1000000079, new Value()); // z1D_Approver (optional)
        e.put(1000000150, new Value()); // Status_Reason (optional)
        e.put(1000003217, new Value()); // Status Reason2 (optional)
        e.put(1000003278, new Value()); // Active Approval (optional)
         */

        //String entryId = ar.createEntry("SRD:ServiceRequestDefinition", e);
        try {
            ar.createEntry("SRD:ServiceRequestDefinition", e);
        } catch (ARException ex) {
            List<StatusInfo> lastStatus = ex.getLastStatus();
            if (lastStatus.size() == 1) {
                if (lastStatus.get(0).getMessageNum() == 150123) {
                    Log.get().info("   Ja estava registrado");
                    return;
                }
            }
            throw ex;
        }
    }

    private String SRD_Number() throws ARException {
        Entry e = new Entry();
        e.put(2, new Value(ar.getUser())); // Submitter (required)
        e.put(7, new Value(5)); // Status (required)
        e.put(8, new Value("NOT USED")); // Short Description (required)        
        return ar.createEntry("SRD:CFG DefinitionNumGenerator", e);
    }

}
