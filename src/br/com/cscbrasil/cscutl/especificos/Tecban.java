/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.especificos;

import br.com.cscbrasil.bmc.ARServerUser;
import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Entry;
import com.bmc.arsys.api.QualifierInfo;
import com.bmc.arsys.api.SortInfo;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ivan
 */
public class Tecban {

    static public void carga_slm_assignment() throws ARException {
        RemedyConnection rmd = RemedyConnections.get();

        ARServerUser ar = rmd.sourc;
        QualifierInfo qualification = ar.parseQualification("HPD:Help Desk Assignment Log", "1=1");

        int maxRecords = 1000;
        if (ar.cscGetMaxRecords() < maxRecords) {
            maxRecords = ar.cscGetMaxRecords();
        }
        ArrayList<SortInfo> sortList = new ArrayList<>();
        sortList.add(new SortInfo(1, Constants.AR_SORT_ASCENDING));
        int[] fieldIds = {1000000161, 1000000079, 3, 2};
        int firstRetrieve = Constants.AR_START_WITH_FIRST_ENTRY;
        int contar = 0;
        while (true) {
            List<Entry> entries = ar.getListEntryObjects("HPD:Help Desk Assignment Log", qualification, firstRetrieve, maxRecords, sortList, fieldIds, false, null);
            if (entries.isEmpty()) {
                break;
            }
            for (Entry entry : entries) {
                Log.get().info(entry.getEntryId());
                QualifierInfo qHelpDesk = ar.parseQualification("HPD:Help Desk", String.format("'1000000161'=\"%s\"", entry.get(1000000161)));
                int[] fHelpDesk = {179};
                Entry eHelpDesk = ar.getOneEntryObject("HPD:Help Desk", qHelpDesk, null, fHelpDesk, false, null);
                if (!eHelpDesk.isEmpty()) {
                    Entry e = new Entry();
                    e.put(2, entry.get(2));
                    e.put(7, new Value(0));
                    e.put(536870913, entry.get(3));
                    e.put(8, new Value("HPD:Help Desk"));
                    e.put(179, eHelpDesk.get(179));
                    e.put(1000000079, entry.get(1000000079));
                    e.put(536870914, new Value(entry.getEntryId()));
                    QualifierInfo qAssignment = ar.parseQualification("TEC:SLM:AssignmentLog", String.format("'8'=\"%s\" AND '179'=\"%s\" AND '1000000079'=\"%s\"", "HPD:Help Desk", e.get(179), e.get(1000000079)));
                    String mergeEntry = ar.mergeEntry("TEC:SLM:AssignmentLog", e, Constants.AR_MERGE_ENTRY_DUP_OVERWRITE, qAssignment, Constants.AR_MERGE_MULTIOPTIONS_CHANGE_FIRST);
                }
            }
            contar += entries.size();
            firstRetrieve += maxRecords;
        }
        Log.get().info(String.format("%d", contar));
    }
}
