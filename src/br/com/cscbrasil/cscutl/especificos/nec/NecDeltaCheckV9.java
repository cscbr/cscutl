/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.especificos.nec;

import br.com.cscbrasil.bmc.RemedyConnection;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.SQLResult;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.RecursiveTask;

/**
 *
 * @author ivan
 */
public class NecDeltaCheckV9 extends RecursiveTask {

    List<String> ids;
    RemedyConnection rmd;
    int schemaIdDestino;
    int schemaIdSource;
    String formName;
    List<List<Value>> sourceContents;
    String formType;
    int i;
    List<String> migrar;

    public NecDeltaCheckV9(List<String> ids, RemedyConnection rmd, int schemaIdSource, int schemaIdDestino, String formName, List<List<Value>> sourceContents, int i, String formType) {
        this.ids = new ArrayList(ids);
        this.rmd = rmd;
        this.schemaIdDestino = schemaIdDestino;
        this.schemaIdSource = schemaIdSource;
        this.formName = formName;
        this.sourceContents = sourceContents;
        this.i = i;
        this.formType = formType;
        this.migrar = new ArrayList<>();
    }

    @Override
    protected Object compute() {
        String inTxt = String.join(",", ids);
        String cmdDestino = String.format("select c1,c6 from T%d where c1 in (%s) order by c1", schemaIdDestino, inTxt);
        SQLResult listSqlDestino;
        try {
            //Log.get().info("      v9:" + cmdDestino);
            listSqlDestino = rmd.desti.getListSQL(cmdDestino, 999999999, false);
        } catch (ARException ex) {
            Log.get().error(ex);
            return null;
        }
        List<List<Value>> destiContents = listSqlDestino.getContents();

        Map<RecursiveTask, Integer> a003s = new ConcurrentHashMap<>();
        // Criacao
        if (destiContents.size() != ids.size()) {
            // pesquisou 1000 ids na v8 mas nao achou os 1000 na v9
            int v8c = 0;
            int v9c = 0;
            while (v8c < ids.size()) {
                String c1v8 = ids.get(v8c).replaceAll("'", "");
                String c1v9 = "~";
                if (v9c < destiContents.size()) {
                    c1v9 = destiContents.get(v9c).get(0).toString();
                }
                if (c1v8.equals(c1v9)) {
                    v8c++;
                    v9c++;
                    continue;
                }
                migrar.add(c1v8);
                /*
                if (multi) {
                    RecursiveTask action = new NecDeltaMigra(rmd, formName, c1v8, schemaIdSource, schemaIdDestino, formType);
                    a003s.put(action, 0);
                    threadPool003.execute(action);
                } else {
                    NecDeltaMigra deltaMigra = new NecDeltaMigra(rmd, formName, c1v8, schemaIdSource, schemaIdDestino, formType);
                    deltaMigra.compute();
                }
                 */
                v8c++;
            }
        }

        // Alteracao
        int j = 0;
        for (int k = 0; k < destiContents.size(); k++) {
            List<Value> recordCompareDesti = destiContents.get(k);
            String c1v9 = recordCompareDesti.get(0).toString();
            int c6v9 = recordCompareDesti.get(1).getIntValue();
            for (; j <= i; j++) {
                List<Value> recordCompareFonte = sourceContents.get(j);
                String c1v8 = recordCompareFonte.get(0).toString();
                int c6v8 = recordCompareFonte.get(1).getIntValue();
                if (c1v8.compareTo(c1v9) < 0) {
                    continue;
                }
                if (c1v8.compareTo(c1v9) == 0) {
                    if (c6v8 > c6v9) {
                        migrar.add(c1v8);
                        /*
                        if (multi) {
                            RecursiveTask action = new NecDeltaMigra(rmd, formName, c1v8, schemaIdSource, schemaIdDestino, formType);
                            a003s.put(action, 0);
                            threadPool003.execute(action);
                        } else {
                            NecDeltaMigra deltaMigra = new NecDeltaMigra(rmd, formName, c1v8, schemaIdSource, schemaIdDestino, formType);
                            deltaMigra.compute();
                        }
                         */
                    }
                    j++;
                    break;
                }
            }
        }
        /*
        if (multi) {
            Iterator<RecursiveTask> it = a003s.keySet().iterator();
            while (it.hasNext()) {
                RecursiveTask a003 = it.next();
                a003.join();
            }
        }
         */
        if (migrar.size() > 0) {
            // print ?
            NecDeltaMigraLote deltaMigra = new NecDeltaMigraLote(rmd, formName, schemaIdSource, schemaIdDestino, formType, migrar,null);
            deltaMigra.compute();
        }
        return null;
    }

}
