/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.especificos.nec;

import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.cscutl.common.Util;
import static br.com.cscbrasil.cscutl.especificos.nec.NecDelta.dbTables;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Entry;
import com.bmc.arsys.api.Value;
import java.util.List;
import java.util.Map;
import static br.com.cscbrasil.cscutl.especificos.nec.NecDelta.fieldIdLists;
import static br.com.cscbrasil.cscutl.especificos.nec.NecDelta.fieldListsAnexos;
import static br.com.cscbrasil.cscutl.especificos.nec.NecDelta.forms;
import static br.com.cscbrasil.cscutl.especificos.nec.NecRetry.corrige306;
import com.bmc.arsys.api.ARErrors;
import com.bmc.arsys.api.ArithmeticOrRelationalOperand;
import com.bmc.arsys.api.AttachmentValue;
import com.bmc.arsys.api.QualifierInfo;
import com.bmc.arsys.api.RelationalOperationInfo;
import com.bmc.arsys.api.SortInfo;
import com.bmc.arsys.api.StatusInfo;
import com.bmc.thirdparty.org.apache.commons.lang.StringEscapeUtils;
import com.marzapower.loggable.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.RecursiveTask;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author ivan
 */
public class NecDeltaMigraLote extends RecursiveTask {

    RemedyConnection rmd;
    String formName;
    int schemaIdSource;
    int schemaIdDestino;
    String formType;
    List<String> migrar;
    NecDB necDB;

    public NecDeltaMigraLote(RemedyConnection rmd, String formName, int schemaIdSource, int schemaIdDestino, String formType, List<String> migrar, NecDB necDB) {
        this.rmd = rmd;
        this.formName = formName;
        this.schemaIdSource = schemaIdSource;
        this.schemaIdDestino = schemaIdDestino;
        this.formType = formType;
        this.migrar = migrar;
        this.necDB = necDB;
    }

    @Override
    protected Object compute() {
        List<Integer> fieldList = fieldIdLists.get(formName);
        List<Integer> fieldListAnexos = fieldListsAnexos.get(formName);

        //String qString = "";
        //int c = 0;
        String qString1 = "";
        for (String entryId : migrar) {
            qString1 += StringUtils.rightPad(entryId.trim(), 15);
        }

        Entry entry = null;
        int[] fieldArray = fieldList.stream().mapToInt(i -> i).toArray();

        String filters = forms.get(formName).get("filters");
        String method = forms.get(formName).get("method");

        try {
            if (entry != null) {
                entry.clear();
            }

            QualifierInfo qualification = gera(qString1);
            ArrayList<SortInfo> sortList = new ArrayList<>();
            sortList.add(new SortInfo(1, Constants.AR_SORT_ASCENDING));

            List<Entry> listEntryObjects = rmd.sourc.getListEntryObjects(formName, qualification, Constants.AR_START_WITH_FIRST_ENTRY, Constants.AR_NO_MAX_LIST_RETRIEVE, sortList, fieldArray, false, null);

            for (Entry request : listEntryObjects) {
                entry = request;
                if (entry.size() > fieldList.size()) {
                    for (Iterator<Map.Entry<Integer, Value>> it = entry.entrySet().iterator(); it.hasNext();) {
                        Map.Entry<Integer, Value> e = it.next();
                        if (!fieldList.contains(e.getKey())) {
                            it.remove();
                        }
                    }
                }
                for (Integer fieldIdAnexo : fieldListAnexos) {
                    AttachmentValue anexo = (AttachmentValue) entry.get(fieldIdAnexo).getValue();
                    if (anexo != null) {
                        try {
                            anexo.setValue(rmd.sourc.getEntryBlob(formName, entry.getEntryId(), fieldIdAnexo));
                            entry.put(fieldIdAnexo, new Value(anexo));
                        } catch (ARException ex) {
                            Log.get().error(ex);
                        }
                    }
                }

                //String newEntry = entry.getEntryId().trim();
                //entry.setEntryId(newEntry);
                if (formName.equals("SRM:Request")) {
                    String obo = entry.get(60600).toString();
                    while (obo.contains("''")) {
                        obo = obo.replaceAll("''", "'");
                    }
                    while (obo.contains(";;")) {
                        obo = obo.replaceAll(";;", ";");
                    }
                    while (obo.contains(";';")) {
                        obo = obo.replaceAll(";';", ";");
                    }
                    entry.put(60600, new Value(obo));
                    //'P560180';'';'CB471364';
                }

                if (method.equals("create")) {
                    Util.deleteViaDBByEntryID(rmd.desti, formName, entry.getEntryId(), dbTables.get(formName));
                }

                int mergeType = Constants.AR_MERGE_ENTRY_DUP_MERGE | Constants.AR_MERGE_NO_REQUIRED_INCREMENT | Constants.AR_MERGE_NO_PATTERNS_INCREMENT | Constants.AR_MERGE_NO_WORKFLOW_FIRED | Constants.AR_MERGE_NO_ASSOCIATION_FIRED;
                if (!filters.equals("no")) {
                    mergeType = Constants.AR_MERGE_ENTRY_DUP_MERGE | Constants.AR_MERGE_NO_REQUIRED_INCREMENT | Constants.AR_MERGE_NO_PATTERNS_INCREMENT | Constants.AR_MERGE_NO_ASSOCIATION_FIRED;
                }

                int retry = 0;
                while (true) {
                    if (!filtroRequestId(formName, entry)) {
                        try {
                            rmd.desti.mergeEntry(formName, entry, mergeType);
                            if (necDB != null) {
                                necDB.removeErro(formName, entry.getEntryId());
                            }
                        } catch (ARException ex) {
                            if (trataErro(formName, ex, entry, retry)) {
                                retry++;
                                continue;
                            }
                        }
                    }
                    break;
                }
            }
        } catch (ARException ex) {
            trataErro(formName, ex, entry, 3);
        }
        return null;
    }

    private QualifierInfo qEntryId(String entryId) {
        ArithmeticOrRelationalOperand operandLeft = new ArithmeticOrRelationalOperand(1, 1);
        ArithmeticOrRelationalOperand operandRight = new ArithmeticOrRelationalOperand(new Value(entryId));
        RelationalOperationInfo left = new RelationalOperationInfo(1, operandLeft, operandRight);
        QualifierInfo retorno = new QualifierInfo(left);
        return retorno;
    }

    private QualifierInfo qOr(QualifierInfo q1, QualifierInfo q2) {
        QualifierInfo q = new QualifierInfo(Constants.AR_COND_OP_OR, q1, q2);
        return q;
    }

    private QualifierInfo gera(String teste) {
        if (teste.length() == 15) {
            return qEntryId(teste.substring(0, 15).trim());
        }
        if (teste.length() == 30) {
            QualifierInfo id1 = qEntryId(teste.substring(0, 15).trim());
            QualifierInfo id2 = qEntryId(teste.substring(15, 30).trim());
            return qOr(id1, id2);
        }
        QualifierInfo id1 = qEntryId(teste.substring(0, 15).trim());
        QualifierInfo id2 = qEntryId(teste.substring(15, 30).trim());
        teste = teste.substring(30);
        return qOr(qOr(id1, id2), gera(teste));
    }

    private void armazenaErro(String formName, Entry entry, StatusInfo s) throws InterruptedException {
        try {
            NecDB necDB = new NecDB();
            necDB.armazenaErro(formName, entry, s);
        } catch (ClassNotFoundException ex) {
            Log.get().error(ex);
        }
    }

    private boolean trataErro(String formName, ARException ex, Entry entry, int retry) {

        List<StatusInfo> lastStatus = ex.getLastStatus();
        if (formType.equals("Audit")) {
            if (lastStatus.size() == 1) {
                if (lastStatus.get(0).getMessageNum() == ARErrors.AR_ERROR_CANNOT_MODIFY_SHADOW_FORM_DATA) {
                    // ignora
                    return false;
                }
            }
        }
        for (StatusInfo s : lastStatus) {
            String msgType;
            switch (s.getMessageType()) {
                case Constants.AR_RETURN_OK:
                    msgType = "OK";
                    break;
                case Constants.AR_RETURN_WARNING:
                    msgType = "WARNING";
                    break;
                case Constants.AR_RETURN_ERROR:
                    msgType = "ERROR";
                    break;
                case Constants.AR_RETURN_FATAL:
                    msgType = "FATAL";
                    break;
                case Constants.AR_RETURN_BAD_STATUS:
                    msgType = "BAD_STATUS";
                    break;
                case Constants.AR_RETURN_PROMPT:
                    msgType = "PROMPT";
                    break;
                case Constants.AR_RETURN_ACCESSIBLE:
                    msgType = "ACCESSIBLE";
                    break;
                case Constants.AR_RETURN_TOOLTIP:
                    msgType = "TOOLTIP";
                    break;
                default:
                    msgType = "UNKNOWN";
            }
            if (entry == null) {
                entry = new Entry();
                entry.setEntryId("000000000000000");
            }
            if (retry <= 1) {
                if (s.getMessageNum() == 306) {
                    entry = corrige306(rmd, formName, s.getAppendedText(), entry);
                    return true;
                }
            }

            try {
                armazenaErro(formName, entry, s);
            } catch (InterruptedException ex1) {
                Log.get().error(ex1);
            }
            if (s.getMessageNum() != 382) {
                String error = String.format("%s (%d): %s; %s", msgType, s.getMessageNum(), s.getMessageText(), s.getAppendedText());
                Log.get().error(String.format(",%s,%s,%s,%s,%d,%s,%s", StringEscapeUtils.escapeCsv(formName.replaceAll("\n", "").replaceAll("\r", "")), StringEscapeUtils.escapeCsv(entry.getEntryId().replaceAll("\n", "").replaceAll("\r", "")), StringEscapeUtils.escapeCsv(error.replaceAll("\n", "").replaceAll("\r", "")), StringEscapeUtils.escapeCsv(msgType.replaceAll("\n", "").replaceAll("\r", "")), s.getMessageNum(), StringEscapeUtils.escapeCsv(s.getMessageText().replaceAll("\n", "").replaceAll("\r", "")), StringEscapeUtils.escapeCsv(s.getAppendedText().replaceAll("\n", "").replaceAll("\r", ""))));
            }
        }
        return false;
    }

    private boolean filtroRequestId(String formName, Entry entry) {
        if (formName.equals("CTM:People")) {
            if (entry.getEntryId().equals("PPL000000000205")) {
                return true;
            }
            if (entry.getEntryId().equals("PPL000000000206")) {
                return true;
            }
            if (entry.getEntryId().equals("PPL000000000207")) {
                return true;
            }
            if (entry.getEntryId().equals("PPL000000000208")) {
                return true;
            }
            if (entry.getEntryId().equals("PPL000000000209")) {
                return true;
            }
            if (entry.getEntryId().equals("PPL000000000210")) {
                return true;
            }
            if (entry.getEntryId().equals("PPL000000000305")) {
                return true;
            }
            if (entry.getEntryId().equals("PPL000000000306")) {
                return true;
            }
            if (entry.getEntryId().equals("PPL000000000307")) {
                return true;
            }
            if (entry.getEntryId().equals("PPL000000000308")) {
                return true;
            }
            if (entry.getEntryId().equals("PPL000000000309")) {
                return true;
            }
            if (entry.getEntryId().equals("PPL000000000310")) {
                return true;
            }
            if (entry.getEntryId().equals("PPL000000000405")) {
                return true;
            }
            if (entry.getEntryId().equals("PPL000000000406")) {
                return true;
            }
        }
        if (formName.equals("CTM:People Permission Groups")) {
            if (entry.getOrDefault(1000000080, new Value("~")).toString().equals("PPL000000000205")) {
                return true;
            }
            if (entry.getOrDefault(1000000080, new Value("~")).toString().equals("PPL000000000206")) {
                return true;
            }
            if (entry.getOrDefault(1000000080, new Value("~")).toString().equals("PPL000000000207")) {
                return true;
            }
            if (entry.getOrDefault(1000000080, new Value("~")).toString().equals("PPL000000000208")) {
                return true;
            }
            if (entry.getOrDefault(1000000080, new Value("~")).toString().equals("PPL000000000209")) {
                return true;
            }
            if (entry.getOrDefault(1000000080, new Value("~")).toString().equals("PPL000000000210")) {
                return true;
            }
            if (entry.getOrDefault(1000000080, new Value("~")).toString().equals("PPL000000000305")) {
                return true;
            }
            if (entry.getOrDefault(1000000080, new Value("~")).toString().equals("PPL000000000306")) {
                return true;
            }
            if (entry.getOrDefault(1000000080, new Value("~")).toString().equals("PPL000000000307")) {
                return true;
            }
            if (entry.getOrDefault(1000000080, new Value("~")).toString().equals("PPL000000000308")) {
                return true;
            }
            if (entry.getOrDefault(1000000080, new Value("~")).toString().equals("PPL000000000309")) {
                return true;
            }
            if (entry.getOrDefault(1000000080, new Value("~")).toString().equals("PPL000000000310")) {
                return true;
            }
            if (entry.getOrDefault(1000000080, new Value("~")).toString().equals("PPL000000000405")) {
                return true;
            }
            if (entry.getOrDefault(1000000080, new Value("~")).toString().equals("PPL000000000406")) {
                return true;
            }
        }
        return false;
    }

}
