/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.especificos.nec;

import br.com.cscbrasil.bmc.ARServerUser;
import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import br.com.cscbrasil.cscutl.common.Util;
import static br.com.cscbrasil.cscutl.especificos.nec.NecDelta.str;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.util.List;

/**
 *
 * @author ivan
 */
public class NecC2 {

    RemedyConnection rmd;

    public void run() throws ARException {
        rmd = RemedyConnections.get();

        List<List<Value>> simpleSQL = Util.simpleSQL(rmd.desti, "select schemaid,name from arschema where schemaType = 1 and (overlayProp = 0 or overlayProp = 1 or overlayProp = 4) ");
        for (List<Value> record : simpleSQL) {
            int schemaId = record.get(0).getIntValue();
            List<List<Value>> simpleSQL1 = Util.simpleSQL(rmd.desti, "select top 1 c1 from t%s where c2='ivan@cscbrasil.com.br'", str(schemaId));
            if (simpleSQL1.size() > 0) {
                Log.get().info(String.format("%4d - %s", record.get(0).getIntValue(), record.get(1).toString()));
            }
        }
    }

    public void zauC2(String formName) throws ARException {
        rmd = RemedyConnections.get();
        ARServerUser ar = rmd.desti;
        List<String> dbTables = Util.getDBTables(ar, formName);
        int formNumber = Util.getFormNumber(ar, formName);
        while (true) {
            List<List<Value>> records = Util.simpleSQL(ar, "select top 5000 c1,c8 from t%s where c2='ivan@cscbrasil.com.br' order by c1", str(formNumber));
            if (records.size() == 0) {
                break;
            }
            for (List<Value> record : records) {
                String entryId = record.get(0).toString();
                String shortD = record.get(1).toString();
                Log.get().info(String.format("%15s - %s", entryId, shortD));
                Util.deleteViaDBByEntryID(ar, formName, entryId, dbTables);
            }
            int a = 0;
        }

    }

}
