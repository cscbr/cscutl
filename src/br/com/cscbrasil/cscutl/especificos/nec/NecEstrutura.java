/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.especificos.nec;

import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import static br.com.cscbrasil.cscutl.common.Util.getDBTables;
import static br.com.cscbrasil.cscutl.common.Util.getformType;
import static br.com.cscbrasil.cscutl.especificos.nec.NecDelta.dbTables;
import static br.com.cscbrasil.cscutl.especificos.nec.NecDelta.fieldIdLists;
import static br.com.cscbrasil.cscutl.especificos.nec.NecDelta.fieldListsAnexos;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Field;
import com.bmc.arsys.api.Form;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ivan
 */
public class NecEstrutura {

    private List<String> formNames;
    private Map<String, String> formTypes;
    RemedyConnection rmd;

    public NecEstrutura(Map<String, Map<String, String>> forms, String formEspecifico) {
        fieldIdLists = new HashMap<>();
        fieldListsAnexos = new HashMap<>();
        rmd = RemedyConnections.get();
        Log.get().info("Lendo estrutura dos formularios...");
        formNames = new ArrayList<>();
        formTypes = new HashMap<>();
        List<List<Value>> formNameContents = new ArrayList<>();
        dbTables = new HashMap<>();
        for (Map.Entry<String, Map<String, String>> entry : forms.entrySet()) {
            Map<String, String> l = entry.getValue();
            String name = entry.getKey();
            String status = l.get("status");
            if (status.equals("ativo") && formEspecifico == null) {
                List<Value> v = new ArrayList<>();
                v.add(new Value(name));
                formNameContents.add(v);
                dbTables.put(name, getDBTables(rmd.desti, name));
                continue;
            }
            if (formEspecifico != null && name.equals(formEspecifico)) {
                List<Value> v = new ArrayList<>();
                v.add(new Value(name));
                formNameContents.add(v);
                dbTables.put(name, getDBTables(rmd.desti, name));
            }
        }

        for (List<Value> record : formNameContents) {
            String formName = record.get(0).toString();
            // Formulario existe em ambos servidores ?
            String formType;
            try {
                Form form = rmd.sourc.getForm(formName);
                formType = getformType(form);
            } catch (ARException ex) {
                Log.get().error(ex);
                continue;
            }
            try {
                Form form = rmd.desti.getForm(formName);
                formType = getformType(form);
            } catch (ARException ex) {
                Log.get().error(ex);
                continue;
            }
            formNames.add(formName);
            formTypes.put(formName, formType);
            Log.get().info(formName);
            // Campos em comum entre os formularios
            try {
                List<Field> sourcFields = rmd.sourc.getListFieldObjects(formName, Constants.AR_FIELD_TYPE_DATA, 0);
                List<Field> destiFields = rmd.desti.getListFieldObjects(formName, Constants.AR_FIELD_TYPE_DATA, 0);

                List<Integer> sourceFieldIdList = new ArrayList<>();
                List<Integer> destinFieldIdList = new ArrayList<>();
                List<Integer> sourceFieldIdAttachList = new ArrayList<>();
                List<Integer> destinFieldIdAttachList = new ArrayList<>();
                for (Field f : sourcFields) {
                    if (f.getFieldOption() == Constants.AR_FIELD_OPTION_DISPLAY) {
                        continue;
                    }
                    if (f.getFieldID() == 15) {
                        continue;
                    }
                    sourceFieldIdList.add(f.getFieldID());
                    if (f.getDataType() == Constants.AR_DATA_TYPE_ATTACH) {
                        sourceFieldIdAttachList.add(f.getFieldID());
                    }
                }
                for (Field f : destiFields) {
                    if (f.getFieldOption() == Constants.AR_FIELD_OPTION_DISPLAY) {
                        continue;
                    }
                    if (f.getFieldID() == 15) {
                        continue;
                    }
                    destinFieldIdList.add(f.getFieldID());
                    if (f.getDataType() == Constants.AR_DATA_TYPE_ATTACH) {
                        destinFieldIdAttachList.add(f.getFieldID());
                    }
                }
                sourceFieldIdList.retainAll(destinFieldIdList);
                sourceFieldIdAttachList.retainAll(destinFieldIdAttachList);

                fieldIdLists.put(formName, new ArrayList<>(sourceFieldIdList));
                fieldListsAnexos.put(formName, new ArrayList<>(sourceFieldIdAttachList));
            } catch (ARException ex) {
                Log.get().error(ex);
            }
        }

    }

    public List<String> getFormNames() {
        return formNames;
    }

    public Map<String, String> getFormTypes() {
        return formTypes;
    }

}
