/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.especificos.nec;

import br.com.cscbrasil.bmc.RemedyConnection;
import static br.com.cscbrasil.cscutl.common.Util.epoch2String;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.SQLResult;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.RecursiveTask;

/**
 *
 * @author ivan
 */
public class NecDeltaQueryV8 extends RecursiveTask {

    final int c6Start;
    final int c6Final;
    final int schemaIdSource;
    final int schemaIdDestino;
    final RemedyConnection rmd;
    final String formName;
    final String formType;

    public NecDeltaQueryV8(RemedyConnection rmd, String formName, int c6Start, int c6Final, int schemaIdSource, int schemaIdDestino, String formType) {
        this.c6Start = c6Start;
        this.c6Final = c6Final;
        this.schemaIdSource = schemaIdSource;
        this.schemaIdDestino = schemaIdDestino;
        this.rmd = rmd;
        //this.rmd = RemedyConnections.get();
        this.formName = formName;
        this.formType = formType;
    }

    @Override
    protected Object compute() {
        String cmdSource = String.format("select c1,c6 from T%d where c6>=%d and c6<=%d order by c1", schemaIdSource, c6Start, c6Final - 1);

        SQLResult listSqlSource;
        try {
            listSqlSource = rmd.sourc.getListSQL(cmdSource, 999999999, false);
        } catch (ARException ex) {
            Log.get().error(ex);
            return null;
        }
        List<String> ids = new ArrayList<>();
        List<List<Value>> sourceContents = listSqlSource.getContents();
        Log.get().info(formName + " a partir de " + epoch2String(c6Start) + " ate " + epoch2String(c6Final - 1) + " " + sourceContents.size());
        Map<RecursiveTask, Integer> a002s = new ConcurrentHashMap<>();
        for (int i = 0; i < sourceContents.size(); i++) {
            List<Value> record = sourceContents.get(i);
            ids.add("'" + record.get(0).toString() + "'");
            if (ids.size() == 1100 || (i + 1) >= sourceContents.size()) {
                //if (multi) {
                //    RecursiveTask action = new NecDeltaCheckV9(ids, rmd, schemaIdSource, schemaIdDestino, formName, sourceContents, i, formType);
                //    a002s.put(action, 0);
                //    threadPool002.execute(action);
                //} else {
                    NecDeltaCheckV9 action = new NecDeltaCheckV9(ids, rmd, schemaIdSource, schemaIdDestino, formName, sourceContents, i, formType);
                    action.compute();
                //}
                ids.clear();
            }
        }
        //if (multi) {
        //    Iterator<RecursiveTask> it = a002s.keySet().iterator();
        //   while (it.hasNext()) {
        //        RecursiveTask a002 = it.next();
        //        a002.join();
        //    }
        //}
        return null;
    }

}
