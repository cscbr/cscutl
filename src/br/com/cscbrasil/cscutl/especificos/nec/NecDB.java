package br.com.cscbrasil.cscutl.especificos.nec;

import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import br.com.cscbrasil.cscutl.especificos.Nec;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Entry;
import com.bmc.arsys.api.StatusInfo;
import com.marzapower.loggable.Log;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.DateTime;
import org.postgresql.jdbc.PgConnection;
import org.postgresql.util.PSQLException;

public class NecDB {

    // variables
    PgConnection connection = null;
    Statement statement = null;
    PreparedStatement pStatement = null;
    ResultSet resultSet = null;
    //String dbURL = null;

    public NecDB() throws ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
    }

    public PgConnection getPgConnection() {
        while (true) {
            close();
            try {
                PgConnection newPgConnection;
                newPgConnection = (PgConnection) DriverManager.getConnection("jdbc:postgresql://minerva.postgresql.dbaas.com.br:5432/minerva?cancelSignalTimeout=30000&tcpKeepAlive=true", "minerva", "Qf4bt86!@#");
                newPgConnection.setAutoCommit(true);
                newPgConnection.setReadOnly(false);
                return newPgConnection;
            } catch (PSQLException ex) {
                Log.get().error(ex.getMessage());
            } catch (SQLException ex) {
                Log.get().error(ex.getMessage());
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
            }
        }

    }

    public Map<String, Map<String, String>> readDB() {
        while (true) {
            Map<String, Map<String, String>> r = new LinkedHashMap<>();
            try {
                connection = getPgConnection();
                statement = connection.createStatement();
                if (Nec.executor == null) {
                    resultSet = statement.executeQuery("select name,status,filters,method from forms order by name");
                } else {
                    resultSet = statement.executeQuery("select name,status,filters,method from forms where executor='" + Nec.executor + "'order by name");
                }
                while (resultSet.next()) {
                    Map<String, String> i = new LinkedHashMap<>();
                    i.put("status", resultSet.getString("status"));
                    i.put("filters", resultSet.getString("filters"));
                    i.put("method", resultSet.getString("method"));
                    r.put(resultSet.getString("name"), i);
                }
                close();
            } catch (SQLException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex1) {
                }
                continue;
            }
            return r;
        }
    }

    public void close() {
        try {
            if (null != resultSet) {
                resultSet.close();
            }
        } catch (SQLException sqlex) {
            Log.get().error(sqlex.getMessage());
        }
        try {
            if (null != pStatement) {
                pStatement.close();
            }
        } catch (SQLException sqlex) {
            Log.get().error(sqlex.getMessage());
        }
        try {
            if (null != statement) {
                statement.close();
            }
        } catch (SQLException sqlex) {
            Log.get().error(sqlex.getMessage());
        }
        try {
            if (null != connection) {
                connection.close();
            }
        } catch (SQLException sqlex) {
            Log.get().error(sqlex.getMessage());
        }
        resultSet = null;
        statement = null;
        pStatement = null;
        connection = null;
    }

    public void atualiza_c6(String formName, int c6Final) {
        while (true) {
            try {
                connection = getPgConnection();
                String SQL = "update forms set last_c6=? where name=?";
                pStatement = connection.prepareStatement(SQL);
                pStatement.setLong(1, c6Final);
                pStatement.setString(2, formName);
                pStatement.executeUpdate();
            } catch (SQLException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex1) {
                }
                continue;
            }
            break;
        }
        close();
    }

    public int get_c6(String formName) {
        int retorno = 0;
        while (true) {
            try {
                connection = getPgConnection();
                String SQL = "select last_c6 from forms where name=?";
                pStatement = connection.prepareStatement(SQL);
                pStatement.setString(1, formName);
                resultSet = pStatement.executeQuery();
                while (resultSet.next()) {
                    retorno = resultSet.getInt("last_c6");
                }
            } catch (SQLException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex1) {
                }
                continue;
            }
            break;
        }
        close();
        return retorno;
    }

    void armazenaErro(String formName, Entry entry, StatusInfo s) {
        while (true) {
            try {
                connection = getPgConnection();
                String SQL = "INSERT INTO errors (name,entryId,messageNum,messageType,messageText,appendedText) VALUES (?,?,?,?,?,?)";
                pStatement = connection.prepareStatement(SQL);
                pStatement.setString(1, formName);
                pStatement.setString(2, entry.getEntryId());
                pStatement.setLong(3, s.getMessageNum());
                pStatement.setLong(4, s.getMessageType());
                pStatement.setString(5, s.getMessageText());
                pStatement.setString(6, s.getAppendedText());
                pStatement.executeUpdate();
            } catch (PSQLException ex) {
                if (!ex.getMessage().contains("duplicate key")) {
                    Log.get().error(ex);
                } else {
                    break;
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex1) {
                }
                continue;
            } catch (SQLException ex) {
                if (!ex.getMessage().contains("duplicate key")) {
                    Log.get().error(ex);
                } else {
                    break;
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex1) {
                }
                continue;
            }
            break;
        }
        close();
    }

    void removeErro(String name, String entryId) {
        while (true) {
            try {
                connection = getPgConnection();
                String SQL = "delete from errors where name=? and entryId=?";
                pStatement = connection.prepareStatement(SQL);
                pStatement.setString(1, name);
                pStatement.setString(2, entryId);
                pStatement.executeUpdate();
            } catch (SQLException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex1) {
                }
                continue;
            }
            break;
        }
        close();
    }

    public void atualiza_count(String formName, int countV8, int countV9, int nextV8, int nextV9) {
        while (true) {
            try {
                connection = getPgConnection();
                String SQL = "update forms set countv8=?, countv9=?, diff=?, atualizacao=now(), nextId8=?, nextId9=? where name=?";
                pStatement = connection.prepareStatement(SQL);
                pStatement.setInt(1, countV8);
                pStatement.setInt(2, countV9);
                pStatement.setInt(3, countV8 - countV9);
                pStatement.setLong(4, nextV8);
                pStatement.setLong(5, nextV9);
                pStatement.setString(6, formName);
                pStatement.executeUpdate();
                pStatement.close();
                connection.close();
                connection = null;
            } catch (SQLException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex1) {
                }
                continue;
            }
            break;
        }
        close();
    }

    void atualiza2_c6(String formName, int c6Final) {
        if (1 == 1) {
            return;
        }
        /*
        DateTime d = new DateTime();
        int millis = (int) (d.getMillis() / 1000);

        if (millis > (c6Final + 299)) {
            c6Final += 299;
        }
         */
        while (true) {
            try {
                connection = getPgConnection();
                String SQL = "update forms set last_c6=? where name=?";
                pStatement = connection.prepareStatement(SQL);
                pStatement.setLong(1, c6Final);
                pStatement.setString(2, formName);
                pStatement.executeUpdate();
            } catch (SQLException ex) {
                Log.get().error(ex.getMessage());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex1) {
                }
                continue;
            }
            break;
        }
        close();
    }
}
