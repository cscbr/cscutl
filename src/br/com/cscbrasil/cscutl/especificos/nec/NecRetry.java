/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.especificos.nec;

import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import static br.com.cscbrasil.cscutl.common.Util.getformType;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.CharacterField;
import com.bmc.arsys.api.CharacterFieldLimit;
import static com.bmc.arsys.api.Constants.*;
import com.bmc.arsys.api.Entry;
import com.bmc.arsys.api.OverlaidInfo;
import com.bmc.arsys.api.StatusInfo;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.postgresql.jdbc.PgConnection;

/**
 *
 * @author ivan
 */
public class NecRetry {

    RemedyConnection rmd;

    public NecRetry() {
        rmd = RemedyConnections.get();
        rmd.sourc.setTimeoutLong(1800);
        rmd.sourc.setTimeoutNormal(1800);
        rmd.sourc.setTimeoutXLong(1800);
        rmd.desti.setTimeoutLong(1800);
        rmd.desti.setTimeoutNormal(1800);
        rmd.desti.setTimeoutXLong(1800);
        rmd.desti.cscSetModeBest();
        rmd.sourc.cscSetModeBest();
    }

    public void run(String arg) throws ClassNotFoundException, SQLException, InterruptedException {

        NecDB necDB = new NecDB();
        NecDelta.forms = necDB.readDB();
        PgConnection connection = necDB.getPgConnection();
        connection.setAutoCommit(true);
        connection.setReadOnly(false);
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select distinct name from errors where messageNum!=382 order by name");
        Map<String, Map<String, String>> map = necDB.readDB();
        List<String> formFica = new ArrayList<>();
        while (resultSet.next()) {
            String name = resultSet.getString("name");
            formFica.add(name);
        }
        resultSet.close();
        statement.close();

        for (Iterator<Map.Entry<String, Map<String, String>>> it = map.entrySet().iterator(); it.hasNext();) {
            Map.Entry<String, Map<String, String>> entry = it.next();
            if (arg != null) {
                if (!entry.getKey().equals(arg)) {
                    it.remove();
                    continue;
                }
            }
            if (!formFica.contains(entry.getKey())) {
                it.remove();
            }
        }

        NecEstrutura necEstrutura = new NecEstrutura(map, arg);
        List<String> formNames = necEstrutura.getFormNames();
        Map<String, String> formTypes = necEstrutura.getFormTypes();

        String Sql;
        if (arg == null) {
            Sql = "select distinct name,appendedText from errors where messageNum=306 order by name,appendedText";
        } else {
            Sql = "select distinct name,appendedText from errors where messageNum=306 and name='" + arg + "'";
        }
        statement = connection.createStatement();
        resultSet = statement.executeQuery(Sql);
        String lastName = "~";
        while (resultSet.next()) {
            String name = resultSet.getString("name");
            String appendedText = resultSet.getString("appendedText");
            if (!formNames.contains(name)) {
                continue;
            }
            if (!lastName.equals(name)) {
                Log.get().info("Corrigindo erros 306 - " + name);
                lastName = name;
            }
            corrige306(rmd, name, appendedText);
        }
        resultSet.close();
        statement.close();

        while (true) {
            statement = connection.createStatement();
            if (arg == null) {
                resultSet = statement.executeQuery("SELECT name,entryId,messageNum,messageType,messageText,appendedText FROM errors where messageNum!=382 ORDER BY name,entryId");
            } else {
                resultSet = statement.executeQuery("SELECT name,entryId,messageNum,messageType,messageText,appendedText FROM errors where messageNum!=382 and name='" + arg + "' ORDER BY name,entryId");
            }
            List<String> migrar = new ArrayList<>();
            lastName = null;
            boolean processou = false;
            while (resultSet.next()) {
                String name = resultSet.getString("name");
                String entryId = resultSet.getString("entryId");
                if (!formNames.contains(name)) {
                    continue;
                }
                processou = true;
                //necDB.removeErro(name, entryId);
                if (lastName == null) {
                    lastName = name;
                }
                if (!name.equals(lastName)) {
                    if (migrar.size() > 0) {
                        int schemaIdSource = rmd.sourc.cscGetFormNumber(lastName);
                        int schemaIdDestino = rmd.desti.cscGetFormNumber(lastName);
                        String formType;
                        try {
                            formType = getformType(rmd.desti.getForm(lastName));
                        } catch (ARException ex) {
                            Log.get().error(ex);
                            lastName = name;
                            continue;
                        }
                        NecDeltaMigraLote deltaMigra = new NecDeltaMigraLote(rmd, lastName, schemaIdSource, schemaIdDestino, formType, migrar, necDB);
                        Log.get().info(String.format("%-50s %d registros", lastName, migrar.size()));
                        deltaMigra.compute();
                    }
                    lastName = name;
                    migrar.clear();
                }
                migrar.add(entryId);
            }
            if (migrar.size() > 0) {
                int schemaIdSource = rmd.sourc.cscGetFormNumber(lastName);
                int schemaIdDestino = rmd.desti.cscGetFormNumber(lastName);
                String formType;
                try {
                    formType = getformType(rmd.desti.getForm(lastName));
                    NecDeltaMigraLote deltaMigra = new NecDeltaMigraLote(rmd, lastName, schemaIdSource, schemaIdDestino, formType, migrar, necDB);
                    Log.get().info(String.format("%-50s %d registros", lastName, migrar.size()));
                    deltaMigra.compute();
                } catch (ARException ex) {
                    Log.get().error(ex);
                }
            }
            statement.close();
            if (!processou) {
                break;
            }
            break;
        }
        connection.close();
    }

    public static Entry corrige306(RemedyConnection rmd, String name, String appendedText, Entry entry) {
        if (appendedText.contains("Maximum length -")) {
            if (!((name.equals("BMC.CORE:BMC_BaseElement") || name.equals("AST:Attributes") || name.equals("SRD:ServiceRequestDefinition_Base")) && appendedText.contains("<8>"))) {
                corrige306(rmd, name, appendedText);
            }
            if ((name.equals("BMC.CORE:BMC_BaseElement") || name.equals("AST:Attributes") || name.equals("SRD:ServiceRequestDefinition_Base")) && appendedText.contains("<8>")) {
                String s = entry.get(8).toString();
                s = Normalizer.normalize(s, Normalizer.Form.NFD);
                //s = s.replaceAll("\\p{M}", "");                
                s = s.replaceAll("[^\\p{ASCII}]", "");
                byte[] bytes = s.getBytes(Charset.forName("UTF-8"));

                if (s.length() > 254) {
                    s = s.substring(0, 254);
                }
                entry.put(8, new Value(s));
                return entry;
            }
            if (appendedText.contains("LowRange - HighRange")) {
                String rStr = "<([\\d]+)>.+\\{([\\d])+.*?([\\d\\.])+";
                Pattern pattern = Pattern.compile(rStr);
                Matcher matcher = pattern.matcher(appendedText);
                if (matcher.find()) {
                    String fieldIdStr = matcher.group(1);
                    String fieldLowStr = matcher.group(2);
                    String fieldHighStr = matcher.group(2);
                    int fieldId = Integer.parseInt(fieldIdStr);
                    int fieldLow = Integer.parseInt(fieldLowStr);
                    int fieldHigh = Integer.parseInt(fieldHighStr);

                    int intValue = entry.get(fieldId).getIntValue();
                    if (intValue < fieldLow) {
                        intValue = fieldLow;
                    }
                    if (intValue > fieldHigh) {
                        intValue = fieldHigh;
                    }
                    entry.put(fieldId, new Value(intValue));
                }
            }
        }
        return entry;
    }

    public static void corrige306(RemedyConnection rmd, String name, String appendedText) {
        if (appendedText.contains("Maximum length -")) {
            String rStr = "<([\\d]+?)>[^\\d]+([\\d]+)";
            Pattern pattern = Pattern.compile(rStr);
            Matcher matcher = pattern.matcher(appendedText);
            if (matcher.find()) {
                String fieldIdStr = matcher.group(1);
                String fieldSizeStr = matcher.group(2);
                int fieldId = Integer.parseInt(fieldIdStr);
                int fieldSize = Integer.parseInt(fieldSizeStr);
                if (name.equals("BMC.CORE:BMC_BaseElement") && fieldId == 8) {
                    return;
                }
                if (name.equals("AST:Attributes") && fieldId == 8) {
                    return;
                }
                if (name.equals("SRD:ServiceRequestDefinition_Base") && fieldId == 8) {
                    return;
                }
                Log.get().info("   fieldId: " + fieldIdStr + " size: " + fieldSizeStr);
                try {
                    // campos tem o mesmo tamanho em ambos servidores ?
                    CharacterField fieldS = (CharacterField) rmd.sourc.getField(name, fieldId);
                    CharacterField fieldD = (CharacterField) rmd.desti.getField(name, fieldId);
                    CharacterFieldLimit fieldLimitS = (CharacterFieldLimit) fieldS.getFieldLimit();
                    CharacterFieldLimit fieldLimitD = (CharacterFieldLimit) fieldD.getFieldLimit();
                    if (fieldLimitS.getMaxLength() != fieldLimitD.getMaxLength()) {
                        Log.get().info("   tamanho invalido v8: " + fieldLimitS.getMaxLength() + "  v9: " + fieldLimitD.getMaxLength());
                        return;
                    }
                    // destino já esta usando chars ao inves de bytes ??
                    if (fieldLimitD.getLengthUnits() == com.bmc.arsys.api.Constants.AR_LENGTH_UNIT_CHAR) {
                        Log.get().info("   destino ja usa CHARACTERS e nao BYTES");
                        return;
                    }

                    OverlaidInfo formOverlay = new OverlaidInfo(name, com.bmc.arsys.api.Constants.AR_STRUCT_ITEM_SCHEMA, name, 0, "Lenght Unit = CHARS", AR_GRANULAR_PERMISSIONS | AR_GRANULAR_FORM_LIST | AR_GRANULAR_INDEX | AR_GRANULAR_OTHER | AR_GRANULAR_ARCHIVE_ASSOC_LIST, 0);
                    OverlaidInfo fieldOverlay = new OverlaidInfo(fieldD.getName(), com.bmc.arsys.api.Constants.AR_STRUCT_ITEM_FIELD, name, fieldId, "Lenght Unit = CHARS", AR_GRANULAR_PERMISSIONS, 0);
                    try {
                        rmd.desti.createOverlay(formOverlay);
                    } catch (ARException ex) {
                        List<StatusInfo> lastStatus = ex.getLastStatus();
                        boolean ignora = false;
                        if (lastStatus.size() == 1) {
                            if (lastStatus.get(0).getMessageNum() == 8899) {
                                ignora = true;
                            }
                        }
                        if (!ignora) {
                            Log.get().error(ex);
                            return;
                        }
                    }
                    try {
                        rmd.desti.createOverlay(fieldOverlay);
                    } catch (ARException ex) {
                        List<StatusInfo> lastStatus = ex.getLastStatus();
                        boolean ignora = false;
                        if (lastStatus.size() == 1) {
                            if (lastStatus.get(0).getMessageNum() == 8899) {
                                ignora = true;
                            }
                        }
                        if (!ignora) {
                            Log.get().error(ex);
                            return;
                        }
                    }
                    fieldLimitD.setLengthUnits(AR_LENGTH_UNIT_CHAR);
                    fieldD.setFieldLimit(fieldLimitD);
                    rmd.desti.setField(fieldD);
                } catch (ARException ex) {
                    Log.get().error(ex);
                }
            }
        }
    }
}
