package br.com.cscbrasil.cscutl.especificos.nec;

import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import br.com.cscbrasil.cscutl.common.Util;
import static br.com.cscbrasil.cscutl.especificos.nec.NecDelta.str;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ivan
 */
public class NecConta {

    static Map<String, Map<String, String>> forms;
    RemedyConnection rmd;
    NecDB necDB;

    public NecConta() throws ClassNotFoundException, SQLException, InterruptedException {
        necDB = new NecDB();
        forms = necDB.readDB();
    }

    public void run(String formEspecifico) throws SQLException, InterruptedException {
        rmd = RemedyConnections.get();
        rmd.sourc.setTimeoutLong(1800);
        rmd.sourc.setTimeoutNormal(1800);
        rmd.sourc.setTimeoutXLong(1800);
        rmd.desti.setTimeoutLong(1800);
        rmd.desti.setTimeoutNormal(1800);
        rmd.desti.setTimeoutXLong(1800);

        Log.get().info(String.format("%-50s %7s %7s %7s", "form", "v8", "v9", "dif"));
        if (formEspecifico == null) {
            for (Map.Entry<String, Map<String, String>> entry : forms.entrySet()) {
                Map<String, String> l = entry.getValue();
                String name = entry.getKey();
                String status = l.get("status");
                //if (status.equals("ativo")) {
                int tSource = Util.getFormNumber(rmd.sourc, name);
                int tDestin = Util.getFormNumber(rmd.desti, name);
                if (tSource > 0 && tDestin > 0) {
                    try {
                        List<List<Value>> resultSource = Util.simpleSQL(rmd.sourc, "select count(c1) from t%s", str(tSource));
                        List<List<Value>> resultDestin = Util.simpleSQL(rmd.desti, "select count(c1) from t%s", str(tDestin));
                        int countV8 = resultSource.get(0).get(0).getIntValue();
                        int countV9 = resultDestin.get(0).get(0).getIntValue();
                        List<List<Value>> resultSource1 = Util.simpleSQL(rmd.sourc, "select nextid from arschema where schemaid=%s", str(tSource));
                        List<List<Value>> resultDestin1 = Util.simpleSQL(rmd.desti, "select nextid from arschema where schemaid=%s", str(tDestin));
                        int nextV8 = resultSource1.get(0).get(0).getIntValue();
                        int nextV9 = resultDestin1.get(0).get(0).getIntValue();
                        necDB.atualiza_count(name, countV8, countV9,nextV8,nextV9);
                        Log.get().info(String.format("%-50s %7d %7d %7d", name, countV8, countV9, countV8 - countV9));
                    } catch (ARException ex) {
                        Log.get().error(ex);
                    }
                }
                //}
            }
        } else {
            int tSource = Util.getFormNumber(rmd.sourc, formEspecifico);
            int tDestin = Util.getFormNumber(rmd.desti, formEspecifico);
            try {
                List<List<Value>> resultSource = Util.simpleSQL(rmd.sourc, "select count(c1) from t%s", str(tSource));
                List<List<Value>> resultDestin = Util.simpleSQL(rmd.desti, "select count(c1) from t%s", str(tDestin));
                int countV8 = resultSource.get(0).get(0).getIntValue();
                int countV9 = resultDestin.get(0).get(0).getIntValue();
                Log.get().info(String.format("%-50s %7d %7d %7d", formEspecifico, countV8, countV9, countV8 - countV9));
            } catch (ARException ex) {
                Log.get().error(ex);
            }
        }
    }
}
