/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.especificos.nec;

import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import br.com.cscbrasil.cscutl.common.Util;
import static br.com.cscbrasil.cscutl.common.Util.simpleSQL;
import static br.com.cscbrasil.cscutl.especificos.nec.NecDelta.fieldIdLists;
import static br.com.cscbrasil.cscutl.especificos.nec.NecDelta.fieldListsAnexos;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.AttachmentValue;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Entry;
import com.bmc.arsys.api.OutputInteger;
import com.bmc.arsys.api.QualifierInfo;
import com.bmc.arsys.api.SortInfo;
import com.bmc.arsys.api.StatusInfo;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.RecursiveTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author ivan
 */
public class NecDelta {

    static Map<String, Map<String, String>> forms;
    RemedyConnection rmd;
    public static Map<String, List<Integer>> fieldIdLists;
    public static Map<String, List<Integer>> fieldListsAnexos;

    static Map<String, List<String>> dbTables;
    NecDB necDB;

    public NecDelta() throws ClassNotFoundException, SQLException, InterruptedException {
        fieldIdLists = new HashMap<>();
        fieldListsAnexos = new HashMap<>();
        necDB = new NecDB();
        forms = necDB.readDB();
    }

    public void run(String formEspecifico) throws SQLException, InterruptedException {
        run2(formEspecifico);
    }

    public void run2(String formEspecifico) throws SQLException, InterruptedException {
        rmd = RemedyConnections.get();
        rmd.sourc.setTimeoutLong(1800);
        rmd.sourc.setTimeoutNormal(1800);
        rmd.sourc.setTimeoutXLong(1800);
        rmd.desti.setTimeoutLong(1800);
        rmd.desti.setTimeoutNormal(1800);
        rmd.desti.setTimeoutXLong(1800);

        NecEstrutura necEstrutura = new NecEstrutura(necDB.readDB(), formEspecifico);

        List<String> formNames = necEstrutura.getFormNames();
        Map<String, String> formTypes = necEstrutura.getFormTypes();

        long startMilis = System.currentTimeMillis();
        for (String formName : formNames) {
            try {
                formulario(formName, formTypes.getOrDefault(formName, "Regular"));
            } catch (ARException ex) {
                Log.get().error(formName, ex);
            }
        }
        long endMilis = System.currentTimeMillis();
        double lapse = ((0.0 + endMilis) - (0.0 + startMilis)) / 1000.0;
        Log.get().info(String.format("%f segundos", lapse));
    }

    private void formulario(String formName, String formType) throws ARException, NullPointerException, InterruptedException {
        if (formName.equals("Group")) {
            formulario_especial(formName, formType, "'106'=%s", 106);
        } else {
            if (formName.equals("User")) {
                formulario_especial(formName, formType, "'101'=\"%s\"", 101);
            } else {
                formulario_comum(formName, formType);
            }
        }
    }

    private void formulario_comum(String formName, String formType) throws ARException, NullPointerException, InterruptedException {
        int schemaIdSource = Util.getFormNumber(rmd.sourc, formName);
        int schemaIdDestino = Util.getFormNumber(rmd.desti, formName);
        int countV8 = 0;
        int countV9 = 0;

        if (schemaIdSource == 0) {
            Log.get().error("Formulario nao existe na v8: " + formName);
        }
        if (schemaIdDestino == 0) {
            Log.get().error("Formulario nao existe na v9: " + formName);
        }
        if (schemaIdSource == 0 || schemaIdDestino == 0) {
            return;
        }

        int last_C6;
        last_C6 = necDB.get_c6(formName);
        if (last_C6 > 301) {
            last_C6 = last_C6 - 301; // sempre retrocede 5 minutos
        } else {
            last_C6 = 0;
        }

        List<List<Value>> simpleSQL = simpleSQL(rmd.sourc, "select min(c6),max(c6) from T%s", str(schemaIdSource));

        int c6Start;
        int C6FINAL;
        int c6Final = last_C6;

        try {
            C6FINAL = simpleSQL.get(0).get(1).getIntValue() + 1;
        } catch (NullPointerException n) {
            C6FINAL = (int) (System.currentTimeMillis() / 1000 - 600);
            //Log.get().error(String.format("retorno inesperado %s: select min(c6),max(c6) from T%s", formName, str(schemaIdSource)));
            //return;
        }

        Map<RecursiveTask, Integer> a001s = new ConcurrentHashMap<>();

        int lastc6Final = 0;
        while (true) {
            c6Start = c6Final;
            c6Final = C6FINAL; //(jump + 1);
            List<List<Value>> sql2 = simpleSQL(rmd.sourc, "select max(c6) from (select top 1000 c6 from T%s where c6>=%s order by c6) a", str(schemaIdSource), str(c6Start));
            if (sql2 != null) {
                if (sql2.get(0) != null) {
                    if (sql2.get(0).get(0) != null) {
                        Value v = sql2.get(0).get(0);
                        if (v.getValue() != null) {
                            int c6FinalAlt = sql2.get(0).get(0).getIntValue() + 1;
                            //if (c6FinalAlt > c6Final) {
                            c6Final = c6FinalAlt;
                            //}
                        } else {
                            break;
                        }
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            } else {
                break;
            }

            if (c6Final > C6FINAL) {
                c6Final = C6FINAL;
            }
            br.com.cscbrasil.cscutl.especificos.nec.NecDeltaQueryV8 action = new br.com.cscbrasil.cscutl.especificos.nec.NecDeltaQueryV8(rmd, formName, c6Start, c6Final, schemaIdSource, schemaIdDestino, formType);
            action.compute();
            necDB.atualiza_c6(formName, c6Final);

            if (c6Final == C6FINAL) {
                break;
            }

            List<List<Value>> simpleSQL1 = simpleSQL(rmd.sourc, "select min(c6) from T%s where c6>=%s", str(schemaIdSource), str(c6Final));
            if (simpleSQL1 == null) {
                break;
            }
            if (simpleSQL1.get(0) == null) {
                break;
            }
            if (simpleSQL1.get(0).get(0) == null) {
                break;
            }
            c6Final = simpleSQL1.get(0).get(0).getIntValue();
            if (lastc6Final != c6Final) {
                lastc6Final = c6Final;
            } else {
                break;
            }
        }
        //if (multi) {
        //    Iterator<RecursiveTask> it = a001s.keySet().iterator();
        //    while (it.hasNext()) {
        //        RecursiveTask a001 = it.next();
        //        a001.join();
        //    }
        //}

        // contagem
        List<List<Value>> resultSource = simpleSQL(rmd.sourc, "select count(c1) from T%s", str(schemaIdSource));
        List<List<Value>> resultDestin = simpleSQL(rmd.desti, "select count(c1) from T%s", str(schemaIdDestino));
        countV8 = resultSource.get(0).get(0).getIntValue();
        countV9 = resultDestin.get(0).get(0).getIntValue();
        int dif = countV8 - countV9;
        List<List<Value>> resultSource1 = Util.simpleSQL(rmd.sourc, "select nextid from arschema where schemaid=%s", str(schemaIdSource));
        List<List<Value>> resultDestin1 = Util.simpleSQL(rmd.desti, "select nextid from arschema where schemaid=%s", str(schemaIdDestino));
        int nextV8 = resultSource1.get(0).get(0).getIntValue();
        int nextV9 = resultDestin1.get(0).get(0).getIntValue();

        necDB.atualiza_count(formName, countV8, countV9, nextV8, nextV9);
        necDB.atualiza2_c6(formName, c6Final);

    }

    public static String str(Integer n) {
        if (n == null) {
            return "null";
        }
        return String.format("%d", n);
    }

    String epoch2String(long value) {
        DateTime jodaDateTime = new DateTime((value + 60 * 60 * 3) * 1000);
        DateTimeFormatter dtfOut = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
        return dtfOut.print(jodaDateTime);
    }

    private void formulario_especial(String formName, String formType, String qualPattern, int fieldId) {
        DateTime d = new DateTime();
        int timeStamp = (int) (d.getMillis() / 1000);

        int schemaIdSource = Util.getFormNumber(rmd.sourc, formName);
        int schemaIdDestino = Util.getFormNumber(rmd.desti, formName);

        if (schemaIdSource == 0) {
            Log.get().error("Formulario nao existe na v8: " + formName);
        }
        if (schemaIdDestino == 0) {
            Log.get().error("Formulario nao existe na v9: " + formName);
        }
        if (schemaIdSource == 0 || schemaIdDestino == 0) {
            return;
        }

        QualifierInfo qualificationSource;
        try {            
            if (formName.equals("User")){
                qualificationSource = rmd.sourc.parseQualification(formName, "'1'>=\"000000000000483\"");
            } else {
                qualificationSource = rmd.sourc.parseQualification(formName, "1=1");
            }                
        } catch (ARException ex) {
            Log.get().error(ex);
            return;
        }
        List<SortInfo> sortList = new ArrayList<>();
        sortList.add(new SortInfo(Constants.AR_CORE_ENTRY_ID, Constants.AR_SORT_ASCENDING));
        int first = Constants.AR_START_WITH_FIRST_ENTRY;
        int maxRetrieve = 1000;
        List<Integer> fieldList = fieldIdLists.get(formName);
        List<Integer> fieldListAnexos = fieldListsAnexos.get(formName);
        int[] fieldArray = fieldList.stream().mapToInt(i -> i).toArray();
        while (true) {
            List<Entry> listEntryObjects;
            while (true) {
                try {
                    listEntryObjects = rmd.sourc.getListEntryObjects(formName, qualificationSource, first, maxRetrieve, sortList, fieldArray, false, null);
                    break;
                } catch (ARException ex) {
                    Log.get().error(ex);
                }
            }
            if (listEntryObjects.isEmpty()) {
                break;
            }
            // select * from tschemaIdDestino where cfieldId=

            for (Entry entry : listEntryObjects) {
                if (entry.size() > fieldList.size()) {
                    for (Iterator<Map.Entry<Integer, Value>> it = entry.entrySet().iterator(); it.hasNext();) {
                        Map.Entry<Integer, Value> e = it.next();
                        if (!fieldList.contains(e.getKey())) {
                            it.remove();
                        }
                    }
                }
                for (Integer fieldIdAnexo : fieldListAnexos) {
                    AttachmentValue anexo = (AttachmentValue) entry.get(fieldIdAnexo).getValue();
                    if (anexo != null) {
                        try {
                            anexo.setValue(rmd.sourc.getEntryBlob(formName, entry.getEntryId(), fieldIdAnexo));
                            entry.put(fieldIdAnexo, new Value(anexo));
                        } catch (ARException ex) {
                            Log.get().error(ex);
                        }
                    }
                }

                int mergeType = Constants.AR_MERGE_ENTRY_DUP_ERROR | Constants.AR_MERGE_NO_REQUIRED_INCREMENT | Constants.AR_MERGE_NO_PATTERNS_INCREMENT | Constants.AR_MERGE_NO_ASSOCIATION_FIRED | Constants.AR_MERGE_NO_WORKFLOW_FIRED;

                QualifierInfo qualificationMerge;
                String qmergeString = String.format(qualPattern, entry.get(fieldId).toString());
                try {
                    qualificationMerge = rmd.desti.parseQualification(formName, qmergeString);
                } catch (ARException ex) {
                    Log.get().error(ex);
                    continue;
                }
                Log.get().error(String.format("%15s - %s", entry.getEntryId(), qmergeString));

                entry.remove(1);
                entry.put(2, new Value(rmd.desti.getUser()));

                try {
                    rmd.desti.mergeEntry(formName, entry, mergeType, qualificationMerge, Constants.AR_MERGE_MULTIOPTIONS_DO_NOTHING);
                    int a = 0;
                } catch (ARException ex) {
                    List<StatusInfo> lastStatus = ex.getLastStatus();
                    if (lastStatus.get(0).getMessageNum() == 382 && formName.equals("User")) {
                        String cmd = String.format("delete from user_cache where username='%s'", entry.get(101).toString());
                        try {
                            Util.simpleSQL(rmd.desti, cmd);
                        } catch (ARException ex1) {
                            Log.get().error(ex1);
                        }
                    }
                    if (lastStatus.get(0).getMessageNum() != 338 && !(lastStatus.get(0).getMessageNum() == 382 && formName.equals("User"))) {
                        Log.get().error(ex);
                    }
                }
            }
            first += maxRetrieve;
        }

        try {
            List<List<Value>> resultSource = simpleSQL(rmd.sourc, "select count(c1) from T%s", str(schemaIdSource));
            List<List<Value>> resultDestin = simpleSQL(rmd.desti, "select count(c1) from T%s", str(schemaIdDestino));
            int countV8 = resultSource.get(0).get(0).getIntValue();
            int countV9 = resultDestin.get(0).get(0).getIntValue();
            int dif = countV8 - countV9;
            List<List<Value>> resultSource1 = Util.simpleSQL(rmd.sourc, "select nextid from arschema where schemaid=%s", str(schemaIdSource));
            List<List<Value>> resultDestin1 = Util.simpleSQL(rmd.desti, "select nextid from arschema where schemaid=%s", str(schemaIdDestino));
            int nextV8 = resultSource1.get(0).get(0).getIntValue();
            int nextV9 = resultDestin1.get(0).get(0).getIntValue();

            necDB.atualiza_count(formName, countV8, countV9, nextV8, nextV9);
        } catch (ARException ex) {
            Log.get().error(ex);
        }

    }

}
