/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.especificos.caixa;

import br.com.cscbrasil.bmc.ARServerUser;
import br.com.cscbrasil.bmc.RemedyConnection;
import static br.com.cscbrasil.cscutl.common.Util.simpleSQL;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Entry;
import com.bmc.arsys.api.QualifierInfo;
import com.bmc.arsys.api.Value;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.RecursiveTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import static br.com.cscbrasil.cscutl.especificos.caixa.Delta.fieldIdLists;
import static br.com.cscbrasil.cscutl.especificos.caixa.Delta.fieldListsAnexos;
import static br.com.cscbrasil.cscutl.especificos.caixa.Delta.mergeServer;
import static br.com.cscbrasil.cscutl.especificos.caixa.Delta.str;
import com.bmc.arsys.api.ARErrors;
import com.bmc.arsys.api.AttachmentValue;
import com.bmc.arsys.api.DiaryListValue;
import com.bmc.arsys.api.EnumItem;
import com.bmc.arsys.api.Field;
import com.bmc.arsys.api.FieldLimit;
import com.bmc.arsys.api.SelectionField;
import com.bmc.arsys.api.SelectionFieldLimit;
import com.bmc.arsys.api.StatusHistoryItem;
import com.bmc.arsys.api.StatusHistoryValue;
import com.bmc.arsys.api.StatusInfo;
import com.bmc.thirdparty.org.apache.commons.lang.StringEscapeUtils;
import com.marzapower.loggable.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.RecursiveTask;

/**
 *
 * @author ivan
 */
public class DeltaMigra extends RecursiveTask {

    RemedyConnection rmd;
    String formName;
    String requestID;
    int schemaIdSource;
    int schemaIdDestino;
    String formType;

    public DeltaMigra(RemedyConnection rmd, String formName, String requestID, int schemaIdSource, int schemaIdDestino, String formType) {
        this.rmd = rmd;
        this.formName = formName;
        this.requestID = requestID;
        this.schemaIdSource = schemaIdSource;
        this.schemaIdDestino = schemaIdDestino;
        this.formType = formType;
    }

    @Override
    protected Object compute() {
        List<Integer> fieldList = fieldIdLists.get(formName);
        List<Integer> fieldListAnexos = fieldListsAnexos.get(formName);

        Entry entry = null;
        int[] fieldArray = fieldList.stream().mapToInt(i -> i).toArray();
        try {
            if (entry != null) {
                entry.clear();
            }
            entry = rmd.sourc.getEntry(formName, requestID, fieldArray);
            if (entry.size() > fieldList.size()) {
                for (Iterator<Map.Entry<Integer, Value>> it = entry.entrySet().iterator(); it.hasNext();) {
                    Map.Entry<Integer, Value> e = it.next();
                    if (!fieldList.contains(e.getKey())) {
                        it.remove();
                    }
                }

                //for (Map.Entry<Integer, Value> e : entry.entrySet()) {
                //    if (!fieldList.contains(e.getKey())) {
                //        entry.remove(e.getKey());
                //    }
                //}
            }
            for (Integer fieldIdAnexo : fieldListAnexos) {
                AttachmentValue anexo = (AttachmentValue) entry.get(fieldIdAnexo).getValue();
                if (anexo != null) {
                    try {
                        anexo.setValue(rmd.sourc.getEntryBlob(formName, entry.getEntryId(), fieldIdAnexo));
                        entry.put(fieldIdAnexo, new Value(anexo));
                    } catch (ARException ex) {
                        Log.get().error(ex);
                    }
                }
            }

            String newEntry = entry.getEntryId().trim();
            entry.setEntryId(newEntry);
            
            if (formName.equals("SRM:Request")){
                String obo = entry.get(60600).toString();
                while (obo.contains("''")){
                    obo = obo.replaceAll("''","'");
                }
                while (obo.contains(";;")){
                    obo = obo.replaceAll(";;",";");
                }
                while (obo.contains(";';")){
                    obo = obo.replaceAll(";';",";");
                }
                entry.put(60600, new Value(obo));
                int a=0;
            }
                //'P560180';'';'CB471364';
            
            
            mergeServer.mergeEntry(formName, entry, Constants.AR_MERGE_ENTRY_DUP_MERGE | Constants.AR_MERGE_NO_REQUIRED_INCREMENT | Constants.AR_MERGE_NO_PATTERNS_INCREMENT | Constants.AR_MERGE_NO_WORKFLOW_FIRED | Constants.AR_MERGE_NO_ASSOCIATION_FIRED);

            //processStatusHistory(entry.get(15).toString(), formName, schemaIdDestino, entry.getEntryId());
            //entry.remove(15);
        } catch (ARException ex) {
            List<StatusInfo> lastStatus = ex.getLastStatus();
            if (formType.equals("Audit")) {
                if (lastStatus.size() == 1) {
                    if (lastStatus.get(0).getMessageNum() == ARErrors.AR_ERROR_CANNOT_MODIFY_SHADOW_FORM_DATA) {
                        // ignora
                        return null;
                    }
                }
            }
            for (StatusInfo s : lastStatus) {
                String msgType;
                switch (s.getMessageType()) {
                    case Constants.AR_RETURN_OK:
                        msgType = "OK";
                        break;
                    case Constants.AR_RETURN_WARNING:
                        msgType = "WARNING";
                        break;
                    case Constants.AR_RETURN_ERROR:
                        msgType = "ERROR";
                        break;
                    case Constants.AR_RETURN_FATAL:
                        msgType = "FATAL";
                        break;
                    case Constants.AR_RETURN_BAD_STATUS:
                        msgType = "BAD_STATUS";
                        break;
                    case Constants.AR_RETURN_PROMPT:
                        msgType = "PROMPT";
                        break;
                    case Constants.AR_RETURN_ACCESSIBLE:
                        msgType = "ACCESSIBLE";
                        break;
                    case Constants.AR_RETURN_TOOLTIP:
                        msgType = "TOOLTIP";
                        break;
                    default:
                        msgType = "UNKNOWN";
                }
                String error = String.format("%s (%d): %s; %s", msgType, s.getMessageNum(), s.getMessageText(), s.getAppendedText());
                Log.get().error(String.format(",%s,%s,%s,%s,%d,%s,%s", StringEscapeUtils.escapeCsv(formName.replaceAll("\n", "").replaceAll("\r", "")), StringEscapeUtils.escapeCsv(entry.getEntryId().replaceAll("\n", "").replaceAll("\r", "")), StringEscapeUtils.escapeCsv(error.replaceAll("\n", "").replaceAll("\r", "")), StringEscapeUtils.escapeCsv(msgType.replaceAll("\n", "").replaceAll("\r", "")), s.getMessageNum(), StringEscapeUtils.escapeCsv(s.getMessageText().replaceAll("\n", "").replaceAll("\r", "")), StringEscapeUtils.escapeCsv(s.getAppendedText().replaceAll("\n", "").replaceAll("\r", ""))));
            }
        }
        return null;
    }

    /*
    private void processStatusHistory(String statusHistoryString, String formName, int schemaIdDestino, String requestId) {
        try {
            List<List<Value>> q1 = simpleSQL(rmd.desti, "select count(entryid) from h%s where entryid='%s'", str(schemaIdDestino), requestId);
            if (q1.get(0).get(0).getIntValue() == 0) {
                // insert
                List<String> campos = new ArrayList<>();
                List<String> valores = new ArrayList<>();
                StatusHistoryValue statusHistory = StatusHistoryValue.decode(statusHistoryString);
                SelectionField c7 = (SelectionField) rmd.sourc.getField(formName, 7);
                SelectionFieldLimit fieldLimit = (SelectionFieldLimit) c7.getFieldLimit();
                List<EnumItem> enums = fieldLimit.getValues();
                campos.add("ENTRYID");
                valores.add(String.format("'%s'", requestId));
                for (int i = 0; i < statusHistory.size(); i++) {
                    if (statusHistory.get(i) != null) {
                        int enumId = enums.get(i).getEnumItemNumber();
                        campos.add(String.format("T%d", enumId));
                        valores.add(String.format("%d", statusHistory.get(i).getTimestamp().getValue()));
                        campos.add(String.format("U%d", enumId));
                        valores.add(String.format("'%s'", statusHistory.get(i).getUser()));
                    }
                }
                String joinCampos = String.join(",", campos);
                String joinValores = String.join(",", valores);

                String sql = String.format("insert into T%d (%s) values (%s)", schemaIdDestino, joinCampos, joinValores);
                simpleSQL(rmd.desti, sql);
                int a = 0;

            } else {
                // update
            }
        } catch (ARException ex) {
            Log.get().error(ex);
        }
    }
     */
}
