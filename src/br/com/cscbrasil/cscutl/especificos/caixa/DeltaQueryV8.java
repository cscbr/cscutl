/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.especificos.caixa;

import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import static br.com.cscbrasil.cscutl.common.Util.simpleSQL;
import static br.com.cscbrasil.cscutl.especificos.Caixa.multi;
import static br.com.cscbrasil.cscutl.especificos.Caixa.threadPool002;
import static br.com.cscbrasil.cscutl.especificos.caixa.Delta.str;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.SQLResult;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.RecursiveTask;
import java.util.concurrent.RecursiveTask;

/**
 *
 * @author ivan
 */
public class DeltaQueryV8 extends RecursiveTask {

    final int c6Start;
    final int c6Final;
    final int schemaIdSource;
    final int schemaIdDestino;
    final RemedyConnection rmd;
    final String formName;
    final String formType;

    public DeltaQueryV8(RemedyConnection rmd, String formName, int c6Start, int c6Final, int schemaIdSource, int schemaIdDestino, String formType) {
        this.c6Start = c6Start;
        this.c6Final = c6Final;
        this.schemaIdSource = schemaIdSource;
        this.schemaIdDestino = schemaIdDestino;
        this.rmd = rmd;
        //this.rmd = RemedyConnections.get();
        this.formName = formName;
        this.formType = formType;
    }

    @Override
    protected Object compute() {
        //double pct = (((c6Start - C6START) + 0.0) / ((C6FINAL - C6START) + 0.0)) * 100;
        //String c6StartTxt = epoch2String(c6Start);
        String cmdSource = String.format("select c1,c6 from T%d where c6>=%d and c6<=%d order by c1", schemaIdSource, c6Start, c6Final - 1);

        SQLResult listSqlSource;
        try {
            //Log.get().info(String.format("   v8:%s (%f%%) %s", cmdSource, pct, c6StartTxt));
            listSqlSource = rmd.sourc.getListSQL(cmdSource, 999999999, false);
        } catch (ARException ex) {
            Log.get().error(ex);
            return null;
        }
        List<String> ids = new ArrayList<>();
        List<List<Value>> sourceContents = listSqlSource.getContents();
        Map<RecursiveTask, Integer> a002s = new ConcurrentHashMap<>();
        for (int i = 0; i < sourceContents.size(); i++) {
            List<Value> record = sourceContents.get(i);
            ids.add("'" + record.get(0).toString() + "'");
            if (ids.size() == 1000 || (i + 1) >= sourceContents.size()) {
                if (multi) {
                    RecursiveTask action = new DeltaCheckV9(ids, rmd, schemaIdSource, schemaIdDestino, formName, sourceContents, i, formType);
                    a002s.put(action, 0);
                    threadPool002.execute(action);
                } else {
                    DeltaCheckV9 action = new DeltaCheckV9(ids, rmd, schemaIdSource, schemaIdDestino, formName, sourceContents, i, formType);
                    action.compute();
                }
                ids.clear();
            }
        }
        if (multi) {
            Iterator<RecursiveTask> it = a002s.keySet().iterator();
            while (it.hasNext()) {
                RecursiveTask a002 = it.next();
                a002.join();
            }
        }
        return null;
    }

    public static void upControle(RemedyConnection rmd, String formulario, String requestId, String operacao, Integer c6v8, Integer c6v9) {
        String v8_modify = String.format("TO_DATE('19700101','yyyymmdd') + ((%s-(3*60*60))/24/60/60)", str(c6v8));
        String v9_modify = String.format("TO_DATE('19700101','yyyymmdd') + ((%s-(3*60*60))/24/60/60)", str(c6v9));
        List<List<Value>> simpleSQL;
        try {
            simpleSQL = simpleSQL(rmd.sourc, "select formulario,requestid from aaa_delta_operacao where formulario='%s' and requestid='%s'", formulario, requestId);
        } catch (ARException ex) {
            Log.get().error(ex);
            return;
        }
        if (simpleSQL.isEmpty()) {
            try {
                simpleSQL(rmd.sourc, "insert into aaa_delta_operacao (data_criacao,data_atualizacao,formulario,requestid,operacao,c6v8,c6v9,v8_modify,v9_modify) values (systimestamp(0),systimestamp(0),'%s','%s','%s',%s,%s,%s,%s)", formulario, requestId, operacao, str(c6v8), str(c6v9), v8_modify, v9_modify);
            } catch (ARException ex) {
                Log.get().error(ex);
            }
        } else {
            try {
                simpleSQL(rmd.sourc, "update aaa_delta_operacao set data_atualizacao=systimestamp(0),operacao='%s',c6v8=%s,c6v9=%s,v8_modify=%s,v9_modify=%s where formulario='%s' and requestid='%s'", operacao, str(c6v8), str(c6v9), v8_modify, v9_modify, formulario, requestId);
            } catch (ARException ex) {
                Log.get().error(ex);
            }
        }
    }

}
