/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.especificos.caixa;

import br.com.cscbrasil.bmc.ARServerUser;
import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import br.com.cscbrasil.cscutl.common.Util;
import static br.com.cscbrasil.cscutl.common.Util.simpleSQL;
import static br.com.cscbrasil.cscutl.especificos.Caixa.multi;
import static br.com.cscbrasil.cscutl.especificos.Caixa.rmd;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.SQLResult;
import com.bmc.arsys.api.Timestamp;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import static br.com.cscbrasil.cscutl.especificos.Caixa.threadPool001;
import com.bmc.arsys.api.ArchiveInfo;
import com.bmc.arsys.api.AuditInfo;
import com.bmc.arsys.api.CharacterFieldLimit;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Field;
import com.bmc.arsys.api.FieldLimit;
import com.bmc.arsys.api.Form;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author ivan
 */
public final class Delta {

    RemedyConnection rmd;
    static ARServerUser mergeServer;

    public static int C6START = 1523750400;
    //public static int C6FINAL = 1525087611;
    //public static int C6FINAL = 1524873599;
    public static Map<String, List<Integer>> fieldIdLists;
    public static Map<String, List<Integer>> fieldListsAnexos;
    private final String strData1;
    //private final String strData2;

    public Delta() {
        strData1 = epoch2String(C6START);
        //strData2 = epoch2String(C6FINAL);
        fieldIdLists = new HashMap<>();
        fieldListsAnexos = new HashMap<>();
        mergeServer = new ARServerUser("Remedy Application Service", "R!APp@Svc#metsysra", null, "en_US", "cctdcapllx0393.df.caixa", 40000);
        mergeServer.setTimeoutLong(1800);
        mergeServer.setTimeoutNormal(1800);
        mergeServer.setTimeoutXLong(1800);
        Log.get().info("Servidor merge = " + mergeServer.getServer());
    }

    public void run(String formEspecifico) {

        rmd = RemedyConnections.get();
        rmd.sourc.setTimeoutLong(1800);
        rmd.sourc.setTimeoutNormal(1800);
        rmd.sourc.setTimeoutXLong(1800);
        rmd.desti.setTimeoutLong(1800);
        rmd.desti.setTimeoutNormal(1800);
        rmd.desti.setTimeoutXLong(1800);

        /*
        try {
            simpleSQL(rmd.sourc, "update aaa_delta_controle set situacao=null,v8_existe=null,v9_existe=null,v9_criar=null,v9_alterar=null");
        } catch (ARException ex) {
            Log.get().error(ex);
            return;
        }
         */
        Log.get().info("Lendo estrutura dos formularios...");
        List<String> formNames = new ArrayList<>();
        Map<String, String> formTypes = new HashMap<>();
        List<List<Value>> formNameContents = new ArrayList<>();
        if (formEspecifico == null) {
            try {
                formNameContents = simpleSQL(rmd.sourc, "select formulario from aaa_delta_controle where status_text='ATIVO' order by formulario");
                //formNameContents = simpleSQL(rmd.sourc, "select formulario from aaa_delta_controle where status_text='ATIVO' order by formulario");
            } catch (ARException ex) {
                Log.get().error("select formulario from aaa_delta_controle where status_text='ATIVO' order by formulario", ex);
                return;
            }
        } else {
            List<Value> v = new ArrayList<>();
            v.add(new Value(formEspecifico));
            formNameContents.add(v);
        }

        for (List<Value> record : formNameContents) {
            String formName = record.get(0).toString();
            // Formulario existe em ambos servidores ?
            String formType = "";
            try {
                Form form = rmd.sourc.getForm(formName);
                formType = getformType(form);
            } catch (ARException ex) {
                try {
                    simpleSQL(rmd.sourc, "update aaa_delta_controle set v8_existe='NAO',tipo='%s' where formulario='%s'", formType, formName);
                } catch (ARException ex1) {
                    Log.get().error(ex);
                }
                continue;
            }
            try {
                mergeServer.getForm(formName);
                Form form = rmd.desti.getForm(formName);
                formType = getformType(form);
            } catch (ARException ex) {
                try {
                    simpleSQL(rmd.sourc, "update aaa_delta_controle set v9_existe='NAO',tipo='%s' where formulario='%s'", formType, formName);
                } catch (ARException ex1) {
                    Log.get().error(ex);
                }
                continue;
            }
            try {
                simpleSQL(rmd.sourc, "update aaa_delta_controle set v8_existe='SIM',v9_existe='SIM',tipo='%s' where formulario='%s'", formType, formName);
            } catch (ARException ex) {
                Log.get().error(ex);
            }
            formNames.add(formName);
            formTypes.put(formName, formType);
            Log.get().info(formName);
            // Campos em comum entre os formularios
            try {
                List<Field> sourcFields = rmd.sourc.getListFieldObjects(formName, Constants.AR_FIELD_TYPE_DATA, 0);
                List<Field> destiFields = rmd.desti.getListFieldObjects(formName, Constants.AR_FIELD_TYPE_DATA, 0);
                List<Field> mergeFields = mergeServer.getListFieldObjects(formName, Constants.AR_FIELD_TYPE_DATA, 0);

                List<Integer> sourceFieldIdList = new ArrayList<>();
                List<Integer> destinFieldIdList = new ArrayList<>();
                List<Integer> mergeFieldIdList = new ArrayList<>();
                List<Integer> sourceFieldIdAttachList = new ArrayList<>();
                List<Integer> destinFieldIdAttachList = new ArrayList<>();
                List<Integer> mergeFieldIdAttachList = new ArrayList<>();
                for (Field f : sourcFields) {
                    if (f.getFieldOption() == Constants.AR_FIELD_OPTION_DISPLAY) {
                        continue;
                    }
                    //if (f.getFieldID() == 15) {
                    //    continue;
                    //}
                    sourceFieldIdList.add(f.getFieldID());
                    if (f.getDataType() == Constants.AR_DATA_TYPE_ATTACH) {
                        sourceFieldIdAttachList.add(f.getFieldID());
                    }
                }
                for (Field f : destiFields) {
                    if (f.getFieldOption() == Constants.AR_FIELD_OPTION_DISPLAY) {
                        continue;
                    }
                    //if (f.getFieldID() == 15) {
                    //    continue;
                    //}
                    destinFieldIdList.add(f.getFieldID());
                    if (f.getDataType() == Constants.AR_DATA_TYPE_ATTACH) {
                        destinFieldIdAttachList.add(f.getFieldID());
                    }
                }
                for (Field f : mergeFields) {
                    if (f.getFieldOption() == Constants.AR_FIELD_OPTION_DISPLAY) {
                        continue;
                    }
                    //if (f.getFieldID() == 15) {
                    //    continue;
                    //}
                    mergeFieldIdList.add(f.getFieldID());
                    if (f.getDataType() == Constants.AR_DATA_TYPE_ATTACH) {
                        mergeFieldIdAttachList.add(f.getFieldID());
                    }
                }
                sourceFieldIdList.retainAll(destinFieldIdList);
                sourceFieldIdList.retainAll(mergeFieldIdList);
                sourceFieldIdAttachList.retainAll(destinFieldIdAttachList);
                sourceFieldIdAttachList.retainAll(mergeFieldIdAttachList);

                /*
                // verificar mudancas de tipo de campo
                for (Integer f : sourceFieldIdList) {
                    Field fSource = null;
                    Field fDestin = null;
                    for (Field a : sourcFields) {
                        if (a.getFieldID() == f) {
                            fSource = a;
                            break;
                        }
                    }
                    for (Field a : destiFields) {
                        if (a.getFieldID() == f) {
                            fDestin = a;
                            break;
                        }
                    }
                    //Field fSource = rmd.sourc.getField(formName, f);
                    //Field fDestin = rmd.desti.getField(formName, f);
                    if (fSource.getFieldType() != fDestin.getFieldType()) {
                        Log.get().error(String.format(",tipo,%s,%s,%d,%d,%d", formName, fSource.getName(), fSource.getFieldID(), fSource.getFieldType(), fDestin.getFieldType()));
                    } else {
                        if (fSource.getDataType() == Constants.AR_DATA_TYPE_CHAR && fSource.getFieldID() != 15) {
                            CharacterFieldLimit lSource = (CharacterFieldLimit) fSource.getFieldLimit();
                            CharacterFieldLimit lDesti = (CharacterFieldLimit) fDestin.getFieldLimit();
                            if (lSource == null && lDesti != null) {
                                Log.get().error(String.format(",tamanho,%s,%s,%d,,%d", formName, fSource.getName(), fSource.getFieldID(), fDestin.getFieldType()));
                            }
                            if (lSource != null && lDesti == null) {
                                Log.get().error(String.format(",tamanho,%s,%s,%d,%d,", formName, fSource.getName(), fSource.getFieldID(), fSource.getFieldType()));
                            }
                            if (lSource == null && lDesti == null) {
                                Log.get().error(String.format(",tamanho,%s,%s,%d,,", formName, fSource.getName(), fSource.getFieldID()));
                            }
                            if (lSource != null && lDesti != null) {
                                if (lSource.getMaxLength() != lDesti.getMaxLength()) {
                                    Log.get().error(String.format(",tamanho,%s,%s,%d,%d,%d", formName, fSource.getName(), fSource.getFieldID(), lSource.getMaxLength(), lDesti.getMaxLength()));
                                }
                            }
                        }
                    }
                }
                 */
                fieldIdLists.put(formName, new ArrayList<>(sourceFieldIdList));
                fieldListsAnexos.put(formName, new ArrayList<>(sourceFieldIdAttachList));
            } catch (ARException ex) {
                Log.get().error(ex);
            }
        }

        long startMilis = System.currentTimeMillis();
        for (String formName : formNames) {
            try {
                formulario(formName, formTypes.getOrDefault(formName, "Regular"));
            } catch (ARException ex) {
                Log.get().error(formName, ex);
            }
        }
        long endMilis = System.currentTimeMillis();
        double lapse = ((0.0 + endMilis) - (0.0 + startMilis)) / 1000.0;
        Log.get().info(String.format("%f segundos", lapse));
    }

    private void formulario(String formName, String formType) throws ARException {
        int schemaIdSource = Util.getFormNumber(rmd.sourc, formName);
        int schemaIdDestino = Util.getFormNumber(rmd.desti, formName);

        simpleSQL(rmd.sourc, "update aaa_delta_controle set situacao='Lendo' where formulario='%s'", formName);

        if (schemaIdSource == 0) {
            Log.get().error("Formulario nao existe na v8: " + formName);
            simpleSQL(rmd.sourc, "update aaa_delta_controle set v8_existe='NAO' where formulario='%s'", formName);
        } else {
            simpleSQL(rmd.sourc, "update aaa_delta_controle set v8_existe='SIM' where formulario='%s'", formName);
        }
        if (schemaIdDestino == 0) {
            Log.get().error("Formulario nao existe na v9: " + formName);
            simpleSQL(rmd.sourc, "update aaa_delta_controle set v9_existe='NAO' where formulario='%s'", formName);
        } else {
            simpleSQL(rmd.sourc, "update aaa_delta_controle set v9_existe='SIM' where formulario='%s'", formName);
        }
        if (schemaIdSource == 0 || schemaIdDestino == 0) {
            simpleSQL(rmd.sourc, "update aaa_delta_controle set situacao='Lido' where formulario='%s'", formName);
            return;
        }

        //Log.get().info(formName + " entre " + strData1 + " e " + strData2);
        Log.get().info(formName + " a partir de " + strData1);
        int c6Start = C6START;
        int c6Final = C6START;
        int jump = 7200; // 2 horas

        Map<RecursiveTask, Integer> a001s = new ConcurrentHashMap<>();
        //List<RecursiveTask> a001s = new ArrayList<>();
        //while (c6Final <= C6FINAL) {
        while (true) {

            c6Start = c6Final;
            c6Final += (jump + 1);
            //if (c6Final > C6FINAL) {
            //    c6Final = C6FINAL;
            //}
            if (multi) {
                RecursiveTask action = new DeltaQueryV8(rmd, formName, c6Start, c6Final, schemaIdSource, schemaIdDestino, formType);
                a001s.put(action, 0);
                threadPool001.execute(action);
            } else {
                DeltaQueryV8 action = new DeltaQueryV8(rmd, formName, c6Start, c6Final, schemaIdSource, schemaIdDestino, formType);
                action.compute();
            }
            //if (c6Final == C6FINAL) {
            //    break;
            //}
            int teste = (int) (System.currentTimeMillis() / 1000L);
            if (c6Final >= teste) {
                break;
            }
        }
        if (multi) {
            Iterator<RecursiveTask> it = a001s.keySet().iterator();
            while (it.hasNext()) {
                RecursiveTask a001 = it.next();
                a001.join();
            }
        }

        int a = 0;

        try {
            simpleSQL(rmd.sourc, "update aaa_delta_controle set situacao='Lido',v9_criar=(select count(data_criacao) from aaa_delta_operacao where formulario='%s' and operacao='CRIAR_V9'),v9_alterar=(select count(data_criacao) from aaa_delta_operacao where formulario='%s' and operacao='ATUALIZAR_V9') where formulario='%s'", formName, formName, formName);
        } catch (ARException ex) {
            Log.get().error(ex);
        }
    }

    public static String str(Integer n) {
        if (n == null) {
            return "null";
        }
        return String.format("%d", n);
    }

    String epoch2String(long value) {
        DateTime jodaDateTime = new DateTime((value + 60 * 60 * 3) * 1000);
        DateTimeFormatter dtfOut = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
        return dtfOut.print(jodaDateTime);
    }

    public static String getformType(Form form) {
        String retorno;
        int formTypeAux = form.getFormType();
        switch (formTypeAux) {
            case Constants.AR_SCHEMA_REGULAR:
                retorno = "Regular";
                break;
            case Constants.AR_SCHEMA_JOIN:
                retorno = "Join";
                break;
            case Constants.AR_SCHEMA_VIEW:
                retorno = "View";
                break;
            case Constants.AR_SCHEMA_DIALOG:
                retorno = "Display";
                break;
            case Constants.AR_SCHEMA_VENDOR:
                retorno = "Vendor";
                break;
            case Constants.AR_SCHEMA_PLACEHOLDER:
                retorno = "Placeholder";
                break;
            default:
                retorno = "Unknown";
                break;
        }

        ArchiveInfo a = form.getArchiveInfo();
        AuditInfo b = form.getAuditInfo();
        if (a.getArchiveType() == Constants.AR_ARCHIVE_NONE && a.getArchiveFrom() != null) {
            if (!a.getArchiveFrom().isEmpty()) {
                retorno = "Archive";
            }
        }
        if (b.getAuditStyle() == Constants.AR_AUDIT_NONE && b.getAuditForm() != null) {
            if (!b.getAuditForm().isEmpty()) {
                retorno = "Audit";
            }
        }
        if (b.getAuditStyle() == Constants.AR_AUDIT_LOG_SHADOW) {
            retorno = "Audit";
        }
        return retorno;
    }

    private void processa(List<String> ids, int schemaIdDestino) {


        /*
        // multithread registro a registro na v9
        List<RecursiveTask> actions = new ArrayList<>();

        for (List<Value> record : sourceContents) {
            RecursiveTask action = new DeltaDetalhe(formName, schemaIdDestino, record);
            actions.add(action);
            threadPool.execute(action);
        for (RecursiveTask action : actions) {
            action.join();
        }
        }
        for (RecursiveTask action : actions) {
            action.join();
        }

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
         */
    }

}
