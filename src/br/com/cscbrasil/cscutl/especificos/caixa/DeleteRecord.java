package br.com.cscbrasil.cscutl.especificos.caixa;

import br.com.cscbrasil.cscutl.common.Util;
import br.com.cscbrasil.cscutl.especificos.Caixa;
import com.bmc.arsys.api.ARException;
import com.marzapower.loggable.Log;
import java.util.List;
import java.util.concurrent.RecursiveAction;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DeleteRecord extends RecursiveAction {

    String formName;
    String recordId;
    String atualizarId;
    List<String> dbTables;

    public DeleteRecord(String formName, String recordId, String atualizarId, List<String> dbTables) {
        this.formName = formName;
        this.recordId = recordId;
        this.atualizarId = atualizarId;
        if (dbTables != null) {
            this.dbTables = dbTables;
        } else {
            this.dbTables = Util.getDBTables(Caixa.ar, formName);
        }
    }

    @Override
    protected void compute() {
        //Log.get().debug(String.format("Eliminando %s de %s em %s", recordId, formName, Caixa.ar.getServer()));
        boolean erro = false;
        try {
            Util.deleteViaDBByEntryID(Caixa.ar, formName, recordId, dbTables);
        } catch (ARException ex1) {
            erro = true;
            Log.get().error(ex1.getMessage(), ex1);
        }
        if (!erro) {
            String cmd = String.format("update CAIXA_IDsFormsExcluidos set status = 4 where REQUEST_ID='%s'", atualizarId);
            try {
                Caixa.ar.getListSQL(cmd, 999999999, false);
            } catch (ARException ex) {
                Log.get().error(ex);
            }
        }
    }

}
