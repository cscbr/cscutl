/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.especificos.caixa;

import br.com.cscbrasil.bmc.RemedyConnection;
import static br.com.cscbrasil.cscutl.common.Util.epoch2String;
import static br.com.cscbrasil.cscutl.especificos.Caixa.multi;
import static br.com.cscbrasil.cscutl.especificos.Caixa.threadPool002;
import static br.com.cscbrasil.cscutl.especificos.Caixa.threadPool003;
import static br.com.cscbrasil.cscutl.especificos.caixa.DeltaQueryV8.upControle;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.SQLResult;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.RecursiveTask;

/**
 *
 * @author ivan
 */
public class DeltaCheckV9 extends RecursiveTask {

    List<String> ids;
    RemedyConnection rmd;
    int schemaIdDestino;
    int schemaIdSource;
    String formName;
    List<List<Value>> sourceContents;
    String formType;
    int i;

    public DeltaCheckV9(List<String> ids, RemedyConnection rmd, int schemaIdSource, int schemaIdDestino, String formName, List<List<Value>> sourceContents, int i, String formType) {
        this.ids = new ArrayList(ids);
        this.rmd = rmd;
        this.schemaIdDestino = schemaIdDestino;
        this.schemaIdSource = schemaIdSource;
        this.formName = formName;
        this.sourceContents = sourceContents;
        this.i = i;
        this.formType = formType;
    }

    @Override
    protected Object compute() {
        String inTxt = String.join(",", ids);
        String cmdDestino = String.format("select c1,c6 from T%d where c1 in (%s) order by c1", schemaIdDestino, inTxt);
        SQLResult listSqlDestino;
        try {
            //Log.get().info("      v9:" + cmdDestino);
            listSqlDestino = rmd.desti.getListSQL(cmdDestino, 999999999, false);
        } catch (ARException ex) {
            Log.get().error(ex);
            return null;
        }
        List<List<Value>> destiContents = listSqlDestino.getContents();

        Map<RecursiveTask, Integer> a003s = new ConcurrentHashMap<>();
        // Criacao
        if (destiContents.size() != ids.size()) {
            // pesquisou 1000 ids na v8 mas nao achou os 1000 na v9
            int v8c = 0;
            int v9c = 0;
            while (v8c < ids.size()) {
                String c1v8 = ids.get(v8c).replaceAll("'", "");
                String c1v9 = "~";
                if (v9c < destiContents.size()) {
                    c1v9 = destiContents.get(v9c).get(0).toString();
                }
                if (c1v8.equals(c1v9)) {
                    v8c++;
                    v9c++;
                    continue;
                }
                //Log.get().info("Falta criar     v9 " + formName + " " + c1v8);
                upControle(rmd, formName, c1v8, "CRIAR_V9", null, null);
                if (!formType.equals("Regular")) {
                    int a = 0;
                }
                if (multi) {
                    RecursiveTask action = new DeltaMigra(rmd, formName, c1v8, schemaIdSource, schemaIdDestino, formType);
                    a003s.put(action, 0);
                    threadPool003.execute(action);
                } else {
                    DeltaMigra deltaMigra = new DeltaMigra(rmd, formName, c1v8, schemaIdSource, schemaIdDestino, formType);
                    deltaMigra.compute();
                }
                v8c++;
            }
        }

        // Alteracao
        int j = 0;
        for (int k = 0; k < destiContents.size(); k++) {
            List<Value> recordCompareDesti = destiContents.get(k);
            String c1v9 = recordCompareDesti.get(0).toString();
            int c6v9 = recordCompareDesti.get(1).getIntValue();
            //String c6v9String = epoch2String(c6v9);
            for (; j <= i; j++) {
                List<Value> recordCompareFonte = sourceContents.get(j);
                String c1v8 = recordCompareFonte.get(0).toString();
                int c6v8 = recordCompareFonte.get(1).getIntValue();
                //String c6v8String = epoch2String(c6v8);
                if (c1v8.compareTo(c1v9) < 0) {
                    continue;
                }
                if (c1v8.compareTo(c1v9) == 0) {
                    //if (c6v8 > c6v9 ) {
                    if (c6v8 > c6v9 && c6v9 <= Delta.C6START) { // v8 atualizada depois do backup e v9 com data antes do backup
                        // estou limitando o campo c6v9 porque se for maior que dia 14 o delta original levou o dado
                        // Falta atualizar na v9
                        //Log.get().info("Falta atualizar v9 " + formName + " " + c1v9 + " v8.c6=" + c6v8String + " v9.c6=" + c6v9String);                        
                        upControle(rmd, formName, c1v9, "ATUALIZAR_V9", c6v8, c6v9);
                        if (!formType.equals("Regular")) {
                            int a = 0;
                        }
                        if (multi) {
                            RecursiveTask action = new DeltaMigra(rmd, formName, c1v8, schemaIdSource, schemaIdDestino, formType);
                            a003s.put(action, 0);
                            threadPool003.execute(action);
                        } else {
                            DeltaMigra deltaMigra = new DeltaMigra(rmd, formName, c1v8, schemaIdSource, schemaIdDestino, formType);
                            deltaMigra.compute();
                        }
                    }
                    j++;
                    break;
                }
            }
        }
        if (multi) {
            Iterator<RecursiveTask> it = a003s.keySet().iterator();
            while (it.hasNext()) {
                RecursiveTask a003 = it.next();
                a003.join();
            }
        }
        return null;
    }

}
