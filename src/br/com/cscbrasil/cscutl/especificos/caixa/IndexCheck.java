/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.especificos.caixa;

import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import static br.com.cscbrasil.cscutl.especificos.Caixa.ar;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.SQLResult;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ivan
 */
public class IndexCheck {

    RemedyConnection rmd;

    public void run() throws IOException {
        rmd = RemedyConnections.get();
        metadados();
    }

    private void metadados() throws FileNotFoundException, IOException {
        Log.get().info("Verificando indices das tabelas de metadados");
        String file = "indices_nativos_9_1_04_002.csv";
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            for (String line; (line = br.readLine()) != null;) {
                String[] split = line.split(",");
                String TABLE_NAME = split[0];
                String TABLESPACE_NAME = split[1];
                String STATUS = split[2];
                String INDEX_NAME = split[3];
                String INDEX_TYPE = split[4];
                String TABLE_TYPE = split[5];
                String UNIQUENESS = split[6];
                String COLUMN_NAME = split[7];
                String COLUMN_POSITION = split[8];
                String COLUMN_LENGTH = split[9];
                String CHAR_LENGTH = split[10];
                String DESCEND = split[11];
                if (TABLE_NAME.equals("TABLE_NAME")) {
                    continue;
                }
                if (!INDEX_TYPE.equals("NORMAL")){
                    continue;
                }
                SQLResult listSQL;

                // Existe a tabela ??
                String cmd = String.format("select * from user_tables where table_name='%s'", TABLE_NAME);
                try {
                    listSQL = rmd.desti.getListSQL(cmd, 0, false);
                } catch (ARException ex) {
                    Log.get().error(ex);
                    continue;
                }
                List<List<Value>> contents = listSQL.getContents();
                if (contents.size() == 0) {
                    continue;
                }

                // Existe o indice ?
                cmd = String.format("select * from user_indexes where table_name='%s' and index_name='%s'", TABLE_NAME, INDEX_NAME);
                try {
                    listSQL = rmd.desti.getListSQL(cmd, 0, false);
                } catch (ARException ex) {
                    Log.get().error(ex);
                    continue;
                }
                contents = listSQL.getContents();
                if (contents.size() == 0) {
                    Log.get().error(String.format("Indice %s de %s nao existe",INDEX_NAME,TABLE_NAME));
                    continue;
                }
            }

        }

    }

}
