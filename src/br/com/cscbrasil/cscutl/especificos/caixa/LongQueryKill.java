/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.especificos.caixa;

import br.com.cscbrasil.bmc.ARServerUser;
import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import br.com.cscbrasil.common.Parameters;
import br.com.cscbrasil.cscutl.common.Util;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.SQLResult;
import com.bmc.arsys.api.Timestamp;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 *
 * @author ivan
 */
public class LongQueryKill {

    public void run() {
        RemedyConnection rmd = RemedyConnections.get();

        int minutos = 15;
        try {
            minutos = Parameters.getInt("long", "timeout");
        } catch (IOException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException | InvalidKeyException | BadPaddingException | NumberFormatException ex) {
            Log.get().error(ex);
            return;
        }

        Log.get().info(String.format("Queries mais velhas do que %d minutos serao eliminadas...", minutos));

        ARServerUser ar = rmd.desti;

        String cmd = String.format("select s.sid sid,s.SERIAL# serial,s.inst_id inst,s.sql_exec_start,sysdate,t.sql_text from gv$sqltext_with_newlines t inner join gv$session s on t.ADDRESS = s.SQL_ADDRESS And t.HASH_VALUE = s.SQL_HASH_VALUE where s.USERNAME ='ARADMIN' and s.osuser='arsystem' And s.STATUS = 'ACTIVE' and t.piece=0 and (sysdate-sql_exec_start)*1440>%d order by s.sql_exec_start", minutos);

        while (true) {
            try {
                Log.get().info("Pesquisando...");
                SQLResult listSQL = ar.getListSQL(cmd, 999999999, false);
                List<List<Value>> contents = listSQL.getContents();
                for (List<Value> content : contents) {
                    int sid = content.get(0).getIntValue();
                    int serial = content.get(1).getIntValue();
                    int inst = content.get(2).getIntValue();
                    Timestamp start = (Timestamp) content.get(3).getValue();
                    String start_string = timestamp2String(start);
                    Value v = content.get(4);
                    String sql = content.get(5).toString();
                    sql = sql.replace("\n", " ");
                    sql = sql.replace("\r", " ");
                    sql = sql.replace("\t", " ");
                    sql = sql.trim();
                    sql = sql + "                                                                             ";
                    String kill = String.format("alter system kill session '%d,%d,@%d' immediate", sid, serial, inst);
                    Log.get().info(String.format("Eliminar %s %s", start_string, sql.substring(0, 30)));
                    Log.get().info(String.format(kill));
                    try {
                        SQLResult listSQL1 = ar.getListSQL(kill, 999999999, false);
                    } catch (ARException ex) {
                    }
                }
            } catch (ARException ex) {
                Log.get().error(ex);
            }
            Util.minuteNap();
        }
    }

    String timestamp2String(Timestamp timestamp) {
        DateTime jodaDateTime = new DateTime(timestamp.getValue() * 1000);
        DateTimeFormatter dtfOut = DateTimeFormat.forPattern("MM/dd/yyyy HH:mm:ss");
        return dtfOut.print(jodaDateTime.toDateTime(DateTimeZone.forOffsetHours(-3)));
    }

}
