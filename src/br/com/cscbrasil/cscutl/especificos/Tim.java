/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.especificos;

import br.com.cscbrasil.bmc.ARServerUser;
import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Entry;
import com.bmc.arsys.api.Field;
import com.bmc.arsys.api.FieldMapping;
import com.bmc.arsys.api.Form;
import com.bmc.arsys.api.JoinFieldMapping;
import com.bmc.arsys.api.JoinForm;
import com.bmc.arsys.api.QualifierInfo;
import com.bmc.arsys.api.SQLResult;
import com.bmc.arsys.api.SortInfo;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ivan
 */
public class Tim {

    static public void new_sirios() throws ARException {
        RemedyConnection rmd = RemedyConnections.get();
        ARServerUser ar = rmd.sourc;
        ar.cscSetModeBest();
        QualifierInfo qualification = ar.parseQualification("AST:ClassAttributes", "1=1");
        ArrayList<SortInfo> sortList = new ArrayList<>();
        sortList.add(new SortInfo(1, Constants.AR_SORT_ASCENDING));
        int[] fieldIds = {8};

        List<Entry> asts = ar.getListEntryObjects("AST:ClassAttributes", qualification, Constants.AR_START_WITH_FIRST_ENTRY, 999999999, sortList, fieldIds, true, null);
        for (Entry ast : asts) {
            String formName = ast.get(8).toString();
            Form frm = null;
            try {
                frm = ar.getForm(formName);
            } catch (ARException ex) {

            }
            if (frm == null) {
                continue;
            }
            JoinForm exemplo = (JoinForm) ar.getForm("_EXEMPLO");
            exemplo.setMemberA(formName);
            exemplo.setMemberB(formName);
            String newName = "TIM:NEWSIRIOS:" + formName.replaceAll(":", "_");
            Log.get().info(newName);
            exemplo.setName(newName);
            ar.createForm(exemplo);

            List<Field> fields = ar.getListFieldObjects(formName);
            List<Field> output = new ArrayList<>();
            for (Field field : fields) {
                if (field.getFieldOption() != 4 && field.getFieldID() != 1 && field.getFieldID() != 15) {
                    field.setForm(newName);
                    FieldMapping fieldMap = new JoinFieldMapping(0, field.getFieldID());
                    field.setFieldMap(fieldMap);
                    field.setDisplayInstance(null);
                    field.setReservedIDOK(true);
                    output.add(field);
                    //ar.createField(field, true);
                }
            }
            if (!output.isEmpty()) {
                ar.createMultipleFields(output);
            }
        }
    }

    static public void new_sirios2() throws ARException {
        RemedyConnection rmd = RemedyConnections.get();
        ARServerUser ar = rmd.sourc;
        ar.cscSetModeBest();
        String cmd = "select NAME from arschema where name like 'BMC.CORE:%' AND SCHEMATYPE=2";
        SQLResult listSQL = ar.getListSQL(cmd, 999999999, false);
        List<List<Value>> contents = listSQL.getContents();
        for (List<Value> content : contents) {
            String formName = content.get(0).toString();
            Form frm = null;
            try {
                frm = ar.getForm(formName);
            } catch (ARException ex) {

            }
            if (frm == null) {
                continue;
            }
            JoinForm exemplo = (JoinForm) ar.getForm("_EXEMPLO2");
            exemplo.setMemberA(formName);
            exemplo.setMemberB(formName);
            String newName = "TIM:NEWSIRIOS:" + formName.replaceAll(":", "_");
            Log.get().info(newName);
            exemplo.setName(newName);
            try {
                ar.createForm(exemplo);
            } catch (ARException ex) {
                Log.get().error(ex);
                continue;
            }
            List<Field> fields = ar.getListFieldObjects(formName);
            List<Field> output = new ArrayList<>();
            for (Field field : fields) {
                if (field.getFieldOption() != 4 && field.getFieldID() != 1 && field.getFieldID() != 15) {
                    field.setForm(newName);
                    FieldMapping fieldMap = new JoinFieldMapping(0, field.getFieldID());
                    field.setFieldMap(fieldMap);
                    field.setDisplayInstance(null);
                    field.setReservedIDOK(true);
                    output.add(field);
                    //ar.createField(field, true);
                }
            }
            if (!output.isEmpty()) {
                try {
                    ar.createMultipleFields(output);
                } catch (ARException ex) {
                    Log.get().error(ex);
                }
            }
        }
    }

    static public void new_sirios3() throws IOException {

        List<String> t1 = new ArrayList<>();
        List<String> t2 = new ArrayList<>();
        List<String> t3 = new ArrayList<>();

        t1.add("BMC_APPLICATIONSERVICE");
        t2.add("TIM:NEWSIRIOS:BMC.CORE_BMC_ApplicationService");
        t3.add("BMC.CORE_BMC_ApplicationService");
        t1.add("BMC_APPLICATIONSYSTEM");
        t2.add("TIM:NEWSIRIOS:BMC.CORE_BMC_ApplicationSystem");
        t3.add("BMC.CORE_BMC_ApplicationSystem");
        t1.add("BMC_CLUSTER");
        t2.add("TIM:NEWSIRIOS:BMC.CORE_BMC_Cluster");
        t3.add("BMC.CORE_BMC_Cluster");
        t1.add("BMC_COMPUTERSYSTEM");
        t2.add("TIM:NEWSIRIOS:BMC.CORE_BMC_ComputerSystem");
        t3.add("BMC.CORE_BMC_ComputerSystem");
        t1.add("BMC_DATABASE");
        t2.add("TIM:NEWSIRIOS:BMC.CORE_BMC_Database");
        t3.add("BMC.CORE_BMC_Database");
        t1.add("BMC_IPCONNECTIVITYSUBNET");
        t2.add("TIM:NEWSIRIOS:BMC.CORE_BMC_IPConnectivitySubnet");
        t3.add("BMC.CORE_BMC_IPConnectivitySubnet");
        t1.add("BMC_IPENDPOINT");
        t2.add("TIM:NEWSIRIOS:BMC.CORE_BMC_IPEndPoint");
        t3.add("BMC.CORE_BMC_IPEndPoint");
        t1.add("BMC_LOGICALSYSTEMCOMPONENT");
        t2.add("TIM:NEWSIRIOS:BMC.CORE_BMC_LogicalSystemComponent");
        t3.add("BMC.CORE_BMC_LogicalSystemComponent");
        t1.add("BMC_OPERATINGSYSTEM");
        t2.add("TIM:NEWSIRIOS:BMC.CORE_BMC_OperatingSystem");
        t3.add("BMC.CORE_BMC_OperatingSystem");
        t1.add("BMC_PROCESSOR");
        t2.add("TIM:NEWSIRIOS:BMC.CORE_BMC_Processor");
        t3.add("BMC.CORE_BMC_Processor");
        t1.add("BMC_PRODUCT");
        t2.add("TIM:NEWSIRIOS:BMC.CORE_BMC_Product");
        t3.add("BMC.CORE_BMC_Product");
        t1.add("BMC_SOFTWARESERVER");
        t2.add("TIM:NEWSIRIOS:BMC.CORE_BMC_SoftwareServer");
        t3.add("BMC.CORE_BMC_SoftwareServer");
        t1.add("BMC_VIRTUALSYSTEMENABLER");
        t2.add("TIM:NEWSIRIOS:BMC.CORE_BMC_VirtualSystemEnabler");
        t3.add("BMC.CORE_BMC_VirtualSystemEnabler");

        for (int i = 0; i < t1.size(); i++) {
            String content = readFile("newmodelo.xml", StandardCharsets.UTF_8);
            content = content.replaceAll("BMC_APPLICATIONSERVICE", t1.get(i).toString());
            content = content.replaceAll("TIM:NEWSIRIOS:BMC.CORE_BMC_ApplicationService", t2.get(i).toString());
            content = content.replaceAll("BMC.CORE_BMC_ApplicationService", t3.get(i).toString());
            content = content.replaceAll("YORT1", t2.get(i).toString());
            String nome = t1.get(i).toString() + ".xml";
            Files.write(Paths.get(nome), content.getBytes());
        }

    }

    static String readFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
}
