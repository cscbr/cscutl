/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.especificos;

import br.com.cscbrasil.cscutl.especificos.nec.NecC2;
import br.com.cscbrasil.bmc.ARServerUser;
import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import br.com.cscbrasil.cscutl.common.Util;
import static br.com.cscbrasil.cscutl.common.Util.simpleSQL;
import br.com.cscbrasil.cscutl.especificos.nec.NecConta;
import br.com.cscbrasil.cscutl.especificos.nec.NecDB;
import br.com.cscbrasil.cscutl.especificos.nec.NecDelta;
import static br.com.cscbrasil.cscutl.especificos.nec.NecDelta.str;
import br.com.cscbrasil.cscutl.especificos.nec.NecEstrutura;
import br.com.cscbrasil.cscutl.especificos.nec.NecRetry;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ivan
 */
public class Nec {

    public static RemedyConnection rmd;
    public static ARServerUser ar;
    //public static ForkJoinPool threadPool001;
    //public static ForkJoinPool threadPool002;
    //public static ForkJoinPool threadPool003;
    public static boolean multi;
    public static String executor;

    public Nec(String exec) throws ClassNotFoundException, SQLException {
        rmd = RemedyConnections.get();
        rmd.sourc.cscSetModeBest();
        rmd.desti.cscSetModeBest();
        //threadPool001 = new ForkJoinPool(3, ForkJoinPool.defaultForkJoinWorkerThreadFactory, null, true);
        //threadPool002 = new ForkJoinPool(3, ForkJoinPool.defaultForkJoinWorkerThreadFactory, null, true);
        //threadPool003 = new ForkJoinPool(3, ForkJoinPool.defaultForkJoinWorkerThreadFactory, null, true);
        multi = false;
        executor = exec;
    }

    public void delta(String arg) throws ClassNotFoundException, SQLException, InterruptedException {
        NecDelta d = new NecDelta();
        d.run(arg);
    }

    public void conta(String arg) throws ClassNotFoundException, SQLException, InterruptedException {
        NecConta d = new NecConta();
        d.run(arg);
    }

    public void retry(String arg) throws ClassNotFoundException, SQLException, InterruptedException {
        NecRetry d = new NecRetry();
        d.run(arg);
    }

    public void user(String arg) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void c2(Object object) {
        NecC2 d = new NecC2();
        try {
            d.run();
        } catch (ARException ex) {
            Log.get().error(ex);
        }
    }

    public void delete(String formEspecifico) throws ClassNotFoundException, ARException {
        if (formEspecifico != null && (formEspecifico.equals("User") || formEspecifico.equals("Group"))) {
            return;
        }
        NecDB necDB = new NecDB();
        necDB = new NecDB();
        Map<String, Map<String, String>> forms;
        forms = necDB.readDB();

        long startMilis = System.currentTimeMillis();
        for (Map.Entry<String, Map<String, String>> entry : forms.entrySet()) {
            String key = entry.getKey();
            Map<String, String> value = entry.getValue();
            String status = value.getOrDefault("status", "desnecessário");
            if (status.equals("ativo")) {
                int schemaIdSource = Util.getFormNumber(rmd.sourc, key);
                int schemaIdDestino = Util.getFormNumber(rmd.desti, key);
                int countV8 = 0;
                int countV9 = 0;
                reDelete(key);
                // contagem
                List<List<Value>> resultSource = simpleSQL(rmd.sourc, "select count(c1) from T%s", str(schemaIdSource));
                List<List<Value>> resultDestin = simpleSQL(rmd.desti, "select count(c1) from T%s", str(schemaIdDestino));
                countV8 = resultSource.get(0).get(0).getIntValue();
                countV9 = resultDestin.get(0).get(0).getIntValue();
                List<List<Value>> resultSource1 = Util.simpleSQL(rmd.sourc, "select nextid from arschema where schemaid=%s", str(schemaIdSource));
                List<List<Value>> resultDestin1 = Util.simpleSQL(rmd.desti, "select nextid from arschema where schemaid=%s", str(schemaIdDestino));
                int nextV8 = resultSource1.get(0).get(0).getIntValue();
                int nextV9 = resultDestin1.get(0).get(0).getIntValue();
                necDB.atualiza_count(key, countV8, countV9, nextV8, nextV9);
                Log.get().info(String.format("%-50s v8=%-7d v9=%-7d dif=%-7d nxtV8=%-15d nextV9=%-15d", key, countV8, countV9, countV8 - countV9, nextV8, nextV9));
            } else {
                Log.get().info(key);
            }
        }
        long endMilis = System.currentTimeMillis();
        double lapse = ((0.0 + endMilis) - (0.0 + startMilis)) / 1000.0;
        Log.get().info(String.format("%f segundos", lapse));

    }

    private void reDelete(String formName) throws ARException {        
        String sql = String.format("select requestID from dbo.MIGRA_RecsEliminados where form='%s'", formName);
        List<List<Value>> records;
        try {
            records = Util.simpleSQL(rmd.sourc, sql);
        } catch (ARException ex) {
            Log.get().error(ex);
            return;
        }
        List<String> ids = new ArrayList<>();
        for (int i = 0; i < records.size(); i++) {
            List<Value> record = records.get(i);
            String entryId = record.get(0).toString();
            ids.add(entryId);
            if (ids.size() == 1000 || (i + 1) >= records.size()) {
                Util.deleteViaDBByEntryID(rmd.desti, formName, ids);
                ids.clear();
            }
        }
        if (ids.size() > 0) {
            Util.deleteViaDBByEntryID(rmd.desti, formName, ids);
        }
    }

}
