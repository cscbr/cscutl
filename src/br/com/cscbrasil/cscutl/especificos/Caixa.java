/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.especificos;

import br.com.cscbrasil.cscutl.especificos.caixa.Delta;
import br.com.cscbrasil.bmc.ARServerUser;
import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import br.com.cscbrasil.cscutl.common.SortIgnoreCase;
import br.com.cscbrasil.cscutl.common.Util;
import br.com.cscbrasil.cscutl.especificos.caixa.DeleteRecord;
import br.com.cscbrasil.cscutl.especificos.caixa.IndexCheck;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.SQLResult;
import com.bmc.arsys.api.Value;
import com.bmc.thirdparty.org.apache.commons.lang.StringEscapeUtils;
import com.marzapower.loggable.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

/**
 *
 * @author ivan
 */
public class Caixa {

    public static RemedyConnection rmd;
    public static ARServerUser ar;
    public static ForkJoinPool threadPool001;
    public static ForkJoinPool threadPool002;
    public static ForkJoinPool threadPool003;
    public static boolean multi;

    public Caixa() {
        rmd = RemedyConnections.get();
        rmd.sourc.cscSetModeBest();
        rmd.desti.cscSetModeBest();
        threadPool001 = new ForkJoinPool(128, ForkJoinPool.defaultForkJoinWorkerThreadFactory, null, true);
        threadPool002 = new ForkJoinPool(128, ForkJoinPool.defaultForkJoinWorkerThreadFactory, null, true);
        threadPool003 = new ForkJoinPool(128, ForkJoinPool.defaultForkJoinWorkerThreadFactory, null, true);
        multi = true;
    }

    public void delta_delete() {

        ar = rmd.sourc;
        ar.cscSetModeBest();

        List<String> forms;

        try {
            forms = ar.getListForm(0, Constants.AR_LIST_SCHEMA_REGULAR | Constants.AR_HIDDEN_INCREMENT);
        } catch (ARException ex) {
            Log.get().error(ex);
            return;
        }

        java.util.Collections.sort(forms, new SortIgnoreCase());

        Log.get().info("Carga formularios e tabelas relacionadas (T,B,H,etc)");

        Map<String, List<String>> tabelas = new HashMap<>();

        for (String formName : forms) {
            Log.get().info(formName);
            tabelas.put(formName, Util.getDBTables(ar, formName));
        }

        while (true) {
            long start = System.currentTimeMillis();
            SQLResult listSQL;
            try {
                listSQL = ar.getListSQL("select FORM,ID,REQUEST_ID from CAIXA_IDsFormsExcluidos where STATUS=0 and ROWNUM<1001", 999999999, false);
            } catch (ARException ex) {
                Log.get().error(ex);
                continue;
            }
            List<List<Value>> contents = listSQL.getContents();
            if (contents.size() == 0) {
                break;
            }
            List<RecursiveAction> actions = new ArrayList<>();
            for (List<Value> content : contents) {
                String formName = content.get(0).toString();
                String requestId = content.get(1).toString();
                String statusAtualizarId = content.get(2).toString();
                List<String> dbTables = tabelas.getOrDefault(formName, null);
                RecursiveAction action = new DeleteRecord(formName, requestId, statusAtualizarId, dbTables);
                actions.add(action);
                threadPool001.execute(action);
            }
            for (RecursiveAction action : actions) {
                action.join();
            }
            long end = System.currentTimeMillis();
            double lapse = ((0.0 + end) - (0.0 + start)) / 1000.0;
            Log.get().info(String.format("%f segundos para eliminar %d", lapse, contents.size()));
        }
    }

    public void index_check() throws IOException {
        IndexCheck i = new IndexCheck();
        i.run();
    }

    public void delta()  {
        Delta d = new Delta();
        d.run(null);
    }

    public void delta(String arg)  {
        Delta d = new Delta();
        d.run(arg);
    }
}
