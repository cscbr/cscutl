/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.bulkinstance;

import br.com.cscbrasil.bmc.ARServerUser;
import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Entry;
import com.bmc.arsys.api.Field;
import com.bmc.arsys.api.QualifierInfo;
import com.bmc.arsys.api.Value;
import com.bmc.cmdb.api.*;
import com.marzapower.loggable.Log;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import jxl.Sheet;
import jxl.Workbook;
import jxl.format.CellFormat;
import jxl.read.biff.BiffException;

/**
 *
 * @author ivan
 */
public class BulkInstance {
    
    String fileName;
    Workbook workbookRead;
    Sheet fonte;
    File xls;
    
    public BulkInstance(String fileName) {
        this.fileName = fileName;
        xls = new File(fileName);
    }
    
    public void execute() throws IOException, BiffException, ARException {
        RemedyConnection rmd = RemedyConnections.get();
        ARServerUser ar = rmd.desti;
        ar.cscSetModeBest();
        List<Field> astAttrFields = ar.getListFieldObjects("AST:Attributes");
        List<String> astAttrFieldNames = new ArrayList<>();
        for (Field f : astAttrFields) {
            astAttrFieldNames.add(f.getName());
        }
        try {
            CMDBUtil.CMDBSetServerPort(ar, ar.getPort(), null);
        } catch (ARException ex) {
            Logger.getLogger(BulkInstance.class.getName()).log(Level.SEVERE, null, ex);
        }
        workbookRead = Workbook.getWorkbook(xls);
        fonte = workbookRead.getSheet(0);
        for (int l = 1; l < fonte.getRows(); l++) {
            String nameSpace = fonte.getCell(0, l).getContents();
            String cmdbClass = fonte.getCell(1, l).getContents();
            String datasetId = fonte.getCell(2, l).getContents();
            CMDBClassNameKey classKey = new CMDBClassNameKey(cmdbClass, nameSpace);
            Map<String, CMDBAttributeValue> anAttributeValMap1 = new HashMap<>();
            anAttributeValMap1.put("DatasetId", new CMDBAttributeValue("DatasetId", new Value(datasetId)));
            String ql = "'DatasetId'=\"" + datasetId + "\"";
            Entry registro = new Entry();
            for (int c = 3; c < fonte.getColumns(); c++) {
                String attribute = fonte.getCell(c, 0).getContents();
                String value = fonte.getCell(c, l).getContents();
                if (attribute.equals("Name")) {
                    Log.get().info(String.format("%05d %s", l + 1, value));
                }
                CellFormat cellFormat = fonte.getCell(c, 0).getCellFormat();
                if (cellFormat.getBackgroundColour().getDescription().equals("red")) {
                    ql += String.format(" AND '%s'=\"%s\"", attribute, value);
                }
                if (cellFormat.getBackgroundColour().getDescription().equals("yellow")) {
                    for (Field f : astAttrFields) {
                        if (f.getName().equals(attribute)) {
                            if (f.getDataType() == Constants.AR_DATA_TYPE_INTEGER) {
                                registro.put(f.getFieldID(), new Value(Integer.parseInt(value)));
                            }
                            if (f.getDataType() == Constants.AR_DATA_TYPE_ENUM) {
                                registro.put(f.getFieldID(), new Value(Integer.parseInt(value)));
                            }
                            if (f.getDataType() == Constants.AR_DATA_TYPE_CHAR) {
                                registro.put(f.getFieldID(), new Value(value));
                            }
                        }
                    }
                    continue;
                }
                CMDBAttributeValue attVal1 = new CMDBAttributeValue(attribute, new Value(value));
                anAttributeValMap1.put(attribute, attVal1);
            }
            String[] find = CMDBInstance.find(ar, classKey, ql, null, Constants.AR_START_WITH_FIRST_ENTRY, 1);
            CMDBInstance xyz;
            boolean cria = false;
            if (find == null) {
                xyz = new CMDBInstance(classKey, anAttributeValMap1);
                cria = true;
            } else {
                String[] atts = {"DatasetID", "Name"};
                xyz = CMDBInstance.findByKey(ar, find[0], classKey, atts);
                xyz.setAttributeValues(anAttributeValMap1);
            }
            try {
                if (find == null) {
                    xyz.create(ar);
                } else {
                    xyz.update(ar);
                }
            } catch (ARException ex) {
                Logger.getLogger(BulkInstance.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            if (registro.size() > 1) {
                find = CMDBInstance.find(ar, classKey, ql, null, Constants.AR_START_WITH_FIRST_ENTRY, 1);
                registro.put(400129200, new Value(find[0]));
                
                QualifierInfo q;
                q = ar.parseQualification("AST:Attributes", String.format("'ReconciliationIdentity'=\"%s\"", find[0]));
                ar.mergeEntry("AST:Attributes", registro, Constants.AR_MERGE_ENTRY_DUP_MERGE, q, Constants.AR_MERGE_MULTIOPTIONS_CHANGE_FIRST);
            }
            
        }
        workbookRead.close();
    }
}
