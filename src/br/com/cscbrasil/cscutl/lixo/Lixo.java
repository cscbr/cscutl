/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.lixo;

import br.com.cscbrasil.bmc.ARServerUser;
import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import br.com.cscbrasil.cscutl.utils.FieldList;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.Entry;
import com.bmc.arsys.api.EntryListInfo;
import com.bmc.arsys.api.QualifierInfo;
import com.bmc.arsys.api.Value;
import com.marzapower.loggable.Log;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ivan
 */
public class Lixo {

    RemedyConnection rmd;
    ARServerUser ar;

    public Lixo() {
        ar = RemedyConnections.get().desti;
    }

    public void p1() throws ARException {
        List<Object> retorno = obtemIP("Producao");
        String dnsHostName = retorno.get(0).toString();
        String address = retorno.get(1).toString();

        Log.get().info("dnsHostName: " + dnsHostName);
        Log.get().info("address    : " + address);
    }

    public void relacionaIC(String numeroMudanca,String reconId) throws ARException {
        String formName1 = "CHG:Associations";
        Entry entry1 = new Entry();

        entry1.put(1000000204, new Value(reconId));
        entry1.put(1000000205, new Value(numeroMudanca));
        
        entry1.put(3003400, new Value("BMC.ASSET"));
        entry1.put(301763500, new Value("BMC.ASSET"));
        entry1.put(301763400, new Value("BMC.ASSET"));
        entry1.put(304343971, new Value("BMC.ASSET"));
        entry1.put(1000000203, new Value("CHG:Infrastructure Change"));
        entry1.put(1000000208, new Value("35000"));
        entry1.put(1000000216, new Value());
        entry1.put(10001882, new Value());
        entry1.put(7, new Value("1"));
        entry1.put(301568900, new Value("0"));
        entry1.put(1000000211, new Value("6000"));
        
        entry1.put(1000000206, new Value("172.20.0.138"));
        entry1.put(1000000101, new Value("AST:IPEndpoint"));
        entry1.put(230000009, new Value("BMC_IPENDPOINT"));
        
        
        
        
        

        ar.createEntry(formName1, entry1);

        String formName2 = "AST:CMDB Associations";
        Entry entry2 = new Entry();
        entry2.put(2, new Value()); // Submitter
        entry2.put(4, new Value()); // Assigned To
        entry2.put(7, new Value()); // Status
        entry2.put(8, new Value()); // Short Description
        entry2.put(112, new Value()); // Assignee Groups
        entry2.put(179, new Value()); // InstanceId
        entry2.put(60900, new Value()); // Vendor Assignee Groups
        entry2.put(60901, new Value()); // Vendor Assignee Groups_parent
        entry2.put(60989, new Value()); // Assignee Groups_parent
        entry2.put(230000009, new Value()); // Lookup Keyword
        entry2.put(301325000, new Value()); // Lookup Keyword01
        entry2.put(301362100, new Value()); // Parent_Lookup_Keyword
        entry2.put(301569500, new Value()); // ConsolidatedStatus
        entry2.put(301600300, new Value()); // DataTags
        entry2.put(301763400, new Value()); // Data_Set_ID01
        entry2.put(301763500, new Value()); // Parent_DataSet_ID01
        entry2.put(303156300, new Value()); // ParentStatusKeyword
        entry2.put(303523200, new Value()); // Type_of_relationship
        entry2.put(1000000101, new Value()); // Form Name01
        entry2.put(1000000203, new Value()); // Form Name02
        entry2.put(1000000204, new Value()); // Request ID01
        entry2.put(1000000205, new Value()); // Request ID02
        entry2.put(1000000206, new Value()); // Request Description01
        entry2.put(1000000208, new Value()); // Association Type01
        entry2.put(1000000211, new Value()); // Request Type01
        entry2.put(1000000216, new Value()); // Parent Request ID
        entry2.put(1000000234, new Value()); // Status-Asset
        entry2.put(1000000580, new Value()); // Status-INC
        entry2.put(1000000653, new Value()); // Status-CRQ
        entry2.put(1000000896, new Value()); // Status-PBI
        entry2.put(1000001085, new Value()); // Status-REQ
        entry2.put(1000001266, new Value()); // Status-TSK
        entry2.put(1000001722, new Value()); // Status-RLM
        entry2.put(1000002287, new Value()); // Quantity
        entry2.put(1000002702, new Value()); // Request Description Modified
        entry2.put(1000002705, new Value()); // Status-AOT
        entry2.put(1000002706, new Value()); // Parent Request ID02
        entry2.put(1000003561, new Value()); // Status-KDB
        entry2.put(1000004127, new Value()); // Status-WOR
        entry2.put(1000004987, new Value()); // Status-PKE
        entry2.put(1000004995, new Value()); // Status-PRQ
        entry2.put(1000005003, new Value()); // Status-ACM
        entry2.put(1000005080, new Value()); // Start Date 01
        entry2.put(1000005084, new Value()); // End Date 01
        entry2.put(1000005091, new Value()); // Status-CON
        entry2.put(1000005093, new Value()); // Status-SLA
        entry2.put(1000005095, new Value()); // Status-OLA
        entry2.put(1000005276, new Value()); // z1D Copy CI IA to Change
        entry2.put(1000005277, new Value()); // z1D Copy CI IA to Incident
        entry2.put(1000005490, new Value()); // Calculate Quantity Used
        ar.createEntry(formName2, entry2);
    }

    private List<Object> obtemIP(String systemRole) throws ARException {
        String formName = "AST:IPEndpoint";
        FieldList fList = new FieldList(formName);
        int AssetLifecycleStatus = 7;
        int DNSHostName = 536870914;
        int Address = 260140125;
        int ORDERED = 0;
        QualifierInfo qualification = ar.parseQualification(formName, String.format("'SystemRole'=\"%s\" AND 'AssetLifecycleStatus'=%d", systemRole, ORDERED));
        List<EntryListInfo> listIpEndpoint = ar.getListEntry(formName, qualification, Constants.AR_START_WITH_FIRST_ENTRY, 1, null, null, false, null);
        if (listIpEndpoint.isEmpty()) {
            Log.get().error("Nao foi possivel encontrar um endereco IP para o ambiente " + systemRole);
            return null;
        }
        Entry ipEndpoint = ar.getEntry(formName, listIpEndpoint.get(0).getEntryID(), fList.getFieldIdsAll());

        Entry reserva = new Entry();
        reserva.put(AssetLifecycleStatus, new Value(2));
        ar.setEntry(formName, ipEndpoint.getEntryId(), reserva, null, Constants.AR_JOIN_SETOPTION_NONE);

        // retornos
        String dnsHostName;
        String address;
        dnsHostName = ipEndpoint.get(DNSHostName).toString();
        address = ipEndpoint.get(Address).toString();

        List<Object> retorno = new ArrayList<>();

        retorno.add(dnsHostName);
        retorno.add(address);

        return retorno;
    }
}
