/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cscbrasil.cscutl.expurgo;

import br.com.cscbrasil.bmc.ARServerUser;
import br.com.cscbrasil.bmc.RemedyConnection;
import br.com.cscbrasil.bmc.RemedyConnections;
import br.com.cscbrasil.cscutl.utils.FieldList;
import com.bmc.arsys.api.ARException;
import com.bmc.arsys.api.Association;
import com.bmc.arsys.api.Constants;
import com.bmc.arsys.api.DirectAssociation;
import com.bmc.arsys.api.Entry;
import com.bmc.arsys.api.PKFKMapping;
import com.bmc.arsys.api.QualifierInfo;
import com.bmc.arsys.api.SQLResult;
import com.bmc.arsys.api.Value;
import com.bmc.thirdparty.org.apache.commons.lang.StringEscapeUtils;
import com.marzapower.loggable.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author ivan
 */
public class Expurgo {

    String formName;
    String q;
    QualifierInfo qual;
    static List<String> opened;
    private final RemedyConnection rmd;
    private final ARServerUser ar;
    List<Entry> listEntryObjects;
    String ident;

    public Expurgo(String formName, String q, String ident) throws ARException {
        rmd = RemedyConnections.get();
        ar = rmd.sourc;
        this.formName = formName;
        this.qual = ar.parseQualification(formName, q);
        this.ident = ident;
        FieldList fList = new FieldList(formName);
        int[] fieldIds = fList.getFieldIdsAll();

        listEntryObjects = ar.getListEntryObjects(formName, qual, Constants.AR_START_WITH_FIRST_ENTRY, 999999999, null, fieldIds, false, null);

        if (opened == null) {
            opened = new ArrayList<>();
        }
    }

    public void execute() throws ARException {
        //if (opened.contains(formName)) {
        //    return;
        //}
        //opened.add(formName);

        for (Entry e : listEntryObjects) {
            List<String> listAssociation = ar.getListAssociation(formName, 0, null, Constants.AR_ASSOCIATION_ENFORCE_ALL, Constants.AR_ASSOCIATION_CARDINALITY_ALL, Constants.AR_ASSOCIATION_TYPE_ALL);
            List<Association> listAssociationObjects;
            if (listAssociation.size() > 0) {
                listAssociationObjects = ar.getListAssociationObjects(listAssociation);
            } else {
                listAssociationObjects = new ArrayList<>();
            }

            Log.get().info(String.format(ident + "%s - %s", formName, e.getEntryId()));

            for (Association association : listAssociationObjects) {
                if (!association.isEnable()) {
                    continue;
                }
                DirectAssociation directAssociation = (DirectAssociation) association;
                Set<PKFKMapping> pkfkMappingSet = directAssociation.getPkfkMappingSet();
                String q1 = "";
                for (PKFKMapping pkfk : pkfkMappingSet) {
                    if (q1.length() != 0) {
                        q1 += " AND ";
                    }
                    Value fk = e.get(pkfk.getPrimaryKeyFieldId());
                    q1 += String.format("'%d'=\"%s\"", pkfk.getForeignKeyFieldId(), fk.toString());
                }
                Expurgo n = new Expurgo(association.getSecondaryFormName(), q1, ident + "|  ");
                n.execute();
            }
            deleteViaDBByEntryID(ar, formName, e.getEntryId());
            Log.get().info(String.format(ident + "%s - %s OK", formName, e.getEntryId()));
        }
    }

    void deleteViaDBByEntryID(ARServerUser ar, String formName, String entryID) throws ARException {
        //if (1 == 1) {
        //    return;
        //}
        List<String> dbTables = getDBTables(ar, formName);
        if (dbTables.isEmpty()) {
            Log.get().error("Impossivel determinar as tabelas relacionadas ao form " + formName + " em " + ar.getServer());
            return;
        }
        deleteViaDBByEntryID(ar, formName, entryID, dbTables);
    }

    List<String> getDBTables(ARServerUser ar, String formName) {
        List<String> getDBTables = new ArrayList<>();
        int formId = getFormNumber(ar, formName);
        if (formId == 0) {
            return getDBTables;
        }
        getDBTables.add(String.format("B%d", formId));
        getDBTables.add(String.format("H%d", formId));
        SQLResult result = null;
        try {
            String cmd = String.format("Select fieldid from field inner join arschema on arschema.schemaid=field.schemaid where (arschema.name='%s' OR arschema.name='%s__o' OR arschema.name='%s__c') AND datatype=%d AND foption<%d", escapeForDatabase(formName), escapeForDatabase(formName), escapeForDatabase(formName), Constants.AR_DATA_TYPE_ATTACH, Constants.AR_FIELD_OPTION_DISPLAY);
            result = ar.getListSQL(cmd, 999999999, false);
        } catch (ARException ex) {
            Log.get().fatal(ex.getMessage(), ex);
            return getDBTables;
        }
        List<List<Value>> records = result.getContents();
        for (List<Value> record : records) {
            Value v = record.get(0);
            getDBTables.add(String.format("B%dC%d", formId, v.getIntValue()));
        }
        getDBTables.add(String.format("T%d", formId));
        return getDBTables;
    }

    int getFormNumber(ARServerUser ar, String formName) {
        SQLResult result = null;
        try {
            String cmd = String.format("select schemaid from arschema where name='%s' order by overlayprop", escapeForDatabase(formName));
            result = ar.getListSQL(cmd, 10, false);
        } catch (ARException ex) {
            Log.get().fatal(ex.getMessage(), ex);
            return 0;
        }

        List<List<Value>> records = result.getContents();
        if (records.isEmpty()) {
            return 0;
        }
        if (records.get(0).isEmpty()) {
            return 0;
        }
        Value v = records.get(0).get(0);
        if (v == null) {
            return 0;
        }
        if (v.getValue() instanceof Integer) {
            return v.getIntValue();
        }
        return 0;
    }

    void deleteViaDBByEntryID(ARServerUser ar, String formName, String entryID, List<String> dbTables) throws ARException {
        for (String table : dbTables) {
            String chave = "C1";
            if (table.startsWith("H")) {
                chave = "entryId";
            }
            if (table.startsWith("B") && table.contains("C")) {
                chave = "entryId";
            }
            String cmd = String.format("delete from %s where %s='%s'", table, chave, entryID);
            try {
                SQLResult result = ar.getListSQL(cmd, 999999999, false);
            } catch (ARException ex) {
                if (table.startsWith("T")) {
                    Log.get().error("Erro ao eliminar o registro " + entryID + " do form " + formName + " em " + ar.getServer() + " " + cmd, ex);
                    throw ex;
                }
            }
        }
    }

    String escapeForDatabase(String queryparam) {
        return StringEscapeUtils.escapeSql(queryparam);
    }

}
